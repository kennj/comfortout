<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    protected $fillable = [
        'name',
        'product_image',
        'price',
        'address',
        'description',
        'status',
        'order_count',
    ];

    public function relatedShop()
    {
        return $this->belongsTo(User::class,'shop_id','id')->select(['id', 'name', 'address','phone', 'latitude', 'longitude','shop_time_open','shop_time_close', 'service_radius','shipping_estimate_time']);
    }

    public function relatedCategory()
    {
        $related_category = $this->belongsTo(Category::class,'category_id','id')->select(['id', 'name', 'category_image']);
        return $related_category;
    }

    public function relatedProduct()
    {
        return $this->hasMany(Product::class,'category_id','category_id')->select();
    }
    public function getProductImageAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($product)
        {
            $product->shop_id = auth()->user() ? auth()->user()->id : null;
        });

        static::updating(function($product)
        {
            $product->shop_id = auth()->user() ? auth()->user()->id : null;
        });
    }
}
