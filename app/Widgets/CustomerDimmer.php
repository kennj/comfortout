<?php

namespace App\Widgets;

use App\Order;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Customer;

class CustomerDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        if(auth()->user()->id == 1)
        {
            $count = Customer::count();

        }
        else
        {
            $count = $allOrder = Order::where('shop_id',auth()->user()->id)->count();
        }
        $string = trans_choice('Customers', $count);

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$count} {$string}",
            'text'   => __('Click on button below to view all customers.', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => __('View all customers'),
                'link' => route('voyager.customers.index'),
            ],
            'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
        ]));
    }
}
