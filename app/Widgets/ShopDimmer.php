<?php

namespace App\Widgets;

use App\Product;
use App\User;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;

class ShopDimmer extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = User::where('role_id','!=','1')->count();
        $product_count = Product::where('shop_id',auth()->user()->id)->count();
        $string = trans_choice('Shops', $count);
        if(auth()->user()->role_id === 1)
        {
            return view('voyager::dimmer', array_merge($this->config, [
                'key' => 'Shops',
                'icon'   => 'voyager-file-text',
                'title'  => "{$count} {$string}",
                'text'   => __('Click on button below to view all shops.', ['count' => $count, 'string' => Str::lower($string)]),
                'button' => [
                    'text' => __('View all shops'),
                    'link' => route('voyager.users.index'),
                ],
                'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
            ]));
        }
        else
        {
            $string = trans_choice('Products', $count);
            return view('voyager::dimmer', array_merge($this->config, [
                'key' => 'Products',
                'icon'   => 'voyager-file-text',
                'title'  => "{$product_count} {$string}",
                'text'   => __('Click on button below to view your products.', ['count' => $product_count, 'string' => Str::lower($string)]),
                'button' => [
                    'text' => __('View your Products'),
                    'link' => route('voyager.products.index'),
                ],
                'image' => voyager_asset('images/widget-backgrounds/03.jpg'),
            ]));
        }

    }
}
