<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Transaction
 *
 * @property int $id
 * @property float $amount
 * @property string $charge_id
 * @property int $order_id
 * @property int $type
 * @property string $description
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @package App
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereChargeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Transaction whereUpdatedAt($value)
 */
class Transaction extends Model
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const ORDER_PAYMENT_TYPE = 1;
    const ORDER_TIP_TYPE = 2;

    const CHARGE_BY_BALANCE = 'CHARGE BY BALANCE';

    protected $casts = [
        'order_id' => 'int',
        'type'           => 'int',
        'status'         => 'int'
    ];

    protected $fillable = [
        'charge_id',
        'order_id',
        'type',
        'amount',
        'description',
        'status'
    ];
}
