<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 12 Mar 2018 00:23:23 -0400.
 */

namespace App\Models;

use App\Customer;
use App\Order;
use App\Shipper;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    const NOTIFICATION_CREATE_ORDER = 1;
    const NOTIFICATION_APPROVE_ORDER = 2;
    const NOTIFICATION_CANCEL_ORDER = 3; //back to create order list
    const NOTIFICATION_DELIVERING_ORDER = 4;
    const NOTIFICATION_COMPLETE_ORDER = 5;
    const NOTIFICATION_CUSTOMER_CANCEL = 6;
    const NOTIFICATION_FINISH_PAYMENT_TIPS = 7;

    protected $casts = [
        'customer_id'        => 'int',
        'waiter_id'      => 'int',
        'order_id' => 'int',
        'type'           => 'int'
    ];

    protected $fillable = [
        'customer_id',
        'waiter_id',
        'order_id',
        'message',
        'type'
    ];

    public function relatedCustomer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id')->select(['id', 'name', 'email', 'phone', 'avatar','address','longitude','latitude']);
    }

    public function relatedWaiter()
    {
        return $this->belongsTo(Shipper::class,'waiter_id','id')->select(['id', 'name', 'avatar','email','phone','shop_id','longitude','latitude']);
    }

    public function relatedOrder()
    {
        return $this->belongsTo(Order::class,'order_id','id')->select(['id','order_code','qrcode']);
    }
}
