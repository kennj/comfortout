<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ServiceLocation extends Model
{
    protected $table = 'service_location';

    public function serviceId()
    {
        return $this->belongsTo(Service::class,'service_id','id')->select(['id', 'name']);;
    }
}
