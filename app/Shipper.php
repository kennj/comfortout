<?php

namespace App;

use App\Notifications\ShipperResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class Shipper extends Authenticatable
{
    use Notifiable;

    const LOGIN_YES = 1;
    const LOGIN_NO = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone','latitude','longitude','shop_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */

    public function getAvatarAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }

    public function card($card_id)
    {
        return Stripe::cards()->find($this->stripe_id, $card_id);
    }

    public function selectCard($card_id)
    {
        return Stripe::customers()->update($this->stripe_id, ['default_source' => $card_id]);
    }

    public function checkCard($card_id)
    {
        try {
            return Stripe::cards()->find($this->stripe_id, $card_id);
        } catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            throw new Exception('Card not found.');
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ShipperResetPassword($token));
    }

    public function relatedShop()
    {
        return $this->belongsTo(User::class,'shop_id','id')->select(['id','avatar','name', 'address','phone', 'latitude', 'longitude','shop_time_open','shop_time_close', 'service_radius','shipper_distance_customer']);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($shipper)
        {
//            $shipper->shop_id = auth()->user() ? auth()->user()->id : null;
            $shipper->api_token = sha1(time() . uniqid() . rand(100000000, 999999999));
        });

        static::updating(function($shipper)
        {
//            $shipper->shop_id = auth()->user() ? auth()->user()->id : null;
        });
    }
}
