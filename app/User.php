<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'avatar',
        'email',
        'password',
        'phone',
        'address',
        'latitude',
        'longitude',
        'service_radius',
        'min_shipper_tip_percent',
        'active',
        'service_id',
        'shipper_music',
        'shiper_distance_customer',
        'service_fee_percent',
        'shop_time_open',
        'shop_time_close'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function relatedProduct()
    {
        return $this->hasMany(Product::class,'shop_id','id');
    }

    public function serviceId()
    {
        return $this->belongsTo(Service::class,'service_id','id')->select(['id', 'name', 'service_image']);;
    }

    public function relatedServiceLocation()
    {
        return $this->belongsTo(ServiceLocation::class,'service_location_id','id')->select(['id', 'name']);;
    }

    public function getAvatarAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }

    public function getShopBackgroundAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }
}
