<?php

namespace App\Http\Controllers;


use App\Customer;
use App\Order;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Geckob\Firebase\Firebase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use League\Flysystem\Exception;


class PaymentController extends Controller
{
    public function checkout(Request $request)
    {
        try {
            // Validate Inputs
            $rules     = array(
                'amount'         => 'required|numeric|min:0',
                'tip_amount'         => 'required|numeric|min:0',
                'order_id' => 'required|integer'
            );
            $messages  = array(
                'required' => 'The :attribute field is required.',
                'numeric'  => 'The :attribute field must be a number.',
            );
            $validator = Validator::make($request->all(), $rules, $messages);
            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $user  = Customer::find(Auth::guard('api-customer')->id());
            $order = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }
            DB::beginTransaction();

            $amount = $request->amount;
            $tip_amount = $request->tip_amount;
            $shop = User::find($order->shop_id);
            if(!$shop)
            {
                throw new Exception('Shop not found.');
            }
            $amountForAdmin = $amount*$shop->service_fee_percent/100;
            $amountForShop = $amount - $amountForAdmin + $tip_amount;
            if ($request->card_id) {
                $cards = Stripe::cards()->all($user->stripe_id);
                if (!count($cards['data'])) {
                    throw new Exception('Card is empty. Please add a card.');
                }
                $user->selectCard($request->card_id);
                //charge for shop
                $chargeShop = Stripe::charges()->create(
                    [
                        'customer'    => $user->stripe_id,
                        'currency'    => 'USD',
                        'amount'      => $amountForShop,
                        'description' => 'Payment to order ',
                        'metadata'    => [
                            'order_id' => $order->id
                        ],
                    ]);
                //charge for admin
                $chargeAdmin = Stripe::charges()->create(
                    [
                        'customer'    => $user->stripe_id,
                        'currency'    => 'USD',
                        'amount'      => $amountForAdmin,
                        'description' => 'Payment to order ',
                        'metadata'    => [
                            'order_id' => $order->id
                        ],
                    ]);

            } else {
                if ($user->balance < $amount) {
                    throw new Exception('Your balance is not enough.');
                }
                $chargeShop['id']  = Transaction::CHARGE_BY_BALANCE;
                $chargeAdmin['id']  = Transaction::CHARGE_BY_BALANCE;
                $user->balance -= ($amount + $tip_amount);
                $user->save();
            }

            $transaction = Transaction::create([
                'charge_id'      => $chargeShop['id'],
                'order_id' => $order->id,
                'type'           => Transaction::ORDER_PAYMENT_TYPE,
                'amount'         => $amountForShop,
                'description'    => 'Payment to order',
                'status'         => Transaction::STATUS_PENDING,
            ]);

            $transaction = Transaction::create([
                'charge_id'      => $chargeAdmin['id'],
                'order_id' => $order->id,
                'type'           => Transaction::ORDER_PAYMENT_TYPE,
                'amount'         => $amountForAdmin,
                'description'    => 'Payment to order',
                'status'         => Transaction::STATUS_PENDING,
            ]);

            DB::commit();

            return \Response::json([
                'result'  => true,
                'message' => 'Successfully charged $' . ($amount+$tip_amount) . '! We will be in touch shortly.'
            ]);
        } catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            return response()->json([
                'result'  => false,
                'message' => 'List card is empty. Please add a card.'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return \Response::json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

}
