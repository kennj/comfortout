<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\OrderItem;
use App\Product;
use App\Shop;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;


class OrderController extends Controller
{



    public function checkOrderStatus(Request $request){
        try {
            $order = Order::where('id',$request->order_id)->with('infoShipper')->with('infoCustomer')->with('orderItemsInOrder')->with('relatedShop')->first();
            if(!$order){
                throw new Exception('Order not found.');
            }

//            // Check publisher is online based on firebase
//            $firebase      = new Firebase();
//            $firebase      = $firebase->setPath('user_onlines/');
//            $value         = json_decode($firebase->get($order->id));
//
//            if (!$order->is_login || !$value || $value + 5 < time()) {
//                throw new Exception($order->name . ' is offline now.');
//            }

            return response()->json([
                'result' => true,
                'data' => $order,
                'message' => 'get order info complete'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
