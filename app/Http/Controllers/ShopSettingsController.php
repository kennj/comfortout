<?php

namespace App\Http\Controllers;

use App\Shipper;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Validator;

class ShopSettingsController extends Controller
{
    //

    public function index()
    {
        // Check permission

        $data = User::find(auth()->user()->id);

        return Voyager::view('shop-settings.index', compact('data'));
    }

    public function update(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . Auth()->user()->id . ',id',
            'phone' => 'required',
            'password' => 'nullable|string|min:6',
            'address' => 'required',
            'latitude' => 'required',
            'service_radius' => 'required',
            'min_shipper_tip_percent' => 'required',
            'shipper_distance_customer' => 'required',
        ]);

        $input = $request->all();

        if ($validator->passes()) {

            // Store your user in database
            $shop = User::find(Auth()->user()->id);
            if ($request['password'] != "") {
                $shop->password = bcrypt($request['password']);
            }
            $open_time = date("H:i:s", strtotime($request['open_time']));
            $close_time = date("H:i:s", strtotime($request['close_time']));
//            //convert time to UTC 0
//            $time_zone_offset = $request['local_time'];
//            $open_time = date("H:i:s",strtotime($time_zone_offset.' minutes',strtotime($open_time)));
//            $close_time = date("H:i:s",strtotime($time_zone_offset.' minutes',strtotime($close_time)));
            $shop->name = $request['name'];
            $shop->email = $request['email'];
            $shop->phone = $request['phone'];
            $shop->address = $request['address'];
            $shop->stripe_id = $request['stripe_id'];
            $shop->latitude = $request['latitude'];
            $shop->longitude = $request['longitude'];
            $shop->service_radius = $request['service_radius'];
            $shop->min_shipper_tip_percent = $request['min_shipper_tip_percent'];
            $shop->shipping_estimate_time = $request['shipping_estimate_time'];
            $shop->shipper_distance_customer = $request['shipper_distance_customer'];
            $shop->shop_time_open = $open_time;
            $shop->shop_time_close = $close_time;
            $shop->save();

            return \Illuminate\Support\Facades\Response::json(['success' => '1']);
        }
        return \Illuminate\Support\Facades\Response::json(['errors' => $validator->errors()]);
    }

    public function updateAvatar(Request $request)
    {
        $shop = User::find(auth()->user()->id);
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions

        if ($request->hasFile('avatar')) {
            $slug = 'users'; //folder name
            $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
            // get uploaded file's extension
            $ext = strtolower($request->file('avatar')->getClientOriginalExtension());
            $path = $slug . '/' . date('FY') . '/';

            if (in_array($ext, $valid_extensions)) {
                Storage::disk('public')->putFileAs(
                    $path, $request->file('avatar'), $filename
                );
                $shop->avatar = $path . $filename;
                $shop->save();
                echo '<img src=' . setting('admin.store_path') . $path . $filename . ' style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />';
            } else
                echo 'invalid file';
        }
    }

    public function updateShopBackground(Request $request)
    {
        $shop = User::find(auth()->user()->id);
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp'); // valid extensions

        if ($request->hasFile('shop_background')) {
            $slug = 'users'; //folder name
            $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('shop_background')->getClientOriginalExtension());
            // get uploaded file's extension
            $ext = strtolower($request->file('shop_background')->getClientOriginalExtension());
            $path = $slug . '/' . date('FY') . '/';

            if (in_array($ext, $valid_extensions)) {
                Storage::disk('public')->putFileAs(
                    $path, $request->file('shop_background'), $filename
                );
                $shop->avatar = $path . $filename;
                $shop->save();
                echo '<img src=' . setting('admin.store_path') . $path . $filename . ' style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;" />';
            } else
                echo 'invalid file';
        }
    }

    public function updateMusic(Request $request)
    {
        $shop = User::find(auth()->user()->id);
        $valid_extensions = array('mp3', 'MP3','wav','m4a','m4r','ogg','m4p','wma','flac'); // valid extensions

        if ($request->file('music')) {
            $slug = 'shipper_musics'; //folder name
            $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('music')->getClientOriginalExtension());
            // get uploaded file's extension
            $ext = strtolower($request->file('music')->getClientOriginalExtension());
            $path = $slug . '/' . date('FY') . '/';

            if (in_array($ext, $valid_extensions)) {
                Storage::disk('public')->putFileAs(
                    $path, $request->file('music'), $filename
                );
                $shop->shipper_music = setting('admin.store_path') . $path . $filename;
                $shop->save();
                echo '<audio controls>
                        <source src="'. $shop->shipper_music.'" type="audio/mpeg">
                      </audio>';
            } else
                echo 'invalid file';
        }
    }

    public function shipperUpdateMusic(Request $request)
    {
        $valid_extensions = array('mp3', 'MP3','wav','m4a','m4r','ogg','m4p','wma','flac'); // valid extensions
        if(!$request->shipper_id)
        {
            echo 'missing shipper_id';
        }
        $shipper = Shipper::find($request->shipper_id);
        if ($request->file('music')) {
            $slug = 'shipper_musics'; //folder name
            $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('music')->getClientOriginalExtension());
            // get uploaded file's extension
            $ext = strtolower($request->file('music')->getClientOriginalExtension());
            $path = $slug . '/' . date('FY') . '/';

            if (in_array($ext, $valid_extensions)) {
                Storage::disk('public')->putFileAs(
                    $path, $request->file('music'), $filename
                );
                $shipper->shipper_music = setting('admin.store_path') . $path . $filename;
                $shipper->save();
                echo '<audio controls>
                        <source src="'. $shipper->shipper_music.'" type="audio/mpeg">
                      </audio>';
            } else
                echo 'invalid file';
        }
    }
}
