<?php

namespace App\Http\Controllers\Auth;

use App\Category;
use App\Http\Controllers\CategoryController;
use App\Mail\AdminNeedApproveShop;
use App\Mail\ShopCompleteRegister;
use App\ParentCategory;
use App\Service;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|numeric|min:10',
            'password' => 'required|min:6|confirmed',
            'address' => 'required',
            'latitude' => 'required',
            'service_radius' => 'required|max:4',
            'services' => 'required',
//            'service_location' => 'required',
        ]);

        $input = $request->all();

        if ($validator->passes()) {
            // Store your user in database
            $listmail = setting('admin.list_admin_mail');
            $arrlistmail = explode("\n", $listmail);
            $open_time = date("H:i:s", strtotime($request['open_time']));
            $close_time = date("H:i:s", strtotime($request['close_time']));
            //convert time to UTC 0
//            $time_zone_offset = $request['local_time'];
//            $open_time = date("H:i:s",strtotime($time_zone_offset.' minutes',strtotime($open_time)));
//            $close_time = date("H:i:s",strtotime($time_zone_offset.' minutes',strtotime($close_time)));

            $shop = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'password' => bcrypt($request['password']),
                'address' => $request['address'],
                'latitude' => $request['latitude'],
                'longitude' => $request['longitude'],
                'service_id' => $request['services'],
//                'service_location_id' => $request['service_location'],
                'service_radius' => $request['service_radius'],
                'shop_time_open' => $open_time,
                'shop_time_close' =>  $close_time,
                'active' => 0,
            ]);

//            //update default category for shop
//            $service_id = $request['service_radius'];
//
//            $parrent_category_array = ParentCategory::all();
//
//            foreach ($parrent_category_array as $parent_category)
//            {
//                if($parent_category->service_id == $service_id)
//                {
//                    $category_array = Category::where('parent_id',$parent_category->id)->where('default','1')->get();
//                    foreach ($category_array as $category)
//                    {
//                        $new_category = $category->replicate();;
//                        $new_category->shop_id = $shop->id;
//                        $new_category->slug = $category->slug.'-'.$shop->id;
//                        $new_category->default = 0;
//                        $new_category->order = 0;
//                        $new_category->save();
//                    }
//                }
//            }

            //send mail to admin (request approve customer)
            foreach ($arrlistmail as $adminmail)
            {
                $adminmail = preg_replace('/\s+/', '', $adminmail);
                $mailable = new AdminNeedApproveShop($shop);
                Mail::to($adminmail)->send($mailable);
            }

            //send mail to shop (wait for admin approve)
            $mailable = new ShopCompleteRegister($shop);
            Mail::to($shop->email)->send($mailable);

            return \Illuminate\Support\Facades\Response::json(['success' => '1']);

        }

        return \Illuminate\Support\Facades\Response::json(['errors' => $validator->errors()]);
    }
}
