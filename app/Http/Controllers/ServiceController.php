<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\Helper;
use App\Product;
use App\Service;
use App\ParentCategory;
use App\User;
use Illuminate\Http\Request;
use Dompdf\Exception;


class ServiceController extends Controller
{
    //
    public function showAllServices(Request $request)
    {
        try {
            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            if (!$request->local_time) {
                throw new Exception('Missing local_time.');
            }
            $all_services = Service::all();
            $arr_services_nearby = [];
            if (!count($all_services)) {
                throw new Exception('No Services available.');
            }

            foreach ($all_services as $service) {
//                $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $service->longitude, $service ->latitude,setting('admin.distance_unit'));
//                if ($distance <= setting('admin.distance_detect_nearby_services'))
//                {

                $shops = User::where('role_id', 2)->where('service_id', $service->id)->with('relatedServiceLocation')->get();
                $shopInRange = [];
                foreach ($shops as $shop) {
                    $productByShop = Product::where('shop_id',$shop->id)->get();
                    if(count($productByShop))
                    {
                        $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                        if ($distance <= $shop->service_radius) {
                            $now = date("H:i:s", strtotime($request->local_time));
                            if ($shop->shop_time_open < $shop->shop_time_close) {
                                if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                                    array_push($shopInRange, $shop);
                                    $service->location = $shop->relatedServiceLocation['name'];
                                }
                            } else {
                                if ($shop->shop_time_open < $now || $shop->shop_time_close > $now) {
                                    array_push($shopInRange, $shop);
                                    $service->location = $shop->relatedServiceLocation['name'];
                                }
                            }

                        }
                    }
                }
                if (!$shopInRange) {
                    array_push($arr_services_nearby, $service);
                } else {
                    $service->shops = $shopInRange;
                    array_unshift($arr_services_nearby, $service);
                }

  //        else
   //         {
   //            array_push($arr_services_nearby, $service);

            }

            return response()->json([
                'result' => true,
                'message' => 'Get list Services successfully',
                'data' => $arr_services_nearby,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    public function showParentCategory(Request $request)
    {
        try {
            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            if (!$request->local_time) {
                throw new Exception('Missing local_time.');
            }

            if(!$request->service_id) {
                throw new Exception('Missing service_id.');
            }

            $shops = User::where('role_id', 2)->where('service_id',$request->service_id)->get();
            $shopInRange = [];
            if(count($shops))
            {
                foreach ($shops as $shop) {
                    $productByShop = Product::where('shop_id',$shop->id)->get();
                    if(count($productByShop)) {
                        $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                        if ($distance <= $shop->service_radius) {
                            //Check time
                            //$now = Carbon::now()->toTimeString();
                            $now = date("H:i:s", strtotime($request->local_time));
                            if ($shop->shop_time_open < $shop->shop_time_close) {
                                if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                                    $shopInRange = $shop;

                                }
                            } else {
                                if ($shop->shop_time_open < $now || $shop->shop_time_close > $now) {
                                    $shopInRange = $shop;
                                }
                            }
                        }
                    }
                }
            }

            if(!count($shopInRange)){
                $parent_category = ParentCategory::where('service_id',$request->service_id)->get();
                return response() -> json([
                    'result' =>true,
                    'message' => 'Get list parent categories successfully. No shop available',
                    'data' => $parent_category,
                    'type_data_array' => true,
                ]);

            }

            $productOfShopInRange = Product::where('shop_id',$shopInRange->id)->get();

            if(!count($productOfShopInRange)){

                return response() -> json([
                    'result' =>true,
                    'message' => 'Get list parent categories successfully. No product available',
                    'data' => [],
                    'type_data_array' => true,
                ]);

            }
            $category_ids = [];

            foreach ($productOfShopInRange as $product)
            {
                $productCategory = Category::where('id',$product->category_id)->first();
                if(count($productCategory))
                {
                    if(!in_array($productCategory->id, $category_ids, true)){
                        array_push($category_ids, $productCategory->id);
                    }
                }

            }
            $categories = Category::findMany($category_ids);

            if(!count($categories)){
                return response() -> json([
                    'result' =>true,
                    'message' => 'Get list parent categories successfully. No category available',
                    'data' => [],
                    'type_data_array' => true,
                ]);
            }

            $list_parent_category_id = [];
            foreach ($categories as $category)
            {
                $parent_category = ParentCategory::where('id',$category->parent_id)->first();

                if(count($parent_category))
                {
                    if(!in_array($parent_category->id, $list_parent_category_id, true)){
                        array_push($list_parent_category_id, $parent_category->id);
                    }
                }

            }
            $list_parent_category = ParentCategory::findMany($list_parent_category_id);

            if (!count($list_parent_category)) {
                throw new Exception('No Parent Category available');
            }
            return response()->json([
                'result' => true,
                'message' => 'Get list categories successfully',
                'data' => $list_parent_category,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }
}
