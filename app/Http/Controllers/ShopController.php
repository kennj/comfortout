<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers;

class ShopController extends Controller
{
    //show all Shop
    public function showAll()
    {
        try {
            $shops = User::where('role_id','2')->paginate(1000);
            if (!count($shops)) {
                throw new Exception('No Shop available');
            }

//            $store_path = setting('admin.store_path');
//
//            foreach($shops as $shop)
//            {
//                if($shop->avatar)
//                {
//                    $shop->avatar = $store_path.$shop->avatar;
//                }
//            }
            return response()->json([
                'result' => true,
                'message' => 'Get shop list successfully',
                'data' => $shops,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    public function getShop($id)
    {
        try{
            $shop = User::where('id',$id)->where('role_id','2')->get(); //role_id == 2 Shop
            if(!count($shop)){
                throw new Exception('No Shop available');
            }

//            $store_path = setting('admin.store_path');
//
//            foreach($shop as $shop_temp)
//            {
//                if($shop_temp->avatar)
//                {
//                    $shop_temp->avatar = $store_path.$shop_temp->avatar;
//                }
//            }

            return response()->json([
                'result' =>true,
                'message'=>'Get shop info successfully',
                'data' => $shop,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data'=>array(),
                'type_data_array' =>true,
            ]);
        }
    }

    public function getProductByShop($id)
    {
        try{
            $shop =Product::where('shop_id',$id)->with('relatedShop')->with('relatedCategory')->get();
            if(!count($shop)){
                throw new Exception('No Product available in this shop');
            }
            return response()->json([
                'result' =>true,
                'message'=>'Get product list successfully',
                'data' => $shop,
                'type_data_array' => true,
            ]);
        }
        catch(Exception $e){
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data'=>array(),
                'type_data_array' =>true,
            ]);
        }
    }
}