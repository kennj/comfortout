<?php

namespace App\Http\Controllers;

use app\Helpers\Constants;
use App\Customer;
use App\Models\Notification;
use App\Notifications;
use App\Shipper;
use Carbon\Carbon;
use Geckob\Firebase\Firebase;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use App\Helpers\Helper;
use App\User;

class SearchController extends Controller
{
    public function SearchShopByAddress(Request $request){
        try {
            $shopArrNearbyAddress = [];
            if($request->latitude && $request->longitude)
            {

                $shops = User::where('role_id',2)->with('relatedProduct')->get();
                foreach ($shops as $shop)
                {
                    $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop ->latitude,setting('admin.distance_unit'));
                    if ($distance <= setting('admin.distance_search_shop'))
                    {
                        array_push($shopArrNearbyAddress, $shop);

                    }
                }
            }
            else{
                $search_key = $request->search_key;
                if(!$search_key){
                    throw new Exception('search_key not found.');
                }
                $shops = User::where('name','like','%'.$search_key.'%')->orWhere('address','like','%'.$search_key.'%')->get();
                foreach ($shops as $shop)
                {
                        array_push($shopArrNearbyAddress, $shop);
                }
            }
            //Get Lng Lat base on search key
//            $lnglat = Helper::getLocation($search_key);
//            $shops = User::where('role_id',2)->with('relatedProduct')->get();
//            foreach ($shops as $shop)
//            {
//                $distance = Helper::getDistanceBetweenTwoPoint($lnglat['lng'], $lnglat['lat'], $shop->longitude, $shop ->latitude,setting('admin.distance_unit'));
//                if ($distance <= setting('admin.distance_search_shop'))
//                {
//                    array_push($shopArrNearbyAddress, $shop);
//
//                }
//            }
            if(!$shopArrNearbyAddress)
            {
                throw new Exception('No shop nearby your input address');
            }

            return response()->json([
                'result' => true,
                'message' => 'Get list success.',
                'data' => $shopArrNearbyAddress,
                'type_data_array' =>true

            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data'=> array(),
                'type_data_array' =>true
            ]);
        }
    }
}
