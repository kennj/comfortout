<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Dompdf\Exception;
use App\Product;
use App\Category;
use App\Helpers\Helper;


/**
 * Class ProductsController
 * @package App\Http\Controllers\v1
 */
class ProductController extends Controller
{
    //get all product
    public function showAll()
    {
        try {
            $product = Product::with('relatedShop')->with('relatedCategory')->paginate(1000);
            if (!count($product)) {
                throw new Exception('No Prophpduct available.');
            }
            return response()->json([
                'result' => true,
                'message' => 'Get product successfully',
                'data' => $product,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    //get product HOT
    public function showHot(Request $request)
    {
        try {
            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            $shops = User::where('role_id', 2)->get();
            if (!count($shops)) {
                throw new Exception('No Shop available.');
            }

            foreach ($shops as $shop) {
                $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                if ($distance <= $shop->service_radius) {
                    $product = Product::where('shop_id', $shop->id)->with('relatedShop')->with('relatedCategory')->orderByDesc('order_count')->limit(10)->get();
                }
            }
            if (!count($product)) {
                throw new Exception('No Product available.');
            }
            return response()->json([
                'result' => true,
                'message' => 'Get product successfully',
                'data' => $product,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    //get product using product ID
    public function show(Request $request)
    {
        try {

            if (!$request->lat_customer || !$request->lng_customer) {
                throw new Exception('Missing lat_customer or lng_customer of customer.');
            }

            if (!$request->local_time) {
                throw new Exception('Missing local_time.');
            }

            if (!$request->product_id) {
                throw new Exception('Missing product Id.');
            }
            $product = Product::where('id', $request->product_id)->with('relatedShop')->with('relatedCategory')->first();
//            $product = Product::where('id', $request->product_id)->with('relatedShop')->with('relatedCategory')->with('relatedProduct')->first();
            $relatedProduct = Product::where('category_id',$product->category_id)->where('id','!=',$request->product_id)->get();
            $product->related_product = $relatedProduct;
            if (!$product) {
                throw new Exception('No Product available.');
            }

            $shop = User::where('id', $product->shop_id)->first();
            if (!$shop) {
                throw new Exception('No Shop with this product.');
            }

            $distance = Helper::getDistanceBetweenTwoPoint($request->lng_customer, $request->lat_customer, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
            if ($distance > $shop->service_radius) {
                $product->relatedShop->shop_available = false;
                return response()->json([
                    'result' => true,
                    'data' => $product,
                    'message' => 'Get product successfully. Too far, can not add to cart',
//                    'add_to_cart'=>false
                ]);
            }

            //Check time
            //$now = Carbon::now()->toTimeString();
            $now = date("H:i:s", strtotime($request->local_time));
            if ($shop->shop_time_open < $shop->shop_time_close) {
                if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                    $product->relatedShop->shop_available = false;
                    return response()->json([
                        'result' => true,
                        'data' => $product,
                        'message' => 'Get product successfully: ' . $now . 'not in service time',
//                    'add_to_cart'=>false
                    ]);
                }
            } else {
                if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                    $product->relatedShop->shop_available = false;
                    return response()->json([
                        'result' => true,
                        'data' => $product,
                        'message' => 'Get product successfully: ' . $now . 'not in service time',
//                    'add_to_cart'=>false
                    ]);
                }
            }


            $product->relatedShop->shop_available = true;
            return response()->json([
                'result' => true,
                'message' => 'Get product successfully',
                'data' => $product,
                'type_data_array' => true,
//                'add_to_cart'=>true
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    //get list related product using product ID
    public function showRelatedProducts($id)
    {
        $categoryID = Product::select('category_id')->where('id', $id)->get();
        return $categoryID;

    }
}
