<?php

namespace App\Http\Controllers;

use App\Category;
use App\Helpers\Helper;
use App\User;
use Illuminate\Http\Request;
use App\Product;
use Dompdf\Exception;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function showAllCategories()
    {
        try {
            $all_categories = Category::all();
            if(!count($all_categories)) {
                throw new Exception('No category available.');
            }
//            $store_path = setting('admin.store_path');
//            foreach($all_categories as $category)
//            {
//                if($category->category_image)
//                {
//                    $category->category_image = $store_path.$category->category_image;
//                }
//            }
            return response() -> json ([
                'result' => true,
                'message' => 'Get list categories successfully',
                'data'    => $all_categories,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    public function showProductByCategory(Request $request)
    {
        try{
            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            if (!$request->local_time) {
                throw new Exception('Missing local_time.');
            }
            if (!$request->category_id) {
                throw new Exception('Missing category_id.');
            }

            $list_product_by_category = \App\Product::where('category_id',$request->category_id)->with('relatedShop')->with('relatedCategory')
                ->paginate(1000);
            if(!count($list_product_by_category)){
                throw new Exception('No Product available.');
            }

            $list_product = [];
            foreach ($list_product_by_category as $product) {
                $shop = User::where('id', $product->shop_id)->first();

                if (!$shop) {
                    throw new Exception('No Shop with this product.');
                }

                $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                if ($distance < $shop->service_radius) {
                    //Check time
                    //$now = Carbon::now()->toTimeString();
                    $now = date("H:i:s", strtotime($request->local_time));
                    if ($shop->shop_time_open < $shop->shop_time_close) {
                        if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                            array_push($list_product, $product);

                        }
                    } else {
                        if ($shop->shop_time_open < $now || $shop->shop_time_close > $now) {
                            array_push($list_product, $product);
                        }
                    }
                }
            }
            return response() -> json([
                'result' => true,
                'message' => 'Get list product successfully',
                'data' => $list_product,
                'type_data_array' => true,
            ]);
        } catch (Exception $e){
            return response() ->json([
                'result' =>false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }
}
