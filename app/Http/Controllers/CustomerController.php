<?php

namespace App\Http\Controllers;

use app\Helpers\Constants;
use App\Customer;
use App\Models\Notification;
use App\Shipper;
use Carbon\Carbon;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Geckob\Firebase\Firebase;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use LaravelQRCode\Facades\QRCode;
use League\Flysystem\Exception;
use App\Order;
use App\Product;
use App\User;
use App\OrderItem;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerWelcomeEmail;
use App\Helpers\Helper;

class CustomerController extends Controller
{

    public function getAll()
    {
        try {
            /* @var $customer Customer */
            $customer = Customer::all();
            if (!count($customer)) {
                throw new \Exception('No Customer in list');
            }
//        if (!$customer->avatar) {
//            $customer->avatar = 'http://beachad.test/storage/users/default.png';
//        }
            return response()->json([
                'result' => true,
                'message' => 'Get list customers info successfully',
                'data' => $customer,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }
    }

    public function getbyID($id)
    {
        try {
            /* @var $customer Customer */
            $customer = Customer::where('id', $id)->first();
            if (!count($customer)) {
                throw new \Exception('No Customer match ID');
            }
//        if (!$customer->avatar) {
//            $customer->avatar = 'http://beachad.test/storage/users/default.png';
//        }
            return response()->json([
                'result' => true,
                'message' => 'Get customer info successfully',
                'data' => $customer,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function register(Request $request)
    {
        try {
            // Validator
            $rules = array(
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:customers',
                'phone' => 'required|max:13',
                'password' => 'required|string|min:6',
            );
            $messages = array(
                'required' => 'The :attribute field is required.',
                'email.email' => 'The email must be a valid email address.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            // Check Email
            if (count(Customer::where('email', $request->email)->get())) {
                throw new Exception('Email already exists.');
            }

            $customer = new Customer();
            $customer->email = $request->email;
            $customer->name = $request->name;
            $customer->phone = $request->phone;
            $customer->password = \Hash::make($request->password);

            $customer->is_login = Customer::LOGIN_YES;

            $customer->api_token = sha1(time() . uniqid() . rand(100000000, 999999999));

            $customer->device_token = $request->device_token;

            $slug = 'customers';
            if ($request->hasFile('avatar')) {
                $slug = 'customers';
                $path = $slug . '/' . date('FY') . '/';

                $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
                \Storage::disk('public')->putFileAs(
                    $path, $request->file('avatar'), $filename
                );
                $customer->avatar = $path . $filename;
            } else {

                $customer->avatar = $slug . '/default.png';
            }

            $customer->save();
            $mailable = new CustomerWelcomeEmail($customer);
            Mail::to($customer->email)->send($mailable);

            return response()->json([
                'result' => true,
                'message' => 'Registration successful.',
                'data' => $customer
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }
    }

    public function update(Request $request)
    {
        try {
            // Validator
            $rules = array(
                'name' => 'required|string|max:255',
            );
            $messages = array(
                'required' => 'The :attribute field is required.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $user = Customer::find(Auth::guard('api-customer')->id());
            $user->name = $request->name;
            $user->phone = $request->phone;

            if ($request->hasFile('avatar')) {
                $slug = 'customers'; //folder name
                $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
                $path = $slug . '/' . date('FY') . '/';
                \Storage::disk('public')->putFileAs(
                    $path, $request->file('avatar'), $filename
                );
                $user->avatar = $path . $filename;
            }

            $user->save();

            return response()->json([
                'result' => true,
                'message' => 'Profile updated successfully.',
                'data' => $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array()
            ]);
        }
    }

    public function login(Request $request)
    {
        try {

            // Login By Social
            if ($request->facebook_id || $request->google_id) {
                // Login By Facebook
                if ($request->facebook_id) {
                    $social = 'fb';
                } // Login By Google
                else {
                    $social = 'gg';
                }
                $user = $this->loginSocial($request, $social);

                // Return Success
                return response()->json([
                    'result' => true,
                    'message' => 'Login successfully.',
                    'data' => $user
                ]);
            }

            // Validator
            $rules = array(
                'email' => 'required|email',
                'password' => 'required'
            );
            $messages = array(
                'required' => 'The :attribute field is required.',
                'email.email' => 'The email must be a valid email address.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $selectedCustomer = Customer::where('email', $request->email)->first();
            if ($selectedCustomer) {
                if (Hash::check($request->password, $selectedCustomer->password)) {

                    if ($request->device_token) {
                        $selectedCustomer->device_token = $request->device_token;
                    }

                    if ($request->address) {
                        $selectedCustomer->address = $request->address;
                    }
                    if ($request->latitude && $request->longitude) {
                        $selectedCustomer->latitude = $request->latitude;
                        $selectedCustomer->longitude = $request->longitude;
                    }

                    $selectedCustomer->is_login = Customer::LOGIN_YES;
                    $selectedCustomer->save();
                    // Return Success
                    return response()->json([
                        'result' => true,
                        'message' => 'Login successfully.',
                        'data' => $selectedCustomer,
                        'type_data_array' => false,
                    ]);
                } else {
                    throw new Exception('Email or password is wrong.');
                }
            } else {
                throw new Exception('Email or password is wrong.');
            }
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => new \stdClass(),
                'type_data_array' => false,
            ]);
        }
    }

    public function loginSocial($request, $type)
    {
        try {
            if ($type == 'fb') {
                $customer = Customer::where('facebook_id', $request->facebook_id)->first();

            } else {
                $customer = Customer::where('google_id', $request->google_id)->first();
            }

            if (is_null($customer)) {
                $customer = Customer::firstOrNew(['email' => $request->email]);

                //check email exists
                if (!$customer->exists) {
                    $customer->email = $request->email;
                    $customer->name = $request->name;
                    $customer->avatar = $request->avatar;
                    $customer->api_token = sha1(time() . uniqid() . rand(100000000, 999999999));
                }

                if ($request->facebook_id) {
                    $customer->facebook_id = $request->facebook_id;
                }

                if ($request->google_id) {
                    $customer->google_id = $request->google_id;
                }
            }

            if ($request->address) {
                $customer->address = $request->address;
            }
            if ($request->latitude && $request->longitude) {
                $customer->latitude = $request->latitude;
                $customer->longitude = $request->longitude;
            }

            $customer->is_login = Customer::LOGIN_YES;
            $customer->device_token = $request->device_token;
            $customer->save();

            return $customer;
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data ' => array()
            ]);
        }
    }

    public function logout()
    {
        try {
            $user = Customer::find(\Auth::guard('api-customer')->id());
            $user->device_token = "";
            $user->is_login = Customer::LOGIN_NO;
            $user->save();

            // Return Data
            return response()->json([
                'result' => true,
                'message' => 'Logout successfully.'
            ]);
        } catch (Exception $e) {
            // Return Data
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getListNotification()
    {
        try {
            $notifications = Notification::where('customer_id', Auth::guard('api-customer')->id())
                ->orderBy('created_at', 'DESC')
                ->get();

            foreach ($notifications as $notification) {
                $order = Order::where('id', $notification->order_id)->first();
                $notification->order_code = $order->order_code;
                $notification->qrcode = $order->qrcode;
                $notification->time = Carbon::parse($notification->created_at)->diffForHumans();
                $notification->makeHidden(['type', 'sender']);
            }

            return response()->json([
                'result' => true,
                'message' => 'Get list notifications successfully.',
                'notifications' => $notifications
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => true,
                'message' => $e->getMessage(),
                'notifications' => []
            ]);
        }
    }

    public function deleteNotification(Request $request)
    {
        try {
            $notification = Notification::where('customer_id', Auth::guard('api-customer')->id())
                ->where('id', $request->id)
                ->first();

            if (!$notification) {
                throw new Exception("Data not found.");
            }

            $notification->delete();

            return response()->json([
                'result' => true,
                'message' => 'Delete notification successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function deleteAllNotifications()
    {
        try {
            Notification::where('customer_id', Auth::guard('api-customer')->id())
                ->delete();

            return response()->json([
                'result' => true,
                'message' => 'Delete all notifications successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateLocation(Request $request)
    {
        try {
            $user = Customer::find(Auth::guard('api-customer')->id());
            $user->address = $request->address;
            $user->latitude = $request->latitude;
            $user->longitude = $request->longitude;
            $user->save();

            return response()->json([
                'result' => true,
                'message' => 'Update location successfully.',
                'user' => $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'user' => new \stdClass()
            ]);
        }
    }

//    public function changeUserStatus(Request $request){
//        try {
//            $user = User::find(Auth::guard('api')->id());
//            if(!$user){
//                throw new Exception('User not found.');
//            }
//
//            $user->is_login = $request->is_login;
//            $user->save();
//
//            return response()->json([
//                'result' => true,
//                'message' => 'Change live status successfully.'
//            ]);
//        } catch (Exception $e) {
//            return response()->json([
//                'result' => false,
//                'message' => $e->getMessage()
//            ]);
//        }
//    }

//    public function checkUserStatus(Request $request){
//        try {
//            $user = User::find($request->id);
//            if(!$user){
//                throw new Exception('User not found.');
//            }
//
//            // Check publisher is online based on firebase
//            $firebase      = new Firebase();
//            $firebase      = $firebase->setPath('user_onlines/');
//            $value         = json_decode($firebase->get($user->id));
//
//            if (!$user->is_login || !$value || $value + 5 < time()) {
//                throw new Exception($user->name . ' is offline now.');
//            }
//
//            return response()->json([
//                'result' => true,
//                'message' => $user->name . ' is online now.'
//            ]);
//        } catch (Exception $e) {
//            return response()->json([
//                'result' => false,
//                'message' => $e->getMessage()
//            ]);
//        }
//    }

//    public function getByEmail(Request $request)
//    {
//        try {
//            $user = User::whereEmail($request->email)->first();
//
//            if (!$user) {
//                throw new Exception('Không tìm thấy người dùng.');
//            }
//
//            // Return Success
//            return response()->json([
//                'result'  => true,
//                'message' => 'Lấy thông tin người dùng thành công.',
//                'data'    => $user
//            ]);
//        } catch (Exception $e) {
//            return response()->json([
//                'result'  => false,
//                'message' => $e->getMessage(),
//                'data'    => new \stdClass()
//            ]);
//        }
//    }

//    public function deleteByEmail(Request $request)
//    {
//        try {
//            $user = User::whereEmail($request->email)->first();
//
//            if (!$user) {
//                throw new Exception('Không tìm thấy người dùng.');
//            }
//
//            $user->delete();
//
//            // Return Success
//            return response()->json([
//                'result'  => true,
//                'message' => 'Xóa người dùng thành công.'
//            ]);
//        } catch (Exception $e) {
//            return response()->json([
//                'result'  => false,
//                'message' => $e->getMessage()
//            ]);
//        }
//    }

    public function listAllOrderByCustomer(Request $request)
    {
        try {
            $listOrderByCustomer = Order::where('customer_id', Auth::guard('api-customer')->id())->with('infoCustomer')->with('infoShipper')->with('relatedShop')->with('orderItemsInOrder')->orderBy('id', 'DESC')->get();
            if (!count($listOrderByCustomer)) {
                throw new \Exception('No Orders match this customer');
            }
            return response()->json([
                'result' => true,
                'message' => 'Get list orders of this customer successfully',
                'data' => $listOrderByCustomer,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function createOrders(Request $request)
    {

        try {
//            DB::beginTransaction();
            // Validator
            $rules = array(
                'api_token' => 'required|string|max:255',
            );

            $messages = array(
                'required' => 'The :attribute field is required.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $user = Customer::where('api_token', $request->api_token)->first();

            if (!$user) {
                throw new Exception('Customer not found.');
            }

            if (!$request->data) {
                throw new Exception('No product in cart.');
            }

            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            $items = $request->data;

            $check_shop_duplicates = [];

            foreach ($items as $item) {
                $product = Product::where('id', $item['id'])->first();

                if (!$product) {
                    throw new Exception('Your products do not exists');
                }

                array_push($check_shop_duplicates, $product->shop_id);
            }

            $array_count_values = array_count_values($check_shop_duplicates);
            $orders_array = [];
            //count number of shop to store order_items
            $count_shops = count($array_count_values);

            //amount of money need to pay
            $amount = 0;

            if ($count_shops < 2) {
                $product = Product::where('id', $item['id'])->first();
                if (!$product) {
                    throw new Exception('shop do not exists');
                }
                $shop = User::where('id', $product->shop_id)->select('name', 'latitude', 'longitude', 'service_radius', 'shop_time_open', 'shop_time_close')->first();
                if (!$shop) {
                    throw new Exception('shop do not exists');
                }
                $shippers = Shipper::where('shop_id', $product->shop_id)->get();
                if (!$shippers) {
                    throw new Exception('shippers do not exists');
                }

                //check distance
                $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                if ($distance > $shop->service_radius) {
                    throw new Exception($shop->name . ' too far! You can not create this order.');
                }

                //check time
                if (!$request->local_date_time) {
                    throw new Exception('Missing local_date_time.');
                }
                $dateTimeNow = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                $now = date_format($dateTimeNow, "H:i:s");
                if ($shop->shop_time_open < $shop->shop_time_close) {
                    if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                        throw new Exception('You can not create order because shop do not service now, please try again later');
                    }
                } else {
                    if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                        throw new Exception('You can not create order because shop do not service now, please try again later');
                    }
                }


                $shop_code = "";
                foreach (explode(" ", $shop->name) as $w) {
                    $shop_code .= $w[0];
                }
                $orders = new Order();
                $orders->customer_id = $user->id;
                $orders->latitude = $request->latitude;
                $orders->longitude = $request->longitude;
                $orders->status = Order::STATUS_CREATED;
                $orders->shop_id = $product->shop_id;
                $orders->created_at =  $dateTimeNow;
                $orders->save();

                $order_code = $shop_code . $orders->id . rand(11, 99);

                if ($order_code) {
                    $slug = 'order-qr-code';
//                                $path = $slug.'/'.date('FY').'/';
                    $path = $slug . '/';
                    $file_name = date('FY') . $order_code . '.png';
                    QRCode::text($order_code)
                        ->setOutfile('storage/' . $path . $file_name)
                        ->png();
                    $order_qr_code = $path . $file_name;
                }

                //update order_code and order_qr_code to database
                $orders->order_code = $order_code;
                $store_path = setting('admin.store_path');
                $orders->qr_code = $store_path . $order_qr_code;
                Order::where('id', $orders->id)->update(['order_code' => $order_code, 'qrcode' => $order_qr_code]);

                //set message in order
                $orders->message = "New Order";
                // Push notification to shipper of own shop
                $title = 'Order #' . $order_code . ' is waiting';
                $content = "New Order";

                foreach ($shippers as $shipper) {
                    $data = array(
                        'notification' => $orders,
                        'customer' => $user,
                        'shipper' => $shipper,
                        'status' => Notification::NOTIFICATION_CREATE_ORDER,
                        'type' => 'New Order'
                    );

                    Helper::sendNotificationToShipper($shipper, $title, $content, $data);

                    $notification = new Notification();
                    $notification->customer_id = $user->id;
                    $notification->waiter_id = $shipper->id;
                    $notification->order_id = $orders->id;
                    $notification->message = $content;
                    $notification->type = Notification::NOTIFICATION_CREATE_ORDER;
                    $notification->save();
                }

                foreach ($items as $item) {
                    $product = Product::where('id', $item['id'])->first();
                    $order_items = new OrderItem();
                    $order_items->order_id = $orders->id;
                    $order_items->product_id = $item['id']; //product id
                    $order_items->quantity = $item['quantily']; //quantily of product
                    $order_items->price = $item['quantily'] * $product->price; //price of product
                    $order_items->save();
                    $amount = $amount + $order_items->price;
                }

                //update total amount to order
                Order::where('id', $orders->id)->update(['amount' => $amount]);
                $orders->amount = $amount;

                $order = Order::where('id', $orders->id)->with('infoCustomer')->first();

                $orderdata = json_decode(utf8_decode($order));

                $firebase = new Firebase();
                $firebase = $firebase->setPath('orders/');
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        'status' => Order::STATUS_CREATED
                    ]);

                array_push($orders_array, $order);
            } elseif ($count_shops >= 2) {
                foreach ($array_count_values as $key => $value) {
                    $shop = User::where('id', $product->shop_id)->select('name', 'latitude', 'longitude', 'service_radius', 'shop_time_open', 'shop_time_close')->first();

                    //check distance
                    $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                    if ($distance > $shop->service_radius) {
                        throw new Exception($shop->name . ' too far! You can not checkout this order.');
                    }

                    //check time
                    if (!$request->local_date_time) {
                        throw new Exception('Missing local_date_time.');
                    }
                    $dateTimeNow = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                    $now = date_format($dateTimeNow, "H:i:s");
                    if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                        throw new Exception('You can not checkout because shop do not service now, please try again later');
                    }

                    $shippers = Shipper::where('shop_id', $key)->get();

                    if (!$shippers) {
                        throw new Exception('shippers do not exists');
                    }
                    $shop_code = "";
                    foreach (explode(" ", $shop->name) as $w) {
                        $shop_code .= $w[0];
                    }
                    $orders = new Order();
                    $orders->customer_id = $user->id;
                    $orders->latitude = $request->latitude;
                    $orders->longitude = $request->longitude;
                    $orders->status = Order::STATUS_CREATED;
                    $orders->shop_id = $key;
                    $orders->save();

                    $order_code = $shop_code . $orders->id . rand(11, 99);

                    if ($order_code) {
                        $slug = 'order-qr-code';
//                            $path = $slug.'/'.date('FY').'/';
                        $path = $slug . '/';
                        $file_name = date('FY') . $order_code . '.png';
                        QRCode::text($order_code)
                            ->setOutfile('storage/' . $path . $file_name)
                            ->png();
                        $order_qr_code = $path . $file_name;
                    }

                    $orders->order_code = $order_code;
                    $store_path = setting('admin.store_path');
                    $orders->qr_code = $store_path . $order_qr_code;

                    Order::where('id', $orders->id)->update(['order_code' => $order_code, 'qrcode' => $order_qr_code]);

                    // Push notification to shipper of own shop


                    $title = 'Order #' . $order_code . ' is waiting';
                    $content = "New Order";

                    //set message in order
                    $orders->message = "New Order";
                    foreach ($shippers as $shipper) {
                        $data = array(
                            'notification' => $orders,
                            'customer' => $user,
                            'shipper' => $shipper,
                            'status' => Notification::NOTIFICATION_CREATE_ORDER,
                            'type' => 'order-created'
                        );

                        Helper::sendNotificationToShipper($shipper, $title, $content, $data);

                        $notification = new Notification();
                        $notification->customer_id = $user->id;
                        $notification->waiter_id = $shipper->id;
                        $notification->order_id = $orders->id;
                        $notification->message = $content;
                        $notification->type = Notification::NOTIFICATION_CREATE_ORDER;
                        $notification->save();
                    }

                    foreach ($items as $item) {
                        $product = Product::where('id', $item['id'])->first();
                        if ($product->shop_id == $key) {
                            $product = Product::where('id', $item['id'])->first();
                            $order_items = new OrderItem();
                            $order_items->order_id = $orders->id;
                            $order_items->product_id = $item['id']; //product id
                            $order_items->quantity = $item['quantily']; //quantily of product
                            $order_items->price = $item['quantily'] * $product->price; //quantily of product
                            $order_items->save();
                            $amount = $amount + $order_items->price;
                        }
                    }
                    //update total amount to order
                    Order::where('id', $orders->id)->update(['amount' => $amount]);
                    $amount = 0;
                    $orders->amount = $amount;

                    $order = Order::where('id', $orders->id)->with('infoCustomer')->first();

                    $orderdata = json_decode(utf8_decode($order));

                    $firebase = new Firebase();
                    $firebase = $firebase->setPath('orders/');
                    $firebase->set($orderdata->id,
                        [
                            $orderdata->customer_id => [
                                $orderdata->info_customer->latitude,
                                $orderdata->info_customer->longitude
                            ],
                            'status' => Order::STATUS_CREATED
                        ]);

                    array_push($orders_array, $order);

                }

            }

            // Check amount be able to pay
//            if ($request->card_id) {
//                $cards = Stripe::cards()->all($user->stripe_id);
//                if (!count($cards['data'])) {
//                    throw new Exception('Card is empty. Please add a card.');
//                }
//                $user->selectCard($request->card_id);
//            } else {
//                if ($user->balance < $amount) {
//                    throw new Exception('Your balance is not enough.');
//                }
//            }

            DB::commit();

            //phone of customer
//                $orders_array['customer_phone'] = $user->phone;
            return response()->json([
                'result' => true,
                'message' => 'Order Created.',
                'Data' => $orders_array
            ]);
        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => []
            ]);
        }

    }

    public function cancelOrderByCustomer(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }
            $customer = Customer::find(Auth::guard('api-customer')->id());
            if (!$customer) {
                throw new Exception('Customer not found.');
            }

            $order = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }

            $shippers = Shipper::where('id', $order->shipper_id)->get();

            $order->status = Order::STATUS_CUSTOMER_CANCEL;
            $order->save();

            // Push notification
            foreach ($shippers as $shipper) {
                $title = 'Order #' . $order->order_code . ' is canceled by customer';
                $content = "Order Canceled By Customer";
                $data = array(
                    'order' => $order,
                    'customer' => $customer,
                    'shipper' => $shipper,
                    'status' => Notification::NOTIFICATION_CUSTOMER_CANCEL,
                    'type' => 'order-canceled-by-customer'
                );
                Helper::sendNotificationToShipper($shipper, $title, $content, $data);

                $notification = new Notification();
                $notification->customer_id = $order->customer_id;
                $notification->waiter_id = $shipper->id;
                $notification->order_id = $request->order_id;
                $notification->message = $content;
                $notification->type = Notification::NOTIFICATION_CUSTOMER_CANCEL;
                $notification->save();
            }

            $order = Order::where('id', $request->order_id)->with('infoCustomer')->first();
            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->delete($orderdata->id);
            }

            return response()->json([
                'result' => true,
                'message' => 'customer cancled successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function checkIfCheckOutAvailable(Request $request)
    {
        try {
            if (!$request->lat_customer || !$request->lng_customer) {
                throw new Exception('Missing Lat or Lng of customer.');
            }

            if (!$request->local_time) {
                throw new Exception('Missing local_time.');
            }

            if (!$request->shop_id) {
                throw new Exception('missing shop_id.');
            }
            $shop = User::where('id', $request->shop_id)->first();
            if (!$shop) {
                throw new Exception('Shop do not exist.');
            }

            $distance = Helper::getDistanceBetweenTwoPoint($request->lng_customer, $request->lat_customer, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
            $now = date("H:i:s", strtotime($request->local_time));
            if ($distance > $shop->service_radius) {
                return response()->json([
                    'result' => false,
                    'message' => 'You can not checkout because too far from ' . $shop->name,
                    'data' => $distance,
                ]);
            }
            //check time
            if ($shop->shop_time_open < $shop->shop_time_close) {
                if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                    return response()->json([
                        'result' => false,
                        'message' => 'It is ' . $now . ' You can not checkout because shop do not service now, please try again later',
                    ]);
                }
            } else {
                if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                    return response()->json([
                        'result' => false,
                        'message' => 'It is ' . $now . ' You can not checkout because shop do not service now, please try again later',
                    ]);
                }
            }


            return response()->json([
                'result' => true,
                'message' => 'Get product successfully',
                'data' => $distance,
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result' => false,
                'data' => [],
                'message' => $e->getMessage()
            ]);
        }

    }

    public function orderAgain(Request $request){
        try {
            if (!$request->order_id) {
                throw new Exception('Missing Order Id.');
            }

            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            $old_order_item_arr = OrderItem::where('order_id',$request->order_id)->select('product_id as id','quantity as quantily')->get()->toArray();

            $user = Customer::where('api_token', $request->api_token)->first();

            if (!$user) {
                throw new Exception('Customer not found.');
            }

            if (!$old_order_item_arr) {
                throw new Exception('No product in cart.');
            }

            if (!$request->latitude || !$request->longitude) {
                throw new Exception('Missing latitude or longitude.');
            }
            $items = $old_order_item_arr;

            $check_shop_duplicates = [];

            foreach ($items as $item) {
                $product = Product::where('id', $item['id'])->first();

                if (!$product) {
                    throw new Exception('Your products do not exists');
                }

                array_push($check_shop_duplicates, $product->shop_id);
            }

            $array_count_values = array_count_values($check_shop_duplicates);
            $orders_array = [];
            //count number of shop to store order_items
            $count_shops = count($array_count_values);

            //amount of money need to pay
            $amount = 0;

            if ($count_shops < 2) {
                $product = Product::where('id', $item['id'])->first();
                if (!$product) {
                    throw new Exception('shop do not exists');
                }
                $shop = User::where('id', $product->shop_id)->select('name', 'latitude', 'longitude', 'service_radius', 'shop_time_open', 'shop_time_close')->first();
                if (!$shop) {
                    throw new Exception('shop do not exists');
                }
                $shippers = Shipper::where('shop_id', $product->shop_id)->get();
                if (!$shippers) {
                    throw new Exception('shippers do not exists');
                }

                //check distance
                $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                if ($distance > $shop->service_radius) {
                    throw new Exception($shop->name . ' too far! You can not create this order.');
                }

                //check time
                if (!$request->local_date_time) {
                    throw new Exception('Missing local_date_time.');
                }
                $dateTimeNow = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                $now = date_format($dateTimeNow, "H:i:s");
                if($shop->shop_time_open < $shop->shop_time_close)
                {
                    if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                        throw new Exception('You can not checkout because shop do not service now, please try again later');
                    }
                }
                elseif($shop->shop_time_open > $shop->shop_time_close)
                {
                    if ($shop->shop_time_open > $now && $shop->shop_time_close < $now) {
                        throw new Exception('You can not checkout because shop do not service now, please try again later');
                    }
                }


                $shop_code = "";
                foreach (explode(" ", $shop->name) as $w) {
                    $shop_code .= $w[0];
                }
                $orders = new Order();
                $orders->customer_id = $user->id;
                $orders->latitude = $request->latitude;
                $orders->longitude = $request->longitude;
                $orders->status = Order::STATUS_CREATED;
                $orders->shop_id = $product->shop_id;
                $orders->save();

                $order_code = $shop_code . $orders->id . rand(11, 99);

                if ($order_code) {
                    $slug = 'order-qr-code';
//                                $path = $slug.'/'.date('FY').'/';
                    $path = $slug . '/';
                    $file_name = date('FY') . $order_code . '.png';
                    QRCode::text($order_code)
                        ->setOutfile('storage/' . $path . $file_name)
                        ->png();
                    $order_qr_code = $path . $file_name;
                }

                //update order_code and order_qr_code to database
                $orders->order_code = $order_code;
                $store_path = setting('admin.store_path');
                $orders->qr_code = $store_path . $order_qr_code;
                Order::where('id', $orders->id)->update(['order_code' => $order_code, 'qrcode' => $order_qr_code]);

                //set message in order
                $orders->message = "New Order";
                // Push notification to shipper of own shop
                $title = 'Order #' . $order_code . ' is waiting';
                $content = "New Order";

                foreach ($shippers as $shipper) {
                    $data = array(
                        'notification' => $orders,
                        'customer' => $user,
                        'shipper' => $shipper,
                        'status' => Notification::NOTIFICATION_CREATE_ORDER,
                        'type' => 'New Order'
                    );

                    Helper::sendNotificationToShipper($shipper, $title, $content, $data);

                    $notification = new Notification();
                    $notification->customer_id = $user->id;
                    $notification->waiter_id = $shipper->id;
                    $notification->order_id = $orders->id;
                    $notification->message = $content;
                    $notification->type = Notification::NOTIFICATION_CREATE_ORDER;
                    $notification->save();
                }

                foreach ($items as $item) {
                    $product = Product::where('id', $item['id'])->first();
                    $order_items = new OrderItem();
                    $order_items->order_id = $orders->id;
                    $order_items->product_id = $item['id']; //product id
                    $order_items->quantity = $item['quantily']; //quantily of product
                    $order_items->price = $item['quantily'] * $product->price; //price of product
                    $order_items->save();
                    $amount = $amount + $order_items->price;
                }

                //update total amount to order
                Order::where('id', $orders->id)->update(['amount' => $amount]);
                $orders->amount = $amount;

                $order = Order::where('id', $orders->id)->with('infoCustomer')->first();

                $orderdata = json_decode(utf8_decode($order));

                $firebase = new Firebase();
                $firebase = $firebase->setPath('orders/');
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        'status' => Order::STATUS_CREATED
                    ]);

                array_push($orders_array, $order);
            } elseif ($count_shops >= 2) {
                foreach ($array_count_values as $key => $value) {
                    $shop = User::where('id', $product->shop_id)->select('name', 'latitude', 'longitude', 'service_radius', 'shop_time_open', 'shop_time_close')->first();

                    //check distance
                    $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                    if ($distance > $shop->service_radius) {
                        throw new Exception($shop->name . ' too far! You can not checkout this order.');
                    }

                    //check time
                    if (!$request->local_date_time) {
                        throw new Exception('Missing local_date_time.');
                    }
                    $dateTimeNow = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                    $now = date_format($dateTimeNow, "H:i:s");
                    if($shop->shop_time_open < $shop->shop_time_close)
                    {
                        if ($shop->shop_time_open > $now || $shop->shop_time_close < $now) {
                            throw new Exception('You can not checkout because shop do not service now, please try again later');
                        }
                    }
                    elseif($shop->shop_time_open > $shop->shop_time_close)
                    {
                        if ($shop->shop_time_open > $now && $shop->shop_time_close < $now) {
                            throw new Exception('You can not checkout because shop do not service now, please try again later');
                        }
                    }


                    $shippers = Shipper::where('shop_id', $key)->get();

                    if (!$shippers) {
                        throw new Exception('shippers do not exists');
                    }
                    $shop_code = "";
                    foreach (explode(" ", $shop->name) as $w) {
                        $shop_code .= $w[0];
                    }
                    $orders = new Order();
                    $orders->customer_id = $user->id;
                    $orders->latitude = $request->latitude;
                    $orders->longitude = $request->longitude;
                    $orders->status = Order::STATUS_CREATED;
                    $orders->shop_id = $key;
                    $orders->save();

                    $order_code = $shop_code . $orders->id . rand(11, 99);

                    if ($order_code) {
                        $slug = 'order-qr-code';
//                            $path = $slug.'/'.date('FY').'/';
                        $path = $slug . '/';
                        $file_name = date('FY') . $order_code . '.png';
                        QRCode::text($order_code)
                            ->setOutfile('storage/' . $path . $file_name)
                            ->png();
                        $order_qr_code = $path . $file_name;
                    }

                    $orders->order_code = $order_code;
                    $store_path = setting('admin.store_path');
                    $orders->qr_code = $store_path . $order_qr_code;

                    Order::where('id', $orders->id)->update(['order_code' => $order_code, 'qrcode' => $order_qr_code]);

                    // Push notification to shipper of own shop


                    $title = 'Order #' . $order_code . ' is waiting';
                    $content = "New Order";

                    //set message in order
                    $orders->message = "New Order";
                    foreach ($shippers as $shipper) {
                        $data = array(
                            'notification' => $orders,
                            'customer' => $user,
                            'shipper' => $shipper,
                            'status' => Notification::NOTIFICATION_CREATE_ORDER,
                            'type' => 'order-created'
                        );

                        Helper::sendNotificationToShipper($shipper, $title, $content, $data);

                        $notification = new Notification();
                        $notification->customer_id = $user->id;
                        $notification->waiter_id = $shipper->id;
                        $notification->order_id = $orders->id;
                        $notification->message = $content;
                        $notification->type = Notification::NOTIFICATION_CREATE_ORDER;
                        $notification->save();
                    }

                    foreach ($items as $item) {
                        $product = Product::where('id', $item['id'])->first();
                        if ($product->shop_id == $key) {
                            $product = Product::where('id', $item['id'])->first();
                            $order_items = new OrderItem();
                            $order_items->order_id = $orders->id;
                            $order_items->product_id = $item['id']; //product id
                            $order_items->quantity = $item['quantily']; //quantily of product
                            $order_items->price = $item['quantily'] * $product->price; //quantily of product
                            $order_items->save();
                            $amount = $amount + $order_items->price;
                        }
                    }
                    //update total amount to order
                    Order::where('id', $orders->id)->update(['amount' => $amount]);
                    $amount = 0;
                    $orders->amount = $amount;

                    $order = Order::where('id', $orders->id)->with('infoCustomer')->first();

                    $orderdata = json_decode(utf8_decode($order));

                    $firebase = new Firebase();
                    $firebase = $firebase->setPath('orders/');
                    $firebase->set($orderdata->id,
                        [
                            $orderdata->customer_id => [
                                $orderdata->info_customer->latitude,
                                $orderdata->info_customer->longitude
                            ],
                            'status' => Order::STATUS_CREATED
                        ]);

                    array_push($orders_array, $order);

                }

            }

            // Check amount be able to pay
//            if ($request->card_id) {
//                $cards = Stripe::cards()->all($user->stripe_id);
//                if (!count($cards['data'])) {
//                    throw new Exception('Card is empty. Please add a card.');
//                }
//                $user->selectCard($request->card_id);
//            } else {
//                if ($user->balance < $amount) {
//                    throw new Exception('Your balance is not enough.');
//                }
//            }

            DB::commit();

            //phone of customer
//                $orders_array['customer_phone'] = $user->phone;
            return response()->json([
                'result' => true,
                'message' => 'Order Created.',
                'Data' => $orders_array
            ]);
        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => []
            ]);
        }
    }

}
