<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use Cartalyst\Stripe\Laravel\Facades\Stripe;


class CreditCardController extends Controller
{
    public function add(Request $request)
    {
        try {
            // Validator
            $rules     = ['stripe_token' => 'required'];
            $messages  = ['required' => 'The :attribute field is required.'];
            $validator = Validator::make($request->all(), $rules, $messages);
            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first());
            }
            $stripe_token = $request->stripe_token;
            $user         = Customer::find(Auth::guard('api-customer')->id());
            $stripe_id    = $user->stripe_id;
            if (!$stripe_id || !Stripe::customers()->find($stripe_id)) {
                $stripe_customer = Stripe::customers()->create([
                    'email' => $user->email
                ]);
                $stripe_id       = $stripe_customer['id'];
                $user->update(['stripe_id' => $stripe_id]);
            }

            // create card on stripe
            $card = Stripe::cards()->create(
                $stripe_id,
                $stripe_token
            );

            $cards      = Stripe::cards()->all($user->stripe_id);
            $is_default = count($cards['data']) == 1 ? 1 : 0;

            // response
            $stripe_card             = new \stdClass();
            $stripe_card->card_id    = $card['id'];
            $stripe_card->brand      = $card['brand'];
            $stripe_card->exp_month  = $card['exp_month'];
            $stripe_card->exp_year   = $card['exp_year'];
            $stripe_card->last4      = $card['last4'];
            $stripe_card->is_default = $is_default;

            return response()->json([
                'result'  => true,
                'message' => 'Add card successfully.',
                'card'    => $stripe_card
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'card'    => new \stdClass()
            ]);
        }
    }

    public function setDefault(Request $request)
    {
        try {
            // Validator
            $rules     = ['card_id' => 'required'];
            $messages  = ['required' => 'The :attribute field is required.'];
            $validator = Validator::make($request->all(), $rules, $messages);
            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first());
            }
            $user = Customer::find(Auth::guard('api-customer')->id());
            $user->selectCard($request->card_id);

            return response()->json([
                'result'  => true,
                'message' => 'Set card default successfully.',
            ]);
        } catch (\Exception $e) {

            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function remove(Request $request)
    {
        try {
            // Validator
            $rules     = ['card_id' => 'required'];
            $messages  = ['required' => 'The :attribute field is required.'];
            $validator = Validator::make($request->all(), $rules, $messages);
            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($validator->errors()->first());
            }
            $user      = Customer::find(Auth::guard('api-customer')->id());
            $stripe_id = $user->stripe_id;

            Stripe::cards()->delete($stripe_id, $request->card_id);

            Cashout::where('card_id', $request->card_id)->delete();

            return response()->json([
                'result'  => true,
                'message' => 'Delete card successfully.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getList()
    {
        try {
            $user         = Customer::find(Auth::guard('api-customer')->id());
            $cards        = Stripe::cards()->all($user->stripe_id);
            $stripe_cards = array();
            foreach ($cards['data'] as $key => $card) {
                $stripe_card             = new \stdClass();
                $stripe_card->card_id    = $card['id'];
                $stripe_card->brand      = $card['brand'];
                $stripe_card->exp_month  = $card['exp_month'];
                $stripe_card->exp_year   = $card['exp_year'];
                $stripe_card->last4      = $card['last4'];
                $stripe_card->is_default = ($key == 0) ? 1 : 0;
                array_push($stripe_cards, $stripe_card);
            }

            return response()->json([
                'result'  => true,
                'message' => 'Get your list cards successfully.',
                'cards'   => $stripe_cards
            ]);
        } catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            return response()->json([
                'result'  => true,
                'message' => 'List card is empty. Please add a card.',
                'cards'   => []
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'cards'   => []
            ]);
        }
    }

    public function getBalance()
    {
        try {
            $user    = Customer::find(Auth::guard('api-customer')->id());
            $balance = doubleval($user->balance);

            return \Response::json([
                'result'  => true,
                'message' => 'Get your balance successfully.',
                'balance' => $balance
            ]);
        } catch (\Exception $e) {
            return \Response::json([
                'result'  => false,
                'message' => $e->getMessage(),
                'balance' => 0
            ]);
        }
    }
}
