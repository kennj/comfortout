<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\ParentCategory;
use App\Category;
use App\Product;
use App\User;
use Dompdf\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentCategoryController extends Controller
{
    //
    public function showAllParentCategory()
    {
        try {
            $all_parent_category = ParentCategory::all();
            if(!count($all_parent_category)) {
                throw new Exception('No parent category available.');
            }
            return response() ->json ([
                'result'=>true,
                'message' => 'Get list parent category successfully',
                'data' => $all_parent_category,
                'type_data_array' => true,
            ]);
        } catch (Exception $e) {
            return response() ->json([
                'result' => false,
                'message' => $e->getMessage(),
                'data' => array(),
                'type_data_array' =>true,
            ]);
        }
    }

    public function showCategoriesByParentCategory(Request $request)
    {
        try {

            {
                if (!$request->latitude || !$request->longitude) {
                    throw new Exception('Missing latitude or longitude.');
                }

                if(!$request->parent_category_id) {
                    throw new Exception('Missing parent_category_id.');
                }

                if (!$request->local_time) {
                    throw new Exception('Missing local_time.');
                }

                $shops = User::where('role_id', 2)->get();
                $shopInRange = [];
                if(count($shops))
                {
                    foreach ($shops as $shop) {
                        $productByShop = Product::where('shop_id',$shop->id)->get();
                        if(count($productByShop)) {
                            $distance = Helper::getDistanceBetweenTwoPoint($request->longitude, $request->latitude, $shop->longitude, $shop->latitude, setting('admin.distance_unit'));
                            if ($distance <= $shop->service_radius) {
                                //Check time
                                //$now = Carbon::now()->toTimeString();
                                $now = date("H:i:s", strtotime($request->local_time));
                                if ($shop->shop_time_open < $shop->shop_time_close) {
                                    if ($shop->shop_time_open < $now && $shop->shop_time_close > $now) {
                                        $shopInRange = $shop;

                                    }
                                } else {
                                    if ($shop->shop_time_open < $now || $shop->shop_time_close > $now) {
                                        $shopInRange = $shop;
                                    }
                                }
                            }
                        }
                    }
                }

                if(!$shopInRange){

                    return response() -> json([
                        'result' =>true,
                        'message' => 'Get list categories successfully. No shop available',
                        'data' => [],
                        'type_data_array' => true,
                    ]);

                }

                $productOfShopInRange = Product::where('shop_id',$shopInRange->id)->get();

                if(!count($productOfShopInRange)){

                    return response() -> json([
                        'result' =>true,
                        'message' => 'Get list categories successfully. No product available',
                        'data' => [],
                        'type_data_array' => true,
                    ]);

                }
                $category_ids = [];
                foreach ($productOfShopInRange as $product)
                {
                    $productCategory = Category::where('id',$product->category_id)->first();
                    if(count($productCategory))
                    {
                        if(!in_array($productCategory->id, $category_ids, true) && $productCategory->parent_id == $request->parent_category_id){
                            array_push($category_ids, $productCategory->id);
                        }
                    }

                }
            $category = Category::findMany($category_ids);

            }
            if(!count($category)){
                throw new Exception('No item category available');
            }

            return response() -> json([
                'result' =>true,
                'message' => 'Get list categories successfully',
                'data' => $category,
                'type_data_array' => true,
            ]);
        } catch(Exception $e) {
            return response()->json([
                'result' => false,
                'message'=> $e->getMessage(),
                'data' =>array(),
                'type_data_array' =>true,
            ]);
        }
    }
}
