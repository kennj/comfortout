<?php

namespace App\Http\Controllers;

use app\Helpers\Constants;
use App\Customer;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceToCustomer;
use App\Models\Notification;
use App\Notifications;
use App\Shipper;
use App\User;
use Carbon\Carbon;
use Geckob\Firebase\Firebase;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\Exception;
use App\Order;
use App\Helpers\Helper;

class ShipperController extends Controller
{

    public function getAll()
    {
        try {
            /* @var $shipper Shipper */
            $shipper = Shipper::all();
            if (!count($shipper)) {
                throw new \Exception('No Customer in list');
            }

            return response()->json([
                'result'          => true,
                'message'         => 'Get list customers info successfully',
                'data'            => $shipper,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => false,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }
    }

    public function getbyID($id)
    {
        try {
            /* @var $customer Customer */
            $customer = Shipper::where('id', $id)->first();
            if (!count($customer)) {
                throw new \Exception('No Customer match ID');
            }

            return response()->json([
                'result'          => true,
                'message'         => 'Get customer info successfully',
                'data'            => $customer,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => false,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function register(Request $request)
    {
        try {
            if (!$request->shop_id) {
                throw new Exception('missing shop_id.');
            }
            // Validator
            $rules    = array(
                'name'     => 'required|string|max:255',
                'email'    => 'required|string|email|max:255|unique:shippers',
                'phone'    => 'required|max:13',
                'password' => 'required|string|min:6',
            );
            $messages = array(
                'required'    => 'The :attribute field is required.',
                'email.email' => 'The email must be a valid email address.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            // Check Email
            if (count(Shipper::where('email', $request->email)->get())) {
                throw new Exception('Email already exists.');
            }

            $shipper           = new Shipper();
            $shipper->email    = $request->email;
            $shipper->name     = $request->name;
            $shipper->phone    = $request->phone;
            $shipper->shop_id  = $request->shop_id;
            $shipper->password = \Hash::make($request->password);
            $shipper->status   = 0;

            $shipper->is_login = Shipper::LOGIN_YES;

            $shipper->api_token = sha1(time() . uniqid() . rand(100000000, 999999999));

            $shipper->device_token = $request->device_token;

            $slug = 'shippers';
            if ($request->hasFile('avatar')) {
                $slug = 'shippers';
                $path = $slug . '/' . date('FY') . '/';

                $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
                \Storage::disk('public')->putFileAs(
                    $path, $request->file('avatar'), $filename
                );
                $shipper->avatar = $path . $filename;
            } else {

                $shipper->avatar = $slug . '/default.png';
            }

            $shipper->save();

            return response()->json([
                'result'  => true,
                'message' => 'Registration successful.',
                'data'    => $shipper
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'data'    => array()
            ]);
        }
    }

    public function update(Request $request)
    {
        try {
            // Validator
            $rules    = array(
                'name' => 'required|string|max:255',
            );
            $messages = array(
                'required' => 'The :attribute field is required.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $user = Shipper::find(Auth::guard('api-shipper')->id());

            if ($request->name) {
                $user->name = $request->name;
            }
            if ($request->phone) {
                $user->phone = $request->phone;
            }

            if ($request->hasFile('avatar')) {
                $slug     = 'shippers'; //folder name
                $filename = date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('avatar')->getClientOriginalExtension());
                $path     = $slug . '/' . date('FY') . '/';
                \Storage::disk('public')->putFileAs(
                    $path, $request->file('avatar'), $filename
                );
                $user->avatar = $path . $filename;
            }

            $user->save();

            return response()->json([
                'result'  => true,
                'message' => 'Profile updated successfully.',
                'data'    => $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'data'    => array()
            ]);
        }
    }

    public function login(Request $request)
    {
        try {
            // Validator
            $rules    = array(
                'email'    => 'required|email',
                'password' => 'required'
            );
            $messages = array(
                'required'    => 'The :attribute field is required.',
                'email.email' => 'The email must be a valid email address.',
            );

            $validator = Validator::make($request->all(), $rules, $messages);

            // Check Validator Fails
            if ($validator->fails()) {
                throw new Exception($errors = $validator->errors());
            }

            $selectedShipper = Shipper::where('email', $request->email)->first();
            if ($selectedShipper) {
                if ($selectedShipper->status == 0) {
                    throw new Exception('You can not login now. Please wait shop approve your account.');
                }
                if (Hash::check($request->password, $selectedShipper->password)) {

                    if ($request->device_token) {
                        $selectedShipper->device_token = $request->device_token;
                    }
                    $selectedShipper->is_login = Shipper::LOGIN_YES;
                    $selectedShipper->shop_id  = $selectedShipper->shop_id;

                    $selectedShipper->save();

                    // Return Success
                    return response()->json([
                        'result'          => true,
                        'message'         => 'Login successfully.',
                        'data'            => $selectedShipper,
                        'type_data_array' => false,
                    ]);
                } else {
                    throw new Exception('Email or password is wrong.');
                }
            } else {
                throw new Exception('Email or password is wrong.');
            }
        } catch (Exception $e) {
            return response()->json([
                'result'          => false,
                'message'         => $e->getMessage(),
                'data'            => new \stdClass(),
                'type_data_array' => false,
            ]);
        }
    }

    public function logout()
    {
        try {
            $user               = Shipper::find(\Auth::guard('api-shipper')->id());
            $user->device_token = "";
            $user->is_login     = Shipper::LOGIN_NO;
            $user->save();

            // Return Data
            return response()->json([
                'result'  => true,
                'message' => 'Logout successfully.'
            ]);
        } catch (Exception $e) {
            // Return Data
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getListNotification()
    {
        try {
            $notifications = Notification::where('waiter_id', Auth::guard('api-shipper')->id())
                ->orderBy('created_at', 'DESC')
                ->get();
            if (!$notifications) {
                throw new Exception("Notification not found.");
            }
            foreach ($notifications as $notification) {
                $order                    = Order::where('id', $notification->order_id)->first();
                $notification->order_code = $order->order_code;
                $notification->qrcode     = $order->qrcode;
                $notification->time       = Carbon::parse($notification->created_at)->diffForHumans();
                $notification->makeHidden(['type', 'sender']);
            }

            return response()->json([
                'result'        => true,
                'message'       => 'Get list notifications successfully.',
                'notifications' => $notifications
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'        => false,
                'message'       => $e->getMessage(),
                'notifications' => []
            ]);
        }
    }

    public function deleteNotification(Request $request)
    {
        try {
            $notification = Notification::where('waiter_id', Auth::guard('api-shipper')->id())
                ->where('id', $request->id)
                ->first();

            if (!$notification) {
                throw new Exception("Data not found.");
            }

            $notification->delete();

            return response()->json([
                'result'  => true,
                'message' => 'Delete notification successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function deleteAllNotifications()
    {
        try {
            Notification::where('waiter_id', Auth::guard('api-shipper')->id())
                ->delete();

            return response()->json([
                'result'  => true,
                'message' => 'Delete all notifications successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => true,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function updateLocation(Request $request)
    {
        try {
            $shipper            = Shipper::find(Auth::guard('api-shipper')->id());
            $shipper->latitude  = $request->latitude;
            $shipper->longitude = $request->longitude;
            $shipper->save();

            return response()->json([
                'result'  => true,
                'message' => 'Update location successfully.',
                'user'    => $shipper
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'user'    => new \stdClass()
            ]);
        }
    }

    public function changeShipperStatus(Request $request)
    {
        try {
            $user = Shipper::find(Auth::guard('api-shipper')->id());
            if (!$user) {
                throw new Exception('Shipper not found.');
            }

            $user->is_login = $request->is_login;
            $user->save();

            return response()->json([
                'result'  => true,
                'message' => 'Change shipper status successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function checkShipperStatus(Request $request)
    {
        try {
            $user = Shipper::find($request->id);
            if (!$user) {
                throw new Exception('User not found.');
            }

            // Check publisher is online based on firebase
            $firebase = new Firebase();
            $firebase = $firebase->setPath('user_onlines/');
            $value    = json_decode($firebase->get($user->id));

            if (!$user->is_login || !$value || $value + 5 < time()) {
                throw new Exception($user->name . ' is offline now.');
            }

            return response()->json([
                'result'  => true,
                'message' => $user->name . ' is online now.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getByEmail(Request $request)
    {
        try {
            $user = Shipper::whereEmail($request->email)->first();

            if (!$user) {
                throw new Exception('Không tìm thấy người dùng.');
            }

            // Return Success
            return response()->json([
                'result'  => true,
                'message' => 'Lấy thông tin người dùng thành công.',
                'data'    => $user
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage(),
                'data'    => new \stdClass()
            ]);
        }
    }

    public function deleteByEmail(Request $request)
    {
        try {
            $user = Shipper::whereEmail($request->email)->first();

            if (!$user) {
                throw new Exception('Không tìm thấy người dùng.');
            }

            $user->delete();

            // Return Success
            return response()->json([
                'result'  => true,
                'message' => 'Xóa người dùng thành công.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function listAllOrder(Request $request)
    {
        try {
            $listOrderByShipper = Order::where('shipper_id', Auth::guard('api-shipper')->id())->with('relatedCustomer')->with('orderItemsInOrder')->get();
            if (!count($listOrderByShipper)) {
                throw new \Exception('No Orders match shipper_id');
            }
            return response()->json([
                'result'          => true,
                'message'         => 'Get list orders successfully',
                'data'            => $listOrderByShipper,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => false,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function listAllOrderOfShopNotPickUp(Request $request)
    {
        try {
            $listAllOrderOfShopNotPickUp = Order::where('shop_id', Auth::guard('api-shipper')->user()->shop_id)->where('status', 1)->with('relatedCustomer')->with('orderItemsInOrder')->orderBy('id', 'DESC')->get();
            if (!count($listAllOrderOfShopNotPickUp)) {
                return response()->json([
                    'result'          => true,
                    'message'         => 'Get list orders successfully',
                    'data'            => array(),
                    'type_data_array' => true,
                ]);
            }

            if (!$request->local_date_time) {
                throw new Exception('Missing local_date_time.');
            }

            $shop                = User::find(Auth::guard('api-shipper')->user()->shop_id);
            $openTime            = $shop->shop_time_open;
            $closeTime           = $shop->shop_time_close;
            $arrayOrderNotPickUp = [];
            foreach ($listAllOrderOfShopNotPickUp as $order) {
                $orderDate = date_format($order->created_at, "Y-m-d");
                $now = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                $dateNow   = date_format($now, "Y-m-d");
                $timeNow   = date_format($now, "H:i:s");
                if ($dateNow == $orderDate) {
                    if ($openTime < $closeTime) {
                        if ($timeNow > $openTime && $timeNow < $closeTime) {
                            array_push($arrayOrderNotPickUp, $order);
                        }
                    } else {
                        if ($timeNow < $openTime || $timeNow > $closeTime) {
                            array_push($arrayOrderNotPickUp, $order);
                        }
                    }
                }
            }

            return response()->json([
                'result'          => true,
                'message'         => 'Get list orders successfully',
                'data'            => $arrayOrderNotPickUp,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => true,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function listAllOrderOfShopPickUpInDay(Request $request)
    {
        try {
            $listAllOrderOfShopPickUpInDay = Order::where('status', '!=', 1)->where('status', '!=', 3)->where('status', '!=', 9)->where('shop_id', Auth::guard('api-shipper')->user()->shop_id)->with('relatedCustomer')
                ->where('shipper_id', Auth::guard('api-shipper')->user()->id)->with('relatedCustomer')
                ->with('orderItemsInOrder')->orderBy('id', 'DESC')->get();

            if (!count($listAllOrderOfShopPickUpInDay)) {
                throw new \Exception('No Orders match shipper_id');
            }

            if (!$request->local_date_time) {
                throw new Exception('Missing local_date_time.');
            }

            $shop                     = User::find(Auth::guard('api-shipper')->user()->shop_id);
            $openTime                 = $shop->shop_time_open;
            $closeTime                = $shop->shop_time_close;
            $arrayOrderNotPickUpInDay = [];
            foreach ($listAllOrderOfShopPickUpInDay as $order) {
                $orderDate = date_format($order->created_at, "Y-m-d");
                $dateTimeNow = Carbon::createFromFormat('Y-m-d H:i:s', $request->local_date_time);
                $dateNow   = date_format($dateTimeNow, "Y-m-d");
                $timeNow   = date_format($dateTimeNow, "H:i:s");
                if ($dateNow == $orderDate) {
                    if ($openTime < $closeTime) {
                        if ($timeNow > $openTime && $timeNow < $closeTime) {
                            array_push($arrayOrderNotPickUpInDay, $order);
                        }
                    } else {
                        if ($timeNow < $openTime || $timeNow > $closeTime) {
                            array_push($arrayOrderNotPickUpInDay, $order);
                        }
                    }
                }
            }

            return response()->json([
                'result'          => true,
                'message'         => 'Get list orders successfully',
                'data'            => $arrayOrderNotPickUpInDay,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => true,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function listAllOrderOfShopPickUp(Request $request)
    {
        try {
//            $listAllOrderOfShopPickUp = Order::where('status','!=',1)->where('status','!=',9)->where('shop_id',Auth::guard('api-shipper')->user()->shop_id)->with('relatedCustomer')
            $listAllOrderOfShopPickUp = Order::where('status', 7)->where('shop_id', Auth::guard('api-shipper')->user()->shop_id)->with('relatedCustomer')
                ->where('shipper_id', Auth::guard('api-shipper')->user()->id)->with('relatedCustomer')
                ->with('orderItemsInOrder')->orderBy('id', 'DESC')->get();
            if (!count($listAllOrderOfShopPickUp)) {
                throw new \Exception('No Orders match shipper_id');
            }
            return response()->json([
                'result'          => true,
                'message'         => 'Get list orders successfully',
                'data'            => $listAllOrderOfShopPickUp,
                'type_data_array' => true,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'result'          => true,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }

    public function approveOrder(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }
            $shipper = Shipper::find(Auth::guard('api-shipper')->id());
            $order   = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }
            if (!$request->shipping_estimate_time) {
                throw new Exception('missing shipping_estimate_time.');
            }

            $customer = Customer::find($order->customer_id);

            //update Order status
            $order->status                 = Order::STATUS_APPROVED;
            $order->shipper_id             = $shipper->id;
            $order->shipping_estimate_time = $request->shipping_estimate_time;
            $order->save();

            //set message in order
            $order->message = "Order has been accepted";

            // Push notification
            $title   = $shipper->name . ' has been accepted';
            $content = "Order has been accepted";
            $data    = array(
                'notification' => $order,
                'customer'     => $customer,
                'shipper'      => $shipper,
                'status'       => Notification::NOTIFICATION_APPROVE_ORDER,
                'type'         => 'order-approved'
            );
            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

            $notification              = new Notification();
            $notification->customer_id = $order->customer_id;
            $notification->waiter_id   = $shipper->id;
            $notification->order_id    = $request->order_id;
            $notification->message     = $content;
            $notification->type        = Notification::NOTIFICATION_APPROVE_ORDER;
            $notification->save();

            $order     = Order::where('id', $request->order_id)->with('infoShipper')->with('infoCustomer')->first();
            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value    = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        $orderdata->shipper_id  => [
                            $orderdata->info_shipper->latitude,
                            $orderdata->info_shipper->longitude
                        ],
                        'status'                => Order::STATUS_APPROVED
                    ]);
            }

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'shipper approve order successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function cancelOrder(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }
            $shipper = Shipper::find(Auth::guard('api-shipper')->id());
            $order   = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }
            $customer = Customer::find($order->customer_id);

            $order->status = Order::STATUS_CANCELLED;
            $order->save();

            $order->message = "Order Cancled";
            // Push notification to customers
            $title   = $shipper->name . ' has cancel your order request. Please wait other shipper pickup';
            $content = "Order Canceled By Shipper";
            $data    = array(
                'notification' => $order,
                'customer'     => $customer,
                'shipper'      => $shipper,
                'status'       => Notification::NOTIFICATION_CANCEL_ORDER,
                'type'         => 'order-cancled'
            );
            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

            $notification              = new Notification();
            $notification->customer_id = $order->customer_id;
            $notification->waiter_id   = $shipper->id;
            $notification->order_id    = $request->order_id;
            $notification->message     = $content;
            $notification->type        = Notification::NOTIFICATION_CANCEL_ORDER;
            $notification->save();

            // Push notification to other shippers
//            $title   = $shipper->name . ' has cancel your order request. order back to not pickup list';
//            $content = "Order Canceled By Shipper";
//            $data    = array(
//                'notification' => $order,
//                'customer'     => $customer,
//                'shipper'      => $shipper,
//                'status'       => Notification::NOTIFICATION_CREATE_ORDER,
//                'type'         => 'order-recreated'
//            );
//
//            Helper::sendNotificationToShipper($shipper, $title, $content, $data);
//
//            $notification              = new Notification();
//            $notification->customer_id = $order->customer_id;
//            $notification->waiter_id   = $shipper->id;
//            $notification->order_id    = $request->order_id;
//            $notification->message     = $content;
//            $notification->type        = Notification::NOTIFICATION_CREATE_ORDER;
//            $notification->save();

            $order = Order::where('id', $request->order_id)->with('infoShipper')->with('infoCustomer')->first();

            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value    = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        $orderdata->shipper_id  => [
                            $orderdata->info_shipper->latitude,
                            $orderdata->info_shipper->longitude
                        ],
                        'status'                => Order::STATUS_CANCELLED
                    ]);
            }

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'shipper cancel order successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function deliveringOrder(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }
            $shipper = Shipper::find(Auth::guard('api-shipper')->id());
            $order   = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }
            $customer = Customer::find($order->customer_id);


            $order->status = Order::STATUS_DELIVERING;
            $order->save();

            $order->message = "Order is delivering";
            // Push notification
            $title   = $shipper->name . ' is delivering your order request.';
            $content = "Order is delivering";
            $data    = array(
                'notification' => $order,
                'customer'     => $customer,
                'shipper'      => $shipper,
                'status'       => Notification::NOTIFICATION_DELIVERING_ORDER,
                'type'         => 'order-delivering'
            );
            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

            $notification              = new Notification();
            $notification->customer_id = $order->customer_id;
            $notification->waiter_id   = $shipper->id;
            $notification->order_id    = $request->order_id;
            $notification->message     = $content;
            $notification->type        = Notification::NOTIFICATION_DELIVERING_ORDER;
            $notification->save();

            $order = Order::where('id', $request->order_id)->with('infoShipper')->with('infoCustomer')->first();

            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value    = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        $orderdata->shipper_id  => [
                            $orderdata->info_shipper->latitude,
                            $orderdata->info_shipper->longitude
                        ],
                        'status'                => Order::STATUS_DELIVERING
                    ]);
            }

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'Order Delivering.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function completeOrder(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }
            $shipper = Shipper::find(Auth::guard('api-shipper')->id());
            $order   = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }
            $customer = Customer::find($order->customer_id);

            $order->status = Order::STATUS_COMPLETED;
            $order->save();

            $order->message = "Order has been completed";
            // Push notification
            $title   = 'Order #' . $order->order_code . ' has been completed';
            $content = "Order has been completed";
            $data    = array(
                'notification' => $order,
                'customer'     => $customer,
                'shipper'      => $shipper,
                'status'       => Notification::NOTIFICATION_COMPLETE_ORDER,
                'type'         => 'order-completed'
            );
//            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

//            $notification = new Notification();
//            $notification->customer_id = $order->customer_id;
//            $notification->waiter_id = $shipper->id;
//            $notification->order_id = $request->order_id;
//            $notification->message = $content;
//            $notification->type = Notification::NOTIFICATION_COMPLETE_ORDER;
//            $notification->save();

            $order = Order::where('id', $request->order_id)->with('infoShipper')->with('infoCustomer')->first();

            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value    = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        $orderdata->shipper_id  => [
                            $orderdata->info_shipper->latitude,
                            $orderdata->info_shipper->longitude
                        ],
                        'status'                => Order::STATUS_COMPLETED
                    ]);
            }

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'shipper complete order successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function completePaymentAndTip(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }

            if (!$request->tip_amount) {
                throw new Exception('missing tip_amount.');
            }
            if (!$request->hasFile('signature')) {
                throw new Exception('missing signature.');
            }

            $order = Order::find($request->order_id);
            if (!$order) {
                throw new Exception('Order not found.');
            }

            $shipper = Shipper::find(Auth::guard('api-shipper')->id());

            $customer = Customer::find($order->customer_id);

            //upload signature
            $slug     = 'signatures'; //folder name
            $filename = $order->order_code . date('Y-m-d-H-i-s') . '-' . uniqid() . "." . strtolower($request->file('signature')->getClientOriginalExtension());
            $path     = $slug . '/' . date('FY') . '/';
            \Storage::disk('public')->putFileAs(
                $path, $request->file('signature'), $filename
            );
            $order->signature = $path . $filename;

            //update tips amount
            $order->tips   = $request->tip_amount;
            $order->status = Order::STATUS_ORDER_FINISH_PAYMENT_TIPS;
            $order->save();

            //update total tips of shipper
            $shipper->total_tips += $request->tip_amount;
            $shipper->save();

            $order->message = "Order has been paid and tipped";
            // Push notification to customer
            $title   = 'Order #' . $order->order_code . ' has been paid and tipped';
            $content = "Order has been paid and tipped";
            $data    = array(
                'notification' => $order,
                'customer'     => $customer,
                'status'       => Notification::NOTIFICATION_FINISH_PAYMENT_TIPS,
                'type'         => 'order-completed-payment'
            );

//            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

//            $notification = new Notification();
//            $notification->customer_id = $order->customer_id;
//            $notification->waiter_id = $shipper->id;
//            $notification->order_id = $request->order_id;
//            $notification->message = $content;
//            $notification->type = Notification::NOTIFICATION_FINISH_PAYMENT_TIPS;
//            $notification->save();

            $order = Order::where('id', $request->order_id)->with('infoShipper')->with('infoCustomer')->with('orderItemsInOrder')->first();

            $orderdata = json_decode(utf8_decode($order));

            $firebase = new Firebase();
            $firebase = $firebase->setPath('orders/');
            $value    = json_decode($firebase->get($orderdata->id));
            if ($value) {
                $firebase->set($orderdata->id,
                    [
                        $orderdata->customer_id => [
                            $orderdata->info_customer->latitude,
                            $orderdata->info_customer->longitude
                        ],
                        $orderdata->shipper_id  => [
                            $orderdata->info_shipper->latitude,
                            $orderdata->info_shipper->longitude
                        ],
                        'status'                => Order::STATUS_ORDER_FINISH_PAYMENT_TIPS
                    ]);
            }

            //send invoice to customer
            $mailable = new InvoiceToCustomer($order);
            Mail::to($customer->email)->send($mailable);

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'order finish payment and tips.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    public function checkDistanceOrderCustomer(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new \Exception('Missing Order ID');
            }

            $order = Order::where('id', $request->order_id)->first();

            if (!$order) {
                throw new \Exception('Wrong order_id');
            }

            $shop = User::where('id', $order->shop_id)->first();

            $customer = Customer::where('id', $order->customer_id)->first();

            $shipper = Shipper::find($order->shipper_id);

            $order->message = "Your order has arrived";
            if (!$shipper->shipper_music) {
                $order->music = $shop->shipper_music;

            } else
                $order->music = $shipper->shipper_music;

            // Push notification
            $title   = 'Your order has arrived';
            $content = 'Your order has arrived';
            $data    = array(
                'status'       => Constants::NOTIFICATION_NEAR_BY,
                'type'         => 'order-arrived',
                'notification' => $order
            );

            Helper::sendNotificationToCustomer($customer, $title, $content, $data);

            return response()->json([
                'result'          => true,
//                'message'         => 'Your order has arrived',
                'data'            => $data,
                'type_data_array' => true,
            ]);

        } catch (\Exception $e) {
            return response()->json([
                'result'  => false,
//                'message' => $e->getMessage(),
                'data'    => array(),
            ]);
        }

    }

    //delete order in history (change status to 9)
    public function deleteHistoryOrder(Request $request)
    {
        try {
            if (!$request->order_id) {
                throw new Exception('missing order_id.');
            }

            $order = Order::find($request->order_id);

            if (!$order) {
                throw new Exception('Order not found.');
            }

            $order->status = Order::STATUS_HISTORY_DELETED;
            $order->save();

            $order->message = "Order Deleted";

            return response()->json([
                'result'  => true,
                'data'    => $order,
                'message' => 'Delete order successfully.'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'result'  => false,
                'data'    => [],
                'message' => $e->getMessage()
            ]);
        }
    }

    //count tip daily, weeky, month
    public function tipCount(Request $request)
    {
        try {
            $shipper = Shipper::find(Auth::guard('api-shipper')->id());
            $data    = [];
            if (!$request->date_type) {
                throw new Exception('missing date_type.');
            }
            if ($request->date_type == 1) {
                $all_orders = Order::where('shipper_id', $shipper->id)->where('status', 7)->orWhere('status',9)
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('d-m-Y'); // grouping by days
                        //return Carbon::parse($date->created_at)->format('m'); // grouping by months

                    });
            } elseif ($request->date_type == 7) {
                $all_orders = Order::where('shipper_id', $shipper->id)->where('status', 7)->orWhere('status',9)
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('W-Y'); // grouping by weeks
                        //return Carbon::parse($date->created_at)->format('m'); // grouping by months
                    });
            } elseif ($request->date_type == 30) {
                $all_orders = Order::where('shipper_id', $shipper->id)->where('status', 7)->orWhere('status',9)
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('m-Y'); // grouping by months
                    });
            } elseif ($request->date_type == 365) {
                $all_orders = Order::where('shipper_id', $shipper->id)->where('status', 7)->orWhere('status',9)
                    ->get()
                    ->groupBy(function ($date) {
                        return Carbon::parse($date->created_at)->format('Y'); // grouping by years
                    });
            } else {
                throw new Exception('wrong date_type.');
            }

            foreach ($all_orders as $key => $order) {
                $item         = new \stdClass();
                $item->type   = $request->date_type;
                $item->time   = $key;
                $item->orders = $order;
                array_push($data, $item);
            }

            return response()->json([
                'result'  => true,
                'data'    => $data,
                'message' => 'get list order history successfully.'
            ]);


        } catch (\Exception $e) {
            return response()->json([
                'result'          => false,
                'message'         => $e->getMessage(),
                'data'            => array(),
                'type_data_array' => true,
            ]);
        }

    }
}
