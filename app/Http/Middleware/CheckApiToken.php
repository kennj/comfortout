<?php

namespace App\Http\Middleware;

use Closure;

class CheckApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            //Check user exists?
            if (empty($request->api_token)) {
                throw new \Exception('api_token is not set.');
            }

            //Check user exists?
            if (\Auth::guard('api')->id() === null) {
                throw new \Exception('api_token is invalid.');
            }

            return $next($request);

        } catch (\Exception $e) {
            return response()->json([
                'result' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
