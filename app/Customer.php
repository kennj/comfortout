<?php

namespace App;

use App\Notifications\CustomerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class Customer extends Authenticatable
{
    use Notifiable;

    const LOGIN_YES = 1;
    const LOGIN_NO = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'latitude',
        'longitude',
        'stripe_id',
        'facebook_id',
        'google_id',
        'avatar',
        'address',
        'device_token',
        'api_token',
        'balance',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */

//    public function getAvatar()
//    {
//        if ($this->avatar) {
//            if (strpos($this->avatar, "https://") === false) {
//                if (!\Storage::disk('public')->exists($this->avatar)) {
//                    $this->avatar = asset(\Storage::disk('public')->url('/default.png'));
//                } else {
//                    $this->avatar = asset(\Storage::disk('public')->url($this->avatar));
//                }
//            }
//        } else {
//            $this->avatar = asset(\Storage::disk('public')->url('/default.png'));
//        }
//
//        return $this->avatar;
//    }

    public function card($card_id)
    {
        return Stripe::cards()->find($this->stripe_id, $card_id);
    }

    public function selectCard($card_id)
    {
        return Stripe::customers()->update($this->stripe_id, ['default_source' => $card_id]);
    }

    public function checkCard($card_id)
    {
        try {
            return Stripe::cards()->find($this->stripe_id, $card_id);
        } catch (\Cartalyst\Stripe\Exception\NotFoundException $e) {
            throw new Exception('Card not found.');
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }

    public function getAvatarAttribute($path){
        if(!strpos($path,'googleusercontent') && !strpos($path,'facebook'))
        {
            $store_path = setting('admin.store_path');
            return $store_path.$path;
        }
        return $path;
    }
}
