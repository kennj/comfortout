<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Customer;
use Illuminate\Support\Facades\Auth;

/**
 * Class Order
 * @package App
 */
class Order extends Model
{
    const STATUS_CREATED = 1; //customer create, wait for Shipper Approve
    const STATUS_APPROVED = 2; //Shipper picked, send nortification to Customer.
    const STATUS_CANCELLED = 3; //Shipper Cancle, send nortification to Customer.
    const STATUS_DELIVERING = 4; //Shipper bring product to Customer
    const STATUS_COMPLETED = 5; //Shipper complete order.
    const STATUS_CUSTOMER_CANCEL = 6; //Customer cancle orders
    const STATUS_ORDER_FINISH_PAYMENT_TIPS = 7; //Customer pay for orders and tips
    const STATUS_HISTORY_DELETED = 9; //remove order from history

    protected $casts = [
        'customer_id'      => 'int',
        'product_id' => 'int',
        'status'       => 'int',
        'shop_id' =>'int',
        'order_code'=>'string',
        'signature'=>'string'
    ];

    protected $fillable = [
        'amount',
        'status',
        'latitude',
        'longitude',
        'status',
        'amount',
        'tips'
    ];

    public function relatedCustomer()
    {
        $related_customer = $this->belongsTo(Customer::class, 'customer_id','id')->select(['id', 'name', 'address','phone']);
        return $related_customer;
    }

    public function orderItemsInOrder()
    {
        $order_items_in_order = $this->hasMany(OrderItem::class, 'order_id','id')->with('relatedProduct');
        return $order_items_in_order;
    }

    public function relatedShop()
    {
        return $this->belongsTo(User::class,'shop_id','id')->select(['id','avatar','name', 'address','phone', 'latitude', 'longitude','shop_time_open','shop_time_close', 'service_radius','shipper_distance_customer']);
    }

    public function infoCustomer()
    {
        return $this->belongsTo(Customer::class,'customer_id','id')->select(['id','avatar','email','name','phone','latitude','longitude']);
    }

    public function infoShipper()
    {
        return $this->belongsTo(Shipper::class,'shipper_id','id')->select(['id','avatar','name','phone','latitude','longitude']);
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($order)
        {
            $order->shipper_id = Auth::guard('api-shipper')->id() ? Auth::guard('api-shipper')->id() : null;
        });

        static::updating(function($order)
        {
            $order->shipper_id = Auth::guard('api-shipper')->id() ? Auth::guard('api-shipper')->id() : null;
        });
    }

    public function getQrcodeAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }

        public function getSignatureAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }
}
