<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Translatable;


class Category extends Model
{
    use Translatable,
        HasRelationships;

    protected $translatable = ['slug', 'name'];

    protected $table = 'categories';

    protected $fillable = ['slug', 'name'];

    public function relatedParentCategory()
    {
        return $this->belongsTo(ParentCategory::class,'parent_id','id')->select(['id', 'name', 'category_image']);;
    }

    public function relatedService()
    {
        return $this->belongsTo(Service::class,'service_id','id')->select(['id', 'name', 'service_image']);;
    }
    
//    public function shopId()
//    {
//        return $this->belongsTo(User::class,'shop_id','id')->select(['id', 'name', 'avatar']);;
//    }

    public function getCategoryImageAttribute($path){
        $store_path = setting('admin.store_path');
        return $store_path.$path;
    }

//    public static function boot()
//    {
//        parent::boot();
//
//        static::creating(function($category)
//        {
//            if($user =auth()->user()) {
//                if($user->id != 1)
//                {
//                    $category->shop_id = auth()->user() ? auth()->user()->id : null;
//                }
//            }
//        });
//
//        static::updating(function($category)
//        {
//            if($user = auth()->user()) {
//                if($user->id != 1)
//                {
//                    $category->shop_id = auth()->user() ? auth()->user()->id : null;
//                }
//            }
//        });
//    }
}
