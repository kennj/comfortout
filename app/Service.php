<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Service extends Model
{
    public function getServiceImageAttribute($test){
        $store_path = setting('admin.store_path');
        return $store_path.$test;
    }
}
