<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ParentCategory extends Model
{
    public function getCategoryImageAttribute($test){
        $store_path = setting('admin.store_path');
        return $store_path.$test;
    }

    public function serviceId()
    {
        return $this->belongsTo(Service::class,'service_id','id')->select(['id', 'name', 'service_image']);;
    }
}
