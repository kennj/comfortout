<?php

namespace App\Helpers;

use App\Customer;
use App\Shipper;
use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device;
use paragraph1\phpFCM\Notification;

class Helper
{

    public static function getDistanceBetweenTwoPoint($lngFrom, $LatFrom, $lngTo, $LatTo, $unit){

        //Get latitude and longitude from geo data
        $latitudeFrom = $LatFrom;
        $longitudeFrom = $lngFrom;
        $latitudeTo = $LatTo;
        $longitudeTo = $lngTo;

        //Calculate distance from latitude and longitude (Meter)
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "M") {
            return ($miles * 1609.344); //meter
        } else{
            return ($miles * 0868.4); //feet
        }
    }

    // Get location from address
    public static function getLocation($address)
    {
        // We get the JSON results from this request
        $address = str_replace(' ','+',$address);
        $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address) . '&sensor=false');
        // We convert the JSON to an array
        $geo = json_decode($geo, true);

        // If everything is cool
        if ($geo['status'] == 'OK') {
            $latlng = $geo['results'][0]['geometry']['location'];

            return $latlng;
        }

        return false;
    }

    /**
     * @param $customer Customer
     * @throws \Exception
     */
    public static function sendNotificationToCustomer($customer, $title, $content, $data)
    {
        if ($customer->device_token) {
            $statusCode = Helper::pushNotificationToCustomer([$customer->device_token], $title, $content, $data, 1);
            if ($statusCode != 200) {
                throw new \Exception('Can not push notification.');
            }

            return true;
        }
    }

    /**
     * @param $shipper Shipper
     * @throws \Exception
     */
    public static function sendNotificationToShipper($shipper, $title, $content, $data)
    {
        if ($shipper->device_token) {
            $statusCode = Helper::pushNotificationToShipper([$shipper->device_token], $title, $content, $data, 1);
            if ($statusCode != 200) {
                throw new \Exception('Can not push notification.');
            }

            return true;
        }
    }

    public static function pushNotificationToCustomer($device_tokens, $title, $body, $data, $unread_numbers)
    {
        require_once base_path('vendor/autoload.php');

        $apiKey = env('FIREBASE_KEY');
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification($title, $body);
        $note->setIcon('icon')
            ->setColor('#FF4081')
            ->setBadge($unread_numbers);
        if (isset($data['sound']) && $data['sound']) {
            $note->setSound('mysound.caf');
        } else {
            $note->setSound('sound');
        }

        $message = new Message();
        foreach ($device_tokens as $device_token) {
            $message->addRecipient(new Device($device_token));
        }

        $message->setNotification($note)
            ->setData($data);

        $response = $client->send($message);

        return $response->getStatusCode();

    }

    public static function pushNotificationToShipper($device_tokens, $title, $body, $data, $unread_numbers)
    {
        require_once base_path('vendor/autoload.php');

        $apiKey = env('FIREBASE_KEY');
        $client = new Client();
        $client->setApiKey($apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $note = new Notification($title, $body);
        $note->setIcon('icon')
            ->setColor('#FF4081')
            ->setBadge($unread_numbers);
        if (isset($data['sound']) && $data['sound']) {
            $note->setSound('mysound.caf');
        } else {
            $note->setSound('sound');
        }

        $message = new Message();
        foreach ($device_tokens as $device_token) {
            $message->addRecipient(new Device($device_token));
        }

        $message->setNotification($note)
            ->setData($data);

        $response = $client->send($message);

        return $response->getStatusCode();

    }
}

class Constants
{

    const NOTIFICATION_CREATE_ORDER = 1;
    const NOTIFICATION_APPROVE_ORDER = 2;
    const NOTIFICATION_CANCEL_ORDER = 3;
    const NOTIFICATION_DELIVERING_ORDER = 4;
    const NOTIFICATION_COMPLETE_ORDER = 5;
    const NOTIFICATION_CUSTOMER_CANCEL = 6;
    const NOTIFICATION_NEAR_BY = 8;
}