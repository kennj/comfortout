<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderItem extends Model
{
    public function relatedProduct()
    {
        $related_product = $this->belongsTo(Product::class, 'product_id','id');
        return $related_product;
    }
}
