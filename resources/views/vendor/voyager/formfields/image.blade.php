@if(isset($dataTypeContent->{$row->field}))
    @if(stristr($dataTypeContent->{$row->field}, ".jpg") || stristr($dataTypeContent->{$row->field}, ".png") || stristr($dataTypeContent->{$row->field}, ".gif")||
    stristr($dataTypeContent->{$row->field}, ".jpeg"))
        <img src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
             style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">

    @else
        <img src="{{setting('admin.store_path')}}{{setting('admin.no_image')}}"
             style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
    @endif
@endif
<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
       name="{{ $row->field }}" accept="image/*">
