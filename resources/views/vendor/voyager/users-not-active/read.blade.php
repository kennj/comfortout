@extends('voyager::master')

@section('page_title', __('voyager.generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager.generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        {{--@can('edit', $dataTypeContent)--}}
        {{--<a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">--}}
            {{--<span class="glyphicon glyphicon-pencil"></span>&nbsp;--}}
            {{--{{ __('voyager.generic.edit') }}--}}
        {{--</a>--}}
        {{--@endcan--}}
        <button title="Active" id="activeShop" onclick='activeShop({{ $dataTypeContent->getKey() }}, {{$dataTypeContent->service_id}})' class="btn btn-info">
            <span class="voyager-check"></span>Active
        </button>
        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager.generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-6">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->
                    @foreach($dataType->readRows as $row)
                        @php $rowDetails = json_decode($row->details);
                         if($rowDetails === null){
                                $rowDetails=new stdClass();
                                $rowDetails->options=new stdClass();
                         }
                        @endphp

                        <div class="panel-heading" style="border-bottom:0;">
                            <h3 class="panel-title">{{ $row->display_name }}</h3>
                        </div>

                        <div class="panel-body" style="padding-top:0;">
                            @if($row->type == "image")
                                <img class="img-responsive" style="max-width: 50%;"
                                     src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                            @elseif($row->type == 'multiple_images')
                                @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                        <img class="img-responsive" style="max-width: 50%;"
                                             src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                    @endforeach
                                @else
                                    <img class="img-responsive" style="max-width: 50%;"
                                         src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                                @endif
                            @elseif($row->type == 'relationship')
                                 @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $rowDetails])
                            @elseif($row->type == 'select_dropdown' && property_exists($rowDetails, 'options') &&
                                    !empty($rowDetails->options->{$dataTypeContent->{$row->field}})
                            )

                                <?php echo $rowDetails->options->{$dataTypeContent->{$row->field}};?>
                            @elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'})
                                <a href="{{ $dataTypeContent->{$row->field . '_page_slug'} }}">{{ $dataTypeContent->{$row->field}  }}</a>
                            @elseif($row->type == 'select_multiple')
                                @if(property_exists($rowDetails, 'relationship'))

                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                        @if($item->{$row->field . '_page_slug'})
                                        <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field}  }}</a>@if(!$loop->last), @endif
                                        @else
                                        {{ $item->{$row->field}  }}
                                        @endif
                                    @endforeach

                                @elseif(property_exists($rowDetails, 'options'))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                     {{ $rowDetails->options->{$item} . (!$loop->last ? ', ' : '') }}
                                    @endforeach
                                @endif
                            @elseif($row->type == 'date' || $row->type == 'timestamp')
                                {{ $rowDetails && property_exists($rowDetails, 'format') ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($rowDetails->format) : $dataTypeContent->{$row->field} }}
                            @elseif($row->type == 'checkbox')
                                @if($rowDetails && property_exists($rowDetails, 'on') && property_exists($rowDetails, 'off'))
                                    @if($dataTypeContent->{$row->field})
                                    <span class="label label-info">{{ $rowDetails->on }}</span>
                                    @else
                                    <span class="label label-primary">{{ $rowDetails->off }}</span>
                                    @endif
                                @else
                                {{ $dataTypeContent->{$row->field} }}
                                @endif
                            @elseif($row->type == 'color')
                                <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                            @elseif($row->type == 'coordinates')
                                @include('voyager::partials.coordinates')
                            @elseif($row->type == 'rich_text_box')
                                @include('voyager::multilingual.input-hidden-bread-read')
                                <p>{!! $dataTypeContent->{$row->field} !!}</p>
                            @elseif($row->type == 'file')
                                @if(json_decode($dataTypeContent->{$row->field}))
                                    @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                        <a href="/storage/{{ $file->download_link or '' }}">
                                            {{ $file->original_name or '' }}
                                        </a>
                                        <br/>
                                    @endforeach
                                @else
                                    <a href="/storage/{{ $dataTypeContent->{$row->field} }}">
                                        Download
                                    </a>
                                @endif
                            @else
                                @include('voyager::multilingual.input-hidden-bread-read')
                                <p>{{ $dataTypeContent->{$row->field} }}</p>
                            @endif
                        </div><!-- panel-body -->
                        @if(!$loop->last)
                            <hr style="margin:0;">
                        @endif
                    @endforeach

                </div>
            </div>
            <div class="col-md-6">

            </div>
        </div>
    </div>

    {{-- Single active modal --}}
    <div class="modal modal-primary fade" tabindex="-1" id="confirm-active" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager.generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-check"></i>Shop Active Confirm</h4>
                </div>
                <div class="modal-body">
                    <h4>Please set service fee percent for this Shop (default 15%). You can change it from Shop list</h4>
                    <input id="service_fee" autocomplete=”nope” type="number" class="form-control" max="100"
                           name="service_fee"
                           placeholder="15%" value="15">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div id="response"></div>
                    <span class="text-danger">
                                <strong id="service_location-error"></strong>
                                </span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ __('voyager.generic.cancel') }}</button>
                    <button type="button" id="confirmActiveBtn" class="btn btn-primary pull-right">Active</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div id='loader' style="display:none; position: fixed; top:50%; left:50%"><img src="http://comfortout.kennjdemo.com/storage/RiWmzj4RFdHC0ZoSODtRcyqumSFpbPZPs6ONZd1E.gif"/></div>
@stop

@section('javascript')
    <script>
        // function activeShop(id)
        // {
        //     alertify.confirm("This is a confirm dialog", function (e) {
        //             if (e) {
        //                 var service_fee = $('#service_fee').val();
        //                 $.ajax({
        //                     url:'/admin/active-shop',
        //                     type:'POST',
        //                     data:{shop_id:id,service_fee:service_fee},
        //                     beforeSend: function() {
        //                         $('#loader').show();
        //                     },
        //                     complete: function(){
        //                         $('#loader').hide();
        //                     },
        //                     success:function(data) {
        //
        //                         $('#confirm-active').modal('hide');
        //                         alertify.success("Shop Active Success");
        //                         $('#shop_'+id).remove();
        //                     },
        //                 });
        //             } else {
        //                 alertify.error("You've clicked Cancel");
        //             }
        //         });
        //         return false;
        // }

        function activeShop(id,service_id)
        {
            $('#confirm-active').modal('show');

            //Select service location function
            var selectedService = service_id;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });

            $.ajax({
                method: "POST",
                url: "/admin/serviceTypeList",
                data: {service: selectedService}
            }).done(function (data) {
                $("#response").html(data).fadeIn();
            });

            $('#confirmActiveBtn').click(function () {
                var service_fee = $('#service_fee').val();
                var service_location = $('#service_location').val();
                $.ajax({
                    url:'/admin/active-shop',
                    type:'POST',
                    data:{shop_id:id,service_fee:service_fee,service_location:service_location},
                    beforeSend: function () {
                        $('#loader').show();
                    },
                    complete: function () {
                        $('#loader').hide();
                    },
                    success:function(data) {

                        $('#confirm-active').modal('hide');
                        toastr.success('Shop active success.');
                        $('#shop_'+id).remove();
                    },
                });
            })

        }
    </script>
    @if ($isModelTranslatable)
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
@stop
