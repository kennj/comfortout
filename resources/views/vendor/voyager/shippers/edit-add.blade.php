@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager.generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager.generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" id="main-form"
                          class="form-edit-add"
                          action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                          method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                @if($row->field == "password")
                                <!-- Check if Password form -->
                                    <div class="form-group  col-md-12">
                                        <label for="password">Password</label>
                                        <!-- check if edit case -->
                                        @if(!is_null($dataTypeContent->getKey()))
                                            <p>Leave empty to keep the same</p>
                                        @else
                                        @endif
                                        <input autocomplete=”off” type="text" class="form-control" id="password"
                                               name="password" placeholder="Password" value="">
                                    </div>
                                @elseif($row->field == "phone")
                                <!-- Check if Phone form -->
                                    <div class="form-group  col-md-12">

                                        <label for="phone">Phone</label>
                                        <input type="text" id="showphone" name="showphone" class="form-control" required="true"
                                               placeholder="Phone"
                                               value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif">
                                        <input type="text" id="phone" name="phone" class="form-control"
                                               placeholder="Phone" style="display:none"
                                               value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@elseif(isset($options->default)){{ old($row->field, $options->default) }}@else{{ old($row->field) }}@endif">
                                    </div>
                                    <script>
                                        $(document).ready(function () {
                                            //beauti phone
                                            /****************** Set phone ************************/
                                            $('#showphone').keyup(function (event) {
                                                var phone = $(this).val();
                                                var a = phone.replace('(', "");
                                                var b = a.replace(')', '');
                                                var c = b.replace('-', '');
                                                var d = c.replace(' ', '');
                                                $('#phone').val(d);
                                            });

                                            /******************Set phone *************/

                                            /************fortmat phone  *****************/
                                            $('#showphone')
                                                .on('keypress', function (e) {
                                                    var key = e.charCode || e.keyCode || 0;
                                                    var phone = $(this);
                                                    if (phone.val().length === 0) {
                                                        phone.val(phone.val() + '(');
                                                    }
                                                    // Auto-format- do not expose the mask as the user begins to type
                                                    if (key !== 8 && key !== 9) {
                                                        if (phone.val().length === 4) {
                                                            phone.val(phone.val() + ')');
                                                        }
                                                        if (phone.val().length === 5) {
                                                            phone.val(phone.val() + ' ');
                                                        }
                                                        if (phone.val().length === 9) {
                                                            phone.val(phone.val() + '-');
                                                        }
                                                        if (phone.val().length >= 14) {
                                                            phone.val(phone.val().slice(0, 13));
                                                        }
                                                    }

                                                    // Allow numeric (and tab, backspace, delete) keys only
                                                    return (key == 8 ||
                                                        key == 9 ||
                                                        key == 46 ||
                                                        (key >= 48 && key <= 57) ||
                                                        (key >= 96 && key <= 105));
                                                })

                                                .on('focus', function () {
                                                    var phone = $(this);

                                                    if (phone.val().length === 1) {
                                                        phone.val('(' + phone.val());
                                                    } else {
                                                        var val = phone.val();
                                                        phone.val('').val(val); // Ensure cursor remains at the end
                                                    }
                                                })

                                                .on('blur', function () {
                                                    var phone = $(this);

                                                    if (phone.val() === '(') {
                                                        phone.val('');
                                                    }
                                                });

                                            /********************End show format phone**********************/

                                            $('#showphone').on('change', function (e) {
                                                var length = $(this).val().length;
                                                if (length == 10) {
                                                    var a = $(this).val().substring(0, 3);
                                                    var b = $(this).val().substring(3, 6);
                                                    var c = $(this).val().substring(6, 10);

                                                    $(this).val('(' + a + ')' + ' ' + b + '-' + c);
                                                }
                                            })
                                        });
                                    </script>
                                @elseif($row->field == "shipper_belongsto_user_relationship")
                                <!-- Check if Shop Id form -->
                                    <!-- Check if Admin -->
                                    @if(auth()->user()->role_id === 1)
                                        <?php $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                        ?>
                                        <label for="name">Shop</label>
                                        @include('voyager::formfields.shop-of-shipper')
                                        <br>
                                    @else
                                    <!-- Check if Shop -->
                                        <div class="form-group  col-md-12" style="display:none">

                                            <label for="name">Shop Id</label>
                                            <input type="text" class="form-control" name="shop_id" placeholder="Shop Id"
                                                   data-slug-origin="users" data-slug-forceupdate="true"
                                                   value="{{auth()->user()->id}}">
                                        </div>
                                    @endif
                                @elseif($row->display_name == "Shipper Music")
                                    @if(!is_null($dataTypeContent->getKey()))
                                        <form method="POST" id="updateMusic" enctype="multipart/form-data">
                                            <div class="col-md-12">
                                                <div class="form-group has-feedback">
                                                    <label for="shipper_distance_customer">Music for this shipper (Audio
                                                        file)</label><br>
                                                    <div id="music-player">
                                                        <audio controls>
                                                            <source src="{{ old($row->field, $dataTypeContent->{$row->field})}}"
                                                                    type="audio/mpeg">
                                                        </audio>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="progress">
                                                            <div class="progress-bar progress-bar-success myprogress"
                                                                 role="progressbar" style="width:0%">0%
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="file" id="music" name="music"
                                                           accept=".mp3,.m4a,.m4r,.ogg,.wav,.flac,.aac,.m4p,.wma">
                                                    <span class="text-danger">
                                <strong id="music-error"></strong>
                    </span>
                                                </div>
                                            </div>
                                        </form>
                                        <script>
                                            //update music
                                            $('#music').on('change', function (e) {
                                                e.preventDefault();
                                                var file = this.files[0];
                                                if (file.size > 8000000) {
                                                    alert('max upload size is 8MB');
                                                    return false;
                                                }
                                                // Also see .name, .type
                                                var formData = new FormData();
                                                formData.append("music", $('#music')[0].files[0]);
                                                formData.append("shipper_id",<?php echo $dataTypeContent->getKey() ?>)
                                                $.ajax({
                                                    url: "/admin/shipper/shipper-music-update",
                                                    type: "POST",
                                                    data: formData,
                                                    contentType: false,
                                                    cache: false,
                                                    processData: false,
                                                    beforeSend: function () {
                                                        $("#music-player").fadeOut();
                                                        $("#music-error").fadeOut();
                                                    },
                                                    xhr: function () {
                                                        var xhr = new window.XMLHttpRequest();
                                                        xhr.upload.addEventListener("progress", function (evt) {
                                                            if (evt.lengthComputable) {
                                                                var percentComplete = evt.loaded / evt.total;
                                                                percentComplete = parseInt(percentComplete * 100);
                                                                $('.myprogress').text(percentComplete + '%');
                                                                $('.myprogress').css('width', percentComplete + '%');
                                                            }
                                                        }, false);
                                                        return xhr;
                                                    },
                                                    success: function (data) {
                                                        if (data == 'invalid file') {
                                                            // invalid file format.
                                                            $("#music-error").html("Invalid File !").fadeIn();
                                                        }
                                                        else {
                                                            // view uploaded file.
                                                            $("#music-player").html(data).fadeIn();
                                                            toastr.success("Update Music Success");
                                                        }
                                                    },
                                                    error: function (e) {
                                                        $("#music-error").html(e).fadeIn();
                                                    }
                                                });
                                            });
                                        </script>
                                    @else
                                        <div class="col-md-12">
                                            <div class="form-group has-feedback">
                                                <label for="shipper_distance_customer">Music for this shipper (Audio
                                                    file)</label><br>
                                                <strong id="music-error">
                                                    For reasons of data integrity, music can only be added after
                                                    creation. You can change music in edit shipper page</strong>
                                                </span>
                                            </div>
                                        </div>
                                    @endif
                                @else
                                <!-- GET THE DISPLAY OPTIONS -->
                                    @php
                                        $options = json_decode($row->details);
                                        $display_options = isset($options->display) ? $options->display : NULL;
                                    @endphp
                                    @if ($options && isset($options->legend) && isset($options->legend->text))
                                        <legend class="text-{{$options->legend->align or 'center'}}"
                                                style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                    @endif
                                    @if ($options && isset($options->formfields_custom))
                                        @include('voyager::formfields.custom.' . $options->formfields_custom)
                                    @else
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            {{ $row->slugify }}
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship')
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>
                                    @endif
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button id="submit-main-form" type="submit"
                                    class="btn btn-primary save">{{ __('voyager.generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager.generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager.generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager.generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager.generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            //submit main-form
            $('#submit-main-form').click(function () {
                $('#main-form').submit();

            })
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                $image = $(this).siblings('img');

                params = {
                    slug: '{{ $dataType->slug }}',
                    image: $image.data('image'),
                    id: $image.data('id'),
                    field: $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@stop
