<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="ComfortOutapps">
    <meta name="keywords" content="HTML5, bootstrap, mobile, app, landing, ios, android, responsive">
    <title>ComfortOut</title>


    <link rel='dns-prefetch' href='//fonts.googleapis.com'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{url('asset/vendors/icon-pe/pe-icon-7-stroke/css/pe-icon-7-stroke.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/simple-line-icons/css/simple-line-icons.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/icon-pe/pe-icon-7-stroke/css/helper.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('asset/vendors/bootstrap/css/bootstrap-timepicker.min.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/slick/css/slick.css')}}">
    <link rel="stylesheet" href="{{url('asset/vendors/slick/css/slick-theme.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/owlcarousel/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('asset/vendors/owlcarousel/css/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/animate.css')}}">

    <link rel="stylesheet" href="{{url('asset/vendors/magnific-popup/css/magnific-popup.css')}}">

    <link href="{{url('asset/css/style.css')}}" rel="stylesheet">
    <link href="{{url('asset/css/login.css')}}" rel="stylesheet">
    <link href="{{url('asset/css/alertify.core.css')}}" rel="stylesheet">
    <link href="{{url('asset/css/alertify.default.css')}}" rel="stylesheet">
</head>
<body data-scroll-animation="true">

<div class="preloader" id="preloader">
    <svg class="spinner" id="pageloader_anime" width="32px" height="32px" viewBox="0 0 66 66"
         xmlns="http://www.w3.org/2000/svg">
        <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
    </svg>
</div>

<div class="nav-menu">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light fadeInDown">
                    <a class="navbar-brand" href="/">
                        <img src="{{url('asset/images/logo.png')}}" class="img-fluid logo" alt="logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <ul class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav" id="top-menu">
                            <li class="nav-item active"><a class="nav-link" href="#home">HOME <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item"><a class="nav-link" href="#features">FEATURES</a></li>
                            <li class="nav-item"><a class="nav-link" href="#contact">CONTACT</a></li>
                        </ul>
                        <ul class="navbar-nav" style="margin-left:0">

                            @if (!Auth::check())
                                <li class="nav-item"><a class="nav-link" href="admin">LOGIN</a></li>
                                <li class="nav-item"><a class="nav-link register-model" href="#" data-toggle="modal"
                                                        data-target="#SignUp">SHOP REGISTER</a></li>

                            @else
                                <li class="nav-item"><a class="nav-link" href="admin">ADMIN</a></li>
                            @endif
                        </ul>
            </div>
            </nav>
        </div>
    </div>
</div>
</div>

<section class="slider-bg" id="home">
    <div class="container">
        <div class="row">
            <div class="col-md-6 order-md-2">
                <div class="slider-title">
                    <h1 class="h2 reveal fadeInUp"><b>Easy</b> order && <b>Easy</b> pay.</h1>
                    <p class="reveal fadeInUp" data-wow-delay="0.3s">Noble allows you to order and pay for food and
                        drinks at your favorite venues and events without missing the fun. No more waiting for your
                        server or getting stuck in long lines. Order from your phone and you’ll be notified when your
                        order is ready for pickup or delivery.</p>
                    <a href="#" class="btn btn-primary mr-2 reveal fadeInUp" data-wow-delay="0.6s">
                        <i class="fa fa-apple mr-2" aria-hidden="true"></i>App Store</a>
                    <a href="#" class="btn btn-success reveal fadeInUp" data-wow-delay="0.9s">
                        <i class="fa fa-android mr-2" aria-hidden="true"></i>Play Store</a>
                    <p class="mt-4 text-muted reveal fadeInUp" data-wow-delay="1.2s">Version required ios 10 or later,
                        Android Kitkat or later</p>
                </div>
            </div>
            <div class="col-md-6 order-md-1">
                <div class="phone-wrap reveal fadeIn"><img src="{{ url('asset/images/adult-hand-dark.png')}}"
                                                           class="phone" alt="#">
                    <div class="screen">
                        <div class="screen-slider owl-carousel owl-theme">
                            <div class="item"><img src="{{url('asset/images/img-5.jpg')}}" alt="#"></div>
                            <div class="item"><img src="{{url('asset/images/img-4.jpg')}}" alt="#"></div>
                            <div class="item"><img src="{{url('asset/images/img-3.jpg')}}" alt="#"></div>
                            <div class="item"><img src="{{url('asset/images/img-2.jpg')}}" alt="#"></div>
                            <div class="item"><img src="{{url('asset/images/img-1.jpg')}}" alt="#"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="clients light-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="client-slide owl-carousel reveal fadeIn">
                    <div><img src="{{ url('asset/images/client1.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client2.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client3.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client4.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client5.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client6.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client1.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client2.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client3.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client4.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client5.png') }}" alt="#"></div>
                    <div><img src="{{ url('asset/images/client6.png') }}" alt="#"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<section class="skew-bg space">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="reveal fadeInUp"><b>Increase</b> sales by <b>decreasing</b> time.</h2>
                <p class="reveal fadeInUp" data-wow-delay="0.3s">With Beach, bartenders don’t spend time taking orders
                    and processing payments. Serving time is cut in half as bartenders do what they do best—make
                    delicious drinks for your customers.</p>
            </div>
            <div class="landing-img-bg reveal fadeIn">
                <div class="isometric-stack">
                    <div class="stack-screens"><img src="{{ url('asset/images/img-4.jpg')}}" alt="screen"/>
                        <img src="{{ url('asset/images/img-2.jpg')}}" alt="screen"/>
                        <img src="{{ url('asset/images/img-1.jpg')}}" alt="screen"/></div>
                    <img src="{{ url('asset/images/isometric-view.png')}}" class="img-fluid" alt="#"></div>
            </div>
        </div>
    </div>
</section>


<div class="clearfix"></div>
<section class="app-landing light-bg">
    <div class="container">
        <div class="row justify-content-center center-block">
            <div class="col-md-8">
                <div class="title-block">
                    <h2 class="reveal fadeInUp">Bartenders love Beach</h2>
                    <p class="reveal fadeInUp" data-wow-delay="0.3s">Noble exists to make your bartenders more
                        efficient.
                        By using Beach, each bartender makes 30 percent more drinks every hour.
                        They spend less time closing, printing, and collecting tabs, and more time mixing and pouring.
                        Our handy-dandy efficiency calculator shows you how Noble can increase venue sales.
                    </p>

                    <a href="#" class="btn btn-primary mb-4 reveal fadeInUp" data-wow-delay="0.6s">Learn more<span
                                class="pe-7s-angle-right-circle ml-3"></span></a></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="landing-img-wrap reveal fadeIn" data-wow-delay="0.3s">
                    <div class="iphone-half1"><img src="{{ url('asset/images/detailed-screen.png')}}" class="phone"
                                                   alt="#">
                        <div class="screen"><img src="{{ url('asset/images/img-6.jpg')}}" class="img-fluid" alt="#">
                        </div>
                    </div>
                    <div class="iphone-half2 reveal fadeIn"><img src="{{ url('asset/images/iphone-black.png')}}"
                                                                 class="phone" alt="#">
                        <div class="screen"><img src="{{ url('asset/images/img-7.jpg')}}" class="img-fluid" alt="#">
                        </div>
                    </div>
                    <div class="iphone-half3 reveal fadeIn" data-wow-delay="0.6s">
                        <img src="{{ url('asset/images/iPhone-gold.png')}}" class="phone" alt="#">
                        <div class="screen"><img src="{{ url('asset/images/img-1.jpg')}}" class="img-fluid" alt="#">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="space">
    <div class="container">
        <div class="row">
            <div class="col-md-5 featured3-block">
                <div class="featured1-wrap featured3-wrap">
                    <i class="pe-primary pe-7s-diamond reveal fadeInUp" aria-hidden="true"></i>
                    <h2 class="reveal fadeInUp" data-wow-delay="0.3s">Style that you’ll adore</h2>
                    <h3 class="reveal fadeInUp" data-wow-delay="0.6s">what you can ask more?</h3>
                    <p class="reveal fadeInUp" data-wow-delay="0.9s">You can create custom iOS and macOS apps for your
                        business using Swift, our open source programming language. Apps that have the power to
                        transform workflows.</p>
                    <div class="featured-list mt-5">
                        <i class="pe-primary pe-7s-rocket reveal fadeInUp" data-wow-delay="1.2s" aria-hidden="true"></i>
                        <div class="featured-list-title reveal fadeInUp" data-wow-delay="1.2s">
                            <h4>Fully functional template</h4>
                            <p>Apps that have the power to transform workflows.</p>
                        </div>
                        <hr class="reveal fadeIn" data-wow-delay="1.5s">
                        <i class="pe-primary pe-7s-medal reveal fadeInUp" data-wow-delay="1.5s" aria-hidden="true"></i>
                        <div class="featured-list-title reveal fadeInUp" data-wow-delay="1.5s">
                            <h4>Award winning design</h4>
                            <p>You can create custom iOS and macOS apps</p>
                        </div>
                        <hr class="reveal fadeIn" data-wow-delay="1.8s">
                        <i class="pe-primary pe-7s-edit reveal fadeInUp" data-wow-delay="1.8s" aria-hidden="true"></i>
                        <div class="featured-list-title reveal fadeInUp" data-wow-delay="1.8s">
                            <h4>Easy to customize</h4>
                            <p>Swift, our open source programming language.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-7 featured3-block">
                <div class="featured3-img-wrap">

                    <div class="stylish-screen-skew reveal fadeInRight" data-wow-delay="1.2s">
                        <div class="isometric-stack1">
                            <div class="stack-screens"><img src="{{ url('asset/images/img-5.jpg') }}" alt="screen"/>
                            </div>
                            <img src="{{ url('asset/images/stylish-screen.png') }}" class="img-fluid" alt="#"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="space light-bg" id="features">
    <div class="container">
        <div class="row justify-content-center center-block">
            <div class="col-md-8">
                <div class="title-block">
                    <h2 class="reveal fadeInUp">User reviews</h2>
                    <p class="reveal fadeInUp" data-wow-delay="0.3s">Conveniently coordinate cross-platform data after
                        emerging internal or organic sources. Authoritatively productivate fully tested niches and 2.0
                        vortals. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="loop reveal fadeIn">
                    <div class="comment-wrap">
                        <div class="star-rating mb-2"><i class="fa fa-star" aria-hidden="true"></i> <i
                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <h4>Exceptional Mobile App</h4>
                        <p>The more recent updates have turned this app into one of the best app I've seen! It deserves
                            more than 5 stars. I absolutely love this app and i couldn’t recommend it more. Keep up the
                            good work guys.</p> <span>— Roger Cooper, <small> United States</small></span> <img
                                src="{{ url('asset/images/testi-img1.jpg') }}" class="comment-img" alt="#"></div>
                    <div class="comment-wrap">
                        <div class="star-rating mb-2"><i class="fa fa-star" aria-hidden="true"></i> <i
                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <h4>Keeps getting better!</h4>
                        <p>The more recent updates have turned this app into one of the best app I've seen! It deserves
                            more than 5 stars. I absolutely love this app and i couldn’t recommend it more. Keep up the
                            good work guys.</p> <span>— Juan Sullivan, <small> Australia</small></span> <img
                                src="{{ url('asset/images/testi-img2.jpg')}}" class="comment-img" alt="#"></div>
                    <div class="comment-wrap">
                        <div class="star-rating mb-2"><i class="fa fa-star" aria-hidden="true"></i> <i
                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <h4>It deserves more than 5 stars!</h4>
                        <p>The more recent updates have turned this app into one of the best app I've seen! It deserves
                            more than 5 stars. I absolutely love this app and i couldn’t recommend it more. Keep up the
                            good work guys.</p> <span>— Ann Kennedy, <small> United States</small></span> <img
                                src="{{ url('asset/images/testi-img3.jpg')}}" class="comment-img" alt="#"></div>
                    <div class="comment-wrap">
                        <div class="star-rating mb-2"><i class="fa fa-star" aria-hidden="true"></i> <i
                                    class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star"
                                                                                  aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <h4>It deserves more than 5 stars!</h4>
                        <p>The more recent updates have turned this app into one of the best app I've seen! It deserves
                            more than 5 stars. I absolutely love this app and i couldn’t recommend it more. Keep up the
                            good work guys.</p> <span>— Roger Cooper, <small> United States</small></span> <img
                                src="{{ url('asset/images/testi-img1.jpg')}}" class="comment-img" alt="#"></div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="space light-bg features">
    <div class="container">
        <div class="row">
            <div class="col-md-7">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item phone-tab-list reveal fadeInLeft"><a class="nav-link active" data-toggle="tab"
                                                                             href="#tab-img1" role="tab">
                            <span>01</span>
                            <div class="tab-list-content">
                                <h4>Login with Touch ID</h4>
                                <p class="text-muted">Apps that have the power to transform workflows.</p>
                            </div>
                        </a></li>
                    <li class="nav-item phone-tab-list reveal fadeInLeft" data-wow-delay="0.3s">
                        <a class="nav-link" data-toggle="tab" href="#tab-img2" role="tab">
                            <span>02</span>
                            <div class="tab-list-content">
                                <h4>View Dashboard Analytics</h4>
                                <p class="text-muted">Apps that have the power to transform workflows.</p>
                            </div>
                        </a></li>
                    <li class="nav-item phone-tab-list reveal fadeInLeft" data-wow-delay="0.6s">
                        <a class="nav-link" data-toggle="tab" href="#tab-img3" role="tab">
                            <span>03</span>
                            <div class="tab-list-content">
                                <h4>Enjoy Mobile Services</h4>
                                <p class="text-muted">Apps that have the power to transform workflows.</p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5 justify-content-center">
                <div class="tab-wrap reveal fadeIn"><img src="{{ url('asset/images/tab-screen.png')}}" class="tab-phone"
                                                         alt="#">
                    <div class="tab-screen">

                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-img1" role="tabpanel"><img
                                        src="{{ url('asset/images/img-3.jpg')}}" class="img-fluid" alt="#">
                            </div>
                            <div class="tab-pane" id="tab-img2" role="tabpanel"><img
                                        src="{{ url('asset/images/img-4.jpg')}}" class="img-fluid" alt="#"></div>
                            <div class="tab-pane" id="tab-img3" role="tabpanel"><img
                                        src="{{ url('asset/images/img-6.jpg')}}" class="img-fluid" alt="#"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="space">
    <div class="container">
        <div class="row">
            <div class="col-md-4 detailed-icon-wrap">
                <div class="detailed-block reveal fadeInLeft"><span class="pe-primary pe-7s-server"></span>
                    <h4>Powerful Stack</h4>
                    <p class="text-muted">Conveniently redefine transparent results vis-a-vis inexpensive best practices
                        of the tech.</p>
                </div>
                <div class="detailed-block reveal fadeInLeft" data-wow-delay="0.3s"><span
                            class="pe-primary pe-7s-science"></span>
                    <div class="detailed-title">
                        <h4>React native support</h4>
                        <p class="text-muted">Holisticly cultivate effective web services for leading-edge users.
                            Completely myocardinate impactful </p>
                    </div>
                </div>
                <div class="detailed-block reveal fadeInLeft" data-wow-delay="0.6s"><span
                            class="pe-primary pe-7s-photo"></span>
                    <div class="detailed-title">
                        <h4>Images included</h4>
                        <p class="text-muted">Completely myocardinate impactful processes vis-a-vis mission-critical
                            initiatives practices</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 detaled-wrap-flex">
                <div class="deatiled-phone-wrap reveal fadeIn"><img src="{{ url('asset/images/detailed-screen.png') }}"
                                                                    class="phone"
                                                                    alt="#">
                    <div class="screen">
                        <div class="screen-slider owl-carousel owl-theme">
                            <div class="item"><img src="{{ url('asset/images/img-6.jpg') }}" alt="#"></div>
                            <div class="item"><img src="{{ url('asset/images/img-3.jpg') }}" alt="#"></div>
                            <div class="item"><img src="{{ url('asset/images/img-7.jpg') }}" alt="#"></div>
                            <div class="item"><img src="{{ url('asset/images/img-5.jpg') }}" alt="#"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 detailed-icon-wrap">
                <div class="detailed-block2 reveal fadeInRight"><span
                            class="pe-primary pe-7s-rocket detailed-icon2"></span>
                    <h4>Fully functional template</h4>
                    <p class="text-muted">Conveniently redefine transparent results vis-a-vis inexpensive best practices
                        of web. </p>
                </div>
                <div class="detailed-block2 reveal fadeInRight"><span class="pe-primary pe-7s-medal detailed-icon2"
                                                                      data-wow-delay="0.3s"></span>
                    <h4>Award winning design</h4>
                    <p class="text-muted">Redefine transparent results vis-a-vis inexpensive best practices. Holisticly
                        cultivate.</p>
                </div>
                <div class="detailed-block2 reveal fadeInRight"><span class="pe-primary pe-7s-edit detailed-icon2"
                                                                      data-wow-delay="0.6s"></span>
                    <h4>Easy to customize</h4>
                    <p class="text-muted">Transparent results vis-a-vis inexpensive best practices. Holisticly cultivate
                        effective.</p>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<div class="google-map" id="gmaps" data-lat="40.545340" data-lon="-74.481669"--}}
{{--data-maps-apikey="AIzaSyDMTUkJAmi1ahsx9uCGSgmcSmqDTBF9ygg" data-zoom="11"></div>--}}


<section class="contact" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="contact-form reveal fadeInUp">
                    <form id="phpcontactform" action="#" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 class="center-block mt-4 mb-4">Contact Us</h2>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><input type="text" class="form-control" name="name"
                                                               placeholder="Name"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group"><input type="email" class="form-control" name="email"
                                                               placeholder="Email"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group"><textarea class="form-control" name="message"
                                                                  placeholder="Message" rows="3"></textarea></div>
                                <div class="text-center">
                                    <button type="submit" id="js-contact-btn" class="btn btn-primary mt-4">Send
                                        message<span class="pe-7s-angle-right-circle ml-3"></span></button>
                                    <div id="js-contact-result" data-success-msg="Success, We will get back to you soon"
                                         data-error-msg="Oops! Something went wrong"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="address-block">
                    <div class="address reveal fadeInUp">
                        <p><span class="pe-7s-map-marker"></span>30 Knightsbridge Road Piscataway New Jersey 08854 </p>
                    </div>
                    <div class="address reveal fadeInUp" data-wow-delay="0.3s"><span class="pe-7s-mail"></span>
                        <p>
                            <a href="#" class="__cf_email__" data-cfemail="">ken@kan-tek.com</a>
                        </p>
                    </div>
                    <div class="address reveal fadeInUp" data-wow-delay="0.6s"><span class="pe-7s-call"></span>
                        <p>732-595-2046</p>
                    </div>
                    <ul class="reveal fadeInUp" data-wow-delay="0.9s">
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="space light-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 center-block">
                <h2 class="reveal fadeInUp">Perfect landing page for any App</h2>
                <p class="mb-5 reveal fadeInUp" data-wow-delay="0.3s">You can create custom iOS and macOS apps for your
                    business using Swift, our open source programming language. Apps that have the power to transform
                    workflows, improve client relationships, and boost your productivity.</p>
                <a href="#" class="reveal fadeInUp" data-wow-delay="0.6s">
                    <img src="{{ url('asset/images/app-store.png') }}" class="img-fluid mb-3 mr-sm-3" alt="#">
                </a>
                <a href="#" class="reveal fadeInUp" data-wow-delay="0.9s">
                    <img src="{{ url('asset/images/play-store.png') }}" class="img-fluid mb-3" alt="#">
                </a>
            </div>
        </div>
    </div>
</section>

<footer class="space">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 center-block">
                <p class="reveal fadeInUp">Copyright © 2018. All rights reserved. Kan-tek Inc</p>
                <a href="#" class="link-color reveal fadeInUp" data-wow-delay="0.3s">PRESS</a>
                <a href="#" class="link-color reveal fadeInUp" data-wow-delay="0.6s">TERMS</a>
                <a href="#" class="link-color reveal fadeInUp" data-wow-delay="0.9s">PRIVACY</a>
            </div>
        </div>
    </div>
</footer>


{{--<script data-cfasync="false" src="cdn-cgi/scripts/af2821b0/cloudflare-static/email-decode.min.js"></script>--}}
<script src="{{url('asset/vendors/jquery/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{url('asset/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{url('asset/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('asset/vendors/bootstrap/js/bootstrap-timepicker.min.js')}}"></script>
<script src="{{url('asset/vendors/bootstrap/js/tempusdominus-bootstrap-4.min.js')}}"></script>

<script src="{{url('asset/vendors/slick/js/slick.min.js')}}"></script>

<script src="{{url('asset/vendors/owlcarousel/js/owl.carousel.min.js')}}"></script>

<script src="{{url('asset/vendors/wow.min.js')}}"></script>

<script src="{{url('asset/vendors/magnific-popup/js/magnific-popup.min.js')}}"></script>

<script src="{{url('asset/vendors/validate.js')}}"></script>

{{--<script src="{{url('asset/js/contact.js')}}"></script>--}}

<script src="{{url('asset/js/script.js')}}"></script>

<script src="{{url('asset/js/alertify.min.js')}}"></script>
</body>

<div id="SignUp" class="modal fade" role="dialog">

    <div class="modal-dialog" style="max-width: 600px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-left: 0;">×
                </button>
                <h3 id="myModalLabel" style="margin: 0 auto;">Shop Register</h3>
            </div>
            <div class="modal-body" id="myWizard">
                <form method="POST" id="Register">
                    {{ csrf_field() }}
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1"
                             aria-valuemin="1" aria-valuemax="4" style="width: 20%;">
                            Step 1 of 5
                        </div>
                    </div>
                    <div class="notifications">
                        <span class="text-danger"></span>

                    </div>
                    <div class="navbar">
                        <div class="navbar-inner">
                            <ul class="nav nav-pills register-wizard">
                                <li class="active"><a id="step11" name="step1" href="#step1" data-toggle="tab"
                                                      data-step="1">Shop Information | </a></li>
                                <li><a id="step22" href="#step2" name="step2" data-toggle="tab" data-step="2">Shop
                                        Location | </a></li>
                                <li><a id="step33" href="#step3" name="step3" data-toggle="tab" data-step="3">Shop
                                        Services | </a></li>
                                <li><a id="step44" href="#step4" name="step4" data-toggle="tab"
                                       data-step="4">Complete</a></li>
                            </ul>
                        </div>
                    </div>
                    <br>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="step1">
                            <div class="form-group has-feedback">
                                <label for="name">Shop Name</label>
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control"
                                       placeholder="Full name">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="email">Email</label>
                                <input type="email" name="email" value="{{ old('email') }}" class="form-control"
                                       placeholder="Email">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="phone">Phone</label>
                                <input type="phone" name="phone" value="{{ old('phone') }}" class="form-control"
                                       placeholder="Phone">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="phone-error"></strong>
                            </span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="password">Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="password-error"></strong>
                            </span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="password-confirm">Password Confirm</label>
                                <input type="password" name="password_confirmation" class="form-control"
                                       placeholder="Retype password">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                            <a class="btn btn-primary next" href="#">Continue</a>
                        </div>
                        <div class="tab-pane fade" id="step2">
                            <div class="form-group has-feedback">
                                <label for="name">Address</label>
                                <input id="pac-input" autocomplete=”off” type="text" class="form-control" name="address"
                                       placeholder="Address" value="{{ old('address') }}">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="address-error"></strong>
                            </span>
                                <br>
                                <label for="service_radius">Service Distance ( Metter)</label>
                                <input id="service_radius" autocomplete=”nope” type="text" class="form-control"
                                       name="service_radius"
                                       placeholder="Service Radius" value="{{ old('service_radius') }}">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="service_radius-error"></strong>
                            </span>
                                <input type="text" class="form-control" id="latitude" name="latitude"
                                       placeholder="Latitude" value="{{ old('latitude') }}" style="display:none">
                                <input type="text" class="form-control" id="longitude" name="longitude"
                                       placeholder="Longitude" value="{{ old('longitude') }}" style="display:none">
                                <br>
                                <div id="map-canvas" style="width: 100%; height:300px;"></div>
                            </div>

                            <a class="btn btn-primary back" href="#">Back</a>
                            <a class="btn btn-primary next" href="#">Continue</a>
                        </div>
                        <div class="tab-pane fade" id="step3">
                            <div class="form-group has-feedback">
                                <label for="services">What time you open and close?</label>
                                <input type="text" class="form-control" id="local_time" name="local_time"
                                       style="display:none">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <div class="input-group date" id="open_time" data-target-input="nearest">
                                                <input type="text" name="open_time" value="08:00"
                                                       class="form-control datetimepicker-input"
                                                       data-target="#open_time"/>
                                                <div class="input-group-append" data-target="#open_time"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <p>To</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <div class="input-group date" id="close_time" data-target-input="nearest">
                                                <input type="text" name="close_time" value="17:00"
                                                       class="form-control datetimepicker-input"
                                                       data-target="#close_time"/>
                                                <div class="input-group-append" data-target="#close_time"
                                                     data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <script type="text/javascript">
                                        $(function () {
                                            $('#open_time').datetimepicker({
                                                format: 'HH:mm'
                                            });
                                            $('#close_time').datetimepicker({
                                                format: 'HH:mm'
                                            });
                                        });
                                    </script>
                                </div>
                                <label for="services">What type of business?</label>
                                <div class="row">
                                    <?php foreach ($services as $service)
                                        echo '<div class="col-md-3 services" >
<div class="row"><p class="text-center" style="margin:0 auto;color:red; font-weight:700">' . $service->name . '</p></div><div class="row">
                                                <label class="btn">
                                                    <img src="' . $service->service_image . '" class="img-thumbnail img-check">
                                                    <input type="radio"  name="services" id="' . $service->id . '" value="' . $service->id . '" autocomplete="off">
                                                </label>
                                                </div>
                                              </div>';
                                    ?>
                                </div>
                                <style>
                                    input[type=radio] {
                                        display: none;
                                    }

                                    .radioActive {
                                        background-color: #00000021;
                                    }
                                </style>
                                <script>
                                    $('input:radio').change(function () {
                                        $('.services').addClass('radioActive');
                                        $('div:has(input:radio:not(:checked))').removeClass('radioActive');
                                    });
                                </script>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div id="response"></div>
                            </div>
                            <a class="btn btn-primary back" href="#">Back</a>
                            <a class="btn btn-primary next" href="#">Continue</a>
                        </div>
                        <div class="tab-pane fade" id="step4">
                            <div class="form-group  col-md-12">
                                <p class="text-center">It's all, Please push Register button to complete. you will
                                    receive email when admin approve your request. Thanks</p>
                            </div>

                            <a class="btn btn-primary back" href="#">Back</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                <button id="submitForm" class="btn btn-primary" disabled>Register</button>
            </div>
        </div>
    </div>
</div>
<div id='loader' style="display:none; position: fixed; top:50%; left:50%"><img
            src="http://comfortout.kennjdemo.com/storage/RiWmzj4RFdHC0ZoSODtRcyqumSFpbPZPs6ONZd1E.gif"/></div>
</html>
<script type="text/javascript">

    $(document).ready(function () {
        //Select service function
        $("[type=radio]").change(function () {
            var selectedService = $("[type=radio]:checked").val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            });

            $.ajax({
                method: "POST",
                url: "serviceTypeList",
                data: {service: selectedService}
            }).done(function (data) {
                $("#response").html(data).fadeIn();
            });
        });
    });
    // next click function
    $('.next').click(function () {

        var nextId = $(this).parents('.tab-pane').next().attr("id");
        $('[href="#' + nextId + '"]').tab('show');
        if (nextId == 'step4') {
            $('#submitForm').prop("disabled", false); // Element(s) are now enabled.
        }
        return false;

    })

    //enable submitForm button
    $('#step11').click(function () {
        $('#submitForm').prop("disabled", true); // Element(s) are now disable.
    });
    $('#step22').click(function () {
        $('#submitForm').prop("disabled", true); // Element(s) are now disable.
    });
    $('#step33').click(function () {
        $('#submitForm').prop("disabled", true); // Element(s) are now disable.
    });
    $('#step44').click(function () {
        $('#submitForm').prop("disabled", false); // Element(s) are now enabled.
    });

    //back click function
    $('.back').click(function () {

        var backId = $(this).parents('.tab-pane').prev().attr("id");
        $('[href="#' + backId + '"]').tab('show');
        $('#submitForm').prop("disabled", true); // Element(s) are now disable.
        return false;

    })

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

        //update progress
        var step = $(e.target).data('step');
        var percent = (parseInt(step) / 4) * 100;

        $('.progress-bar').css({width: percent + '%'});
        $('.progress-bar').text("Step " + step + " of 4");

        //e.relatedTarget // previous tab

    })

    $('.register-model').click(function () {

        $('#myWizard a:first').tab('show');
    })

    //submit buton click
    $('body').on('click', '#submitForm', function () {
        var localTime = new Date().getTimezoneOffset();
        $('#local_time').val(localTime);
        var registerForm = $("#Register");
        var formData = registerForm.serialize();
        $('#name-error').html("");
        $('#email-error').html("");
        $('#phone-error').html("");
        $('#address-error').html("");
        $('#service_radius-error').html("");
        $('#password-error').html("");

        $.ajax({
            url: '/register',
            type: 'POST',
            data: formData,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function (data) {
                if (data.errors) {
                    if (data.errors.name) {
                        $('#name-error').html(data.errors.name[0]);
                    }
                    if (data.errors.email) {
                        $('#email-error').html(data.errors.email[0]);
                    }
                    if (data.errors.phone) {
                        $('#phone-error').html(data.errors.phone[0]);
                    }
                    if (data.errors.password) {
                        $('#password-error').html(data.errors.password[0]);
                    }
                    if (data.errors.address) {
                        $('#address-error').html(data.errors.address[0]);
                    }
                    if (data.errors.latitude) {
                        $('#address-error').html('Address invalid! Please try again');
                    }
                    if (data.errors.service_radius) {
                        $('#service_radius-error').html(data.errors.service_radius[0]);
                    }
                    if (data.errors.name || data.errors.email || data.errors.phone || data.errors.password) {
                        $('[href="#step1"]').tab('show');
                        $('#submitForm').prop("disabled", true); // Element(s) are now disable.
                    }
                    else if (data.errors.address || data.errors.service_radius) {
                        $('[href="#step2"]').tab('show');
                        $('#submitForm').prop("disabled", true); // Element(s) are now disable.

                    }

                }
                if (data.success) {
                    $('#SignUp').modal('hide');
                    alertify.alert("You will receive email when your request approve. Thanks");
                    return false;
                }
            },
        });
    });

    //Google map
    function initMap() {
        var lat = 0;
        var lng = 0;
        var Latlng = new google.maps.LatLng(lat, lng);

        var map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: {lat: lat, lng: lng},
            zoom: 15
        });

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });

        if ($('#service_radius').val() != '') {
            var radius = $('#service_radius').val();
            loadRadius(radius);
        }

        //check when change Map address
        $('#pac-input').keyup(function () {
            changeMap();
        });

        //load new map when change address
        function changeMap() {
            var input = document.getElementById('pac-input');
            var autocomplete = new google.maps.places.Autocomplete(input);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    center: {lat: 0, lng: 0},
                    zoom: 15
                });

                var marker = new google.maps.Marker({
                    position: {lat: 0, lng: 0},
                    map: map
                });

                var place = autocomplete.getPlace();
                // If the place has a geometry, then present it on a map.
                if (place) {
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(13);
                    }
                }

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                document.getElementById('latitude').value = place.geometry.location.lat();
                document.getElementById('longitude').value = place.geometry.location.lng();

                if ($('#service_radius').val() != "") {
                    var shopmap = {
                        center: {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()},
                    };
                    var radius = $('#service_radius').val();
                    var shopCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center: shopmap.center,
                        radius: radius * 1.609
                    });

                }
            });
        }

        //check if service_radius change vallue
        $('#service_radius').on('change', function () {
            if ($('#service_radius').val() != "" && $('pac-input').val() != "") {
                var radius = $('#service_radius').val();
                loadRadius(radius);
            }
        });

        function loadRadius(radius) {
            var latitude = 0;
            latitude = parseFloat(document.getElementById('latitude').value);
            var longitude = 0;
            longitude = parseFloat(document.getElementById('longitude').value);

            var shopmap = {
                center: {lat: latitude, lng: longitude},
            };

            function initMap() {
                // Create the map.
                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    zoom: 16,
                    center: {lat: latitude, lng: longitude},
                    mapTypeId: 'roadmap'
                });

                //set mark to shop base on address
                var marker = new google.maps.Marker({
                    position: {lat: latitude, lng: longitude},
                    map: map
                });
                marker.setVisible(true);


                // Construct the circle for each value in citymap.
                var shopCircle = new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                    map: map,
                    center: shopmap.center,
                    radius: radius * 1.609
                });
            }

            initMap();


        }

    }
</script>
<script>
    $('#service_radius').change(function () {
        if ($('#service_radius').val() != "") {
            var radius = $('#service_radius').val();
            loadRadius(radius);
        }
    });

    function loadRadius(radius) {
        var latitude = 0;
        latitude = parseFloat(document.getElementById('latitude').value);
        var longitude = 0;
        longitude = parseFloat(document.getElementById('longitude').value);

        var shopmap = {
            center: {lat: latitude, lng: longitude},
        };

        function initMapRadius() {
            // Create the map.
            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                zoom: 12,
                center: {lat: latitude, lng: longitude},
                mapTypeId: 'roadmap'
            });

            //set mark to shop base on address
            var marker = new google.maps.Marker({
                position: {lat: latitude, lng: longitude},
                map: map
            });
            marker.setVisible(true);


            // Construct the circle for each value in citymap.
            var cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center: shopmap.center,
                radius: radius * 1.609
            });
        }

        initMapRadius();

    }
</script>
<script src="@php echo 'https://maps.googleapis.com/maps/api/js?key='.env('GOOGLE_MAPS_API_KEY').'&libraries=places&callback=initMap'; @endphp "
        async defer></script>
