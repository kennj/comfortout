@extends('shop-settings.master')

@section('page_title', __('voyager.generic.viewing').' '.__('voyager.generic.settings'))

@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }

        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }

        .settings .panel-actions {
            right: 0px;
        }

        .panel hr {
            margin-bottom: 10px;
        }

        .panel {
            padding-bottom: 15px;
        }

        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }

        .sort-icons:hover {
            color: #37474F;
        }

        .voyager-sort-desc {
            margin-right: 10px;
        }

        .voyager-sort-asc {
            top: 10px;
        }

        .page-title {
            margin-bottom: 0;
        }

        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }

        .modal-open .settings .select2-container {
            z-index: 9 !important;
            width: 100% !important;
        }

        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }

        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }

        .settings .panel-title {
            padding-left: 0px;
            padding-right: 0px;
        }

        .new-setting hr {
            margin-bottom: 0;
            position: absolute;
            top: 7px;
            width: 96%;
            margin-left: 2%;
        }

        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }

        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }

        .new-settings-options label {
            margin-top: 13px;
        }

        .new-settings-options .alert {
            margin-bottom: 0;
        }

        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }

        .new-setting-btn i {
            position: relative;
            top: 2px;
        }

        textarea {
            min-height: 120px;
        }

        textarea.hidden {
            display: none;
        }

        .voyager .settings .nav-tabs {
            background: none;
            border-bottom: 0px;
        }

        .voyager .settings .nav-tabs .active a {
            border: 0px;
        }

        .select2 {
            width: 100% !important;
            border: 1px solid #f1f1f1;
            border-radius: 3px;
        }

        .voyager .settings input[type=file] {
            width: 100%;
        }

        .settings .select2 {
            margin-left: 10px;
        }

        .settings .select2-selection {
            height: 32px;
            padding: 2px;
        }

        .voyager .settings .nav-tabs > li {
            margin-bottom: -1px !important;
        }

        .voyager .settings .nav-tabs a {
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .voyager .settings .nav-tabs a i {
            display: block;
            font-size: 22px;
        }

        .tab-content {
            background: #ffffff;
            border: 1px solid transparent;
        }

        .tab-content > div {
            padding: 10px;
        }

        .settings .no-padding-left-right {
            padding-left: 0px;
            padding-right: 0px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            background: #fff !important;
            color: #62a8ea !important;
            border-bottom: 1px solid #fff !important;
            top: -1px !important;
        }

        .nav-tabs > li a {
            transition: all 0.3s ease;
        }

        .nav-tabs > li.active > a:focus {
            top: 0px !important;
        }

        .voyager .settings .nav-tabs > li > a:hover {
            background-color: #fff !important;
        }

        .button-inc-dec {
            margin: 0 0 0 5px;
            text-indent: -9999px;
            cursor: pointer;
            width: 29px;
            float:left;
            height: 29px;
            text-align: center;
            background: url(http://comfortout.kennjdemo.com/storage/6agY4r0oPVyJAf2NvUxtTuJsis92PSSz4FfWfdkd.png) no-repeat;
        }

        .dec {
            background-position: 0 -29px;
        }

        .min-tip-percent{
            float:left;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-settings"></i> Shop Settings
    </h1>
@stop

@section('content')
    <div class="page-content settings container-fluid">
        <form method="POST" id="updateForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group has-feedback">
                        <label for="name">Shop Name</label>
                        <input type="text" name="name" value="{{$data->name or old('name') }}" class="form-control"
                               placeholder="Full name">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="name-error"></strong>
                            </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="email">Email</label>
                        <input type="email" name="email" value="{{$data->email or old('email') }}" class="form-control"
                               placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="password">Password</label>
                        <input autocomplete=”off” type="password" name="password" value="" class="form-control"
                               placeholder="Leave empty to keep the same">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="password-error"></strong>
                            </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" value="{{$data->phone or old('phone') }}" class="form-control"
                               placeholder="Phone">
                        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="phone-error"></strong>
                            </span>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="stripe_id">Stripe ID</label>
                        <input type="text" name="stripe_id" value="{{$data->stripe_id or old('stripe_id') }}"
                               class="form-control"
                               placeholder="Your Stripe Id">
                        <span class="glyphicon glyphicon-piggy-bank form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="stripe_id-error"></strong>
                            </span>
                    </div>
                    <label for="time">Service Time</label>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="open_time" value="{{ $data->shop_time_open}}"
                                       name="open_time" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>

                        </div>
                        <div class="col-md-2"><p>To</p></div>
                        <div class="col-md-5">
                            <div class="input-group bootstrap-timepicker timepicker">
                                <input id="close_time" value="{{ $data->shop_time_close}}"
                                       name="close_time" type="text" class="form-control input-small">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                            </div>
                        </div>
                    </div>

                    <script type="text/javascript">
                        $('#open_time').timepicker({defaultTime: '8:00:00',showMeridian:false,explicitMode:true,showSeconds:true,showInputs:false});
                        $('#close_time').timepicker({defaultTime: '18:00:00',showMeridian:false,explicitMode:true,showSeconds:true,showInputs:false});
                    </script>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback min-tip-percent">
                                <label for="min_shipper_tip_percent" style="float:left;padding-top:5px;padding-right:2px;">Min Tips For Shipper</label>
                                <input id="min_shipper_tip_percent" step="5" min="0" autocomplete=”off” type="number" style="    height: 34px;float:left;border: 2px dashed #19B5FE;padding-left: 10px;"
                                       name="min_shipper_tip_percent"
                                       placeholder="15%"
                                       value="{{ $data->min_shipper_tip_percent or old('min_shipper_tip_percent') }}">
                                <span class="glyphicons glyphicons-money form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="min_shipper_tip_percent-error"></strong>
                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label for="shipping_estimate_time">Shipping estimate time(Minutes)</label>
                                <input id="shipping_estimate_time" step="1" min="0" autocomplete=”off” type="number"
                                       name="shipping_estimate_time"   class="form-control"
                                       placeholder="15%"
                                       value="{{ $data->shipping_estimate_time or old('shipping_estimate_time') }}">
                                <span class="glyphicons glyphicons-money form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="shipping_estimate_time-error"></strong>
                    </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group has-feedback">
                                <label for="shipper_distance_customer">Distance play music (Meters)</label>
                                <input id="shipper_distance_customer" step="1" min="0" autocomplete=”off” type="number"
                                       class="form-control" name="shipper_distance_customer"
                                       placeholder="10"
                                       value="{{ $data->shipper_distance_customer or old('shipper_distance_customer') }}">
                                <span class="glyphicons glyphicons-money form-control-feedback"></span>
                                <span class="text-danger">
                                <strong id="shipper_distance_customer-error"></strong>
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <form method="POST" id="updateMusic" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <div class="form-group has-feedback">
                                    <label for="shipper_distance_customer">Default music (if not set from the shipper)</label><br>
                                    <div id="music-player">
                                        <audio controls>
                                            <source src="{{$data->shipper_music}}" type="audio/mpeg">
                                        </audio>
                                    </div>
                                    <div class="form-group">
                                        <div class="progress" id="progressMusic" style="display: none;">
                                            <div class="progress-bar progress-bar-success myprogress" role="progressbar" style="width:0%">0%</div>
                                        </div>
                                    </div>
                                    <input type="file" id="music" name="music" accept=".mp3,.m4a,.m4r,.ogg,.wav,.flac,.aac,.m4p,.wma">
                                    <span class="text-danger">
                                <strong id="music-error"></strong>
                    </span>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="col-md-8">
                    <div class="row">
                        <form method="POST" id="updateAvatar" enctype="multipart/form-data">
                            <label for="avatar">Avatar</label>

                            <div class="form-group has-feedback">
                                <div id="preview" class="col-md-5">
                                    <img src="{{$data->avatar}}"
                                         style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                                </div>
                                <div class="form-group">
                                    <div class="progress" id="progressavt" style="display: none;">
                                        <div class="progress-bar progress-bar-success imageprogress" role="progressbar" style="width:0%">0%</div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <input type="file" id="avatar" name="avatar" accept="image/*">
                                    <strong id="avatar-error"></strong>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{--<div class="row">--}}
                        {{--<form method="POST" id="updateShopBackground" enctype="multipart/form-data">--}}
                            {{--<label for="shop_background">Shop Background</label>--}}

                            {{--<div class="form-group has-feedback">--}}
                                {{--<div id="shopBackgroundPreview" class="col-md-5">--}}
                                    {{--<img src="{{$data->shop_background}}"--}}
                                         {{--style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<div class="shop-background-progress">--}}
                                        {{--<div class="progress-bar progress-bar-success shop-background-progress" role="progressbar" style="width:0%">0%</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-7">--}}
                                    {{--<input type="file" id="shop_background" name="shop_background" accept="image/*">--}}
                                    {{--<strong id="shop_background-error"></strong>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                    <div class="form-group has-feedback">
                        <label for="address">Address</label>
                        <input id="pac-input" autocomplete=”off” type="text" class="form-control" name="address"
                               placeholder="Address" value="{{ $data->address or old('address') }}">
                        <span class="glyphicon glyphicon-home form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="address-error"></strong>
                            </span>
                        <br>
                        <label for="service_radius">Service Distance ( Metter)</label>
                        <input id="service_radius" autocomplete=”nope” type="text" class="form-control"
                               name="service_radius"
                               placeholder="Service Radius" value="{{$data->service_radius or old('service_radius') }}">
                        <span class="glyphicon glyphicon-dashboard form-control-feedback form-control-feedback"></span>
                        <span class="text-danger">
                                <strong id="service_radius-error"></strong>
                            </span>
                        <input type="text" class="form-control" id="latitude" name="latitude"
                               placeholder="Latitude" value="{{$data->latitude or old('latitude') }}"
                               style="display:none">
                        <input type="text" class="form-control" id="longitude" name="longitude"
                               placeholder="Longitude" value="{{$data->longitude or old('longitude') }}"
                               style="display:none">
                        <input type="text" class="form-control" id="local_time" name="local_time"
                               style="display:none">
                        <br>
                    </div>
                    <label for="service_radius">Map</label>
                    <div id="map-canvas" style="width: 100%; height:500px;"></div>
                </div>
            </div>
        </form>
        <button id="submitForm" class="btn btn-primary">Update</button>
    </div>
    <div id='loader' style="display:none; position: fixed; top:50%; left:50%"><img
                src="http://comfortout.kennjdemo.com/storage/RiWmzj4RFdHC0ZoSODtRcyqumSFpbPZPs6ONZd1E.gif"/></div>
@stop

@section('javascript')
    <script>
        //get time in UTC 0 convert to local Time
        // window.addEventListener("DOMContentLoaded", function() {
        //     var localTimeOffset = new Date().getTimezoneOffset();
        //     var openTime = moment.utc($('#open_time').val(), "H:i:s");
        //     var closeTime = moment.utc($('#close_time').val(), "H:i:s");
        //     openTime.subtract(localTimeOffset,'minutes');
        //     openTime = openTime.format("HH:mm:ss");
        //     closeTime.subtract(localTimeOffset,'minutes');
        //     closeTime = closeTime.format("HH:mm:ss");
        //     $('#open_time').val(openTime);
        //     $('#close_time').val(closeTime);
        //
        // },false);

        //increase, decrease min-tip-percent
        $(function() {

            $(".min-tip-percent").append('<div class="inc button-inc-dec">+</div><div class="dec button-inc-dec">-</div>');

            $(".button-inc-dec").on("click", function() {

                var $button = $(this);
                var oldValue = $button.parent().find("input").val();

                if ($button.text() == "+") {
                    var newVal = parseFloat(oldValue) + 5;
                } else {
                    // Don't allow decrementing below zero
                    if (oldValue > 0) {
                        var newVal = parseFloat(oldValue) - 5;
                    } else {
                        newVal = 0;
                    }
                }

                $button.parent().find("input").val(newVal);

            });

        });

        //update avatar
        $('#avatar').on('change', function (e) {
            e.preventDefault();
            var file = this.files[0];
            if (file.size > 8000000) {
                alert('max upload size is 8MB');
                return false;
            }
            // Also see .name, .type
            var formData = new FormData();
            formData.append("avatar", $('#avatar')[0].files[0]);
            $.ajax({
                url: "/admin/shop-avatar-update",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#preview").fadeOut();
                    $("#avatar-error").fadeOut();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        $("#progressavt").fadeIn();
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.imageprogress').text(percentComplete + '%');
                            $('.imageprogress').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    if (data == 'invalid file') {
                        // invalid file format.
                        $("#avatar-error").html("Invalid File !").fadeIn();
                    }
                    else {
                        // view uploaded file.
                        $("#preview").html(data).fadeIn();
                        $("#progressavt").fadeOut();
                        toastr.success("Update Avatar Success");
                    }
                },
                error: function (e) {
                    $("#avatar-error").html(e).fadeIn();
                }
            });
        });

        //update shop background
        // $('#shop_background').on('change', function (e) {
        //     e.preventDefault();
        //     var file = this.files[0];
        //     if (file.size > 8000000) {
        //         alert('max upload size is 8MB');
        //         return false;
        //     }
        //     // Also see .name, .type
        //     var formData = new FormData();
        //     formData.append("shop_background", $('#shop_background')[0].files[0]);
        //     $.ajax({
        //         url: "/admin/shop-background-update",
        //         type: "POST",
        //         data: formData,
        //         contentType: false,
        //         cache: false,
        //         processData: false,
        //         beforeSend: function () {
        //             $("#shopBackgroundPreview").fadeOut();
        //             $("#shop_background-error").fadeOut();
        //         },
        //         xhr: function () {
        //             var xhr = new window.XMLHttpRequest();
        //             xhr.upload.addEventListener("progress", function (evt) {
        //                 if (evt.lengthComputable) {
        //                     var percentComplete = evt.loaded / evt.total;
        //                     percentComplete = parseInt(percentComplete * 100);
        //                     $('.shop-background-progress').text(percentComplete + '%');
        //                     $('.shop-background-progress').css('width', percentComplete + '%');
        //                 }
        //             }, false);
        //             return xhr;
        //         },
        //         success: function (data) {
        //             if (data == 'invalid file') {
        //                 // invalid file format.
        //                 $("#shop_background-error").html("Invalid File !").fadeIn();
        //             }
        //             else {
        //                 // view uploaded file.
        //                 $("#shopBackgroundPreview").html(data).fadeIn();
        //                 toastr.success("Update Avatar Success");
        //             }
        //         },
        //         error: function (e) {
        //             $("#shop_background-error").html(e).fadeIn();
        //         }
        //     });
        // });
        //update music
        $('#music').on('change', function (e) {
            e.preventDefault();
            var file = this.files[0];
            if (file.size > 8000000) {
                alert('max upload size is 8MB');
                return false;
            }
            // Also see .name, .type
            var formData = new FormData();
            formData.append("music", $('#music')[0].files[0]);
            $.ajax({
                url: "/admin/shop-music-update",
                type: "POST",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $("#music-player").fadeOut();
                    $("#music-error").fadeOut();
                    $("#progressMusic").fadeIn();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, false);
                    return xhr;
                },
                success: function (data) {
                    if (data == 'invalid file') {
                        // invalid file format.
                        $("#progressMusic").fadeOut();
                        $("#music-error").html("Invalid File !").fadeIn();

                    }
                    else {
                        // view uploaded file.
                        $("#progressMusic").fadeOut();
                        $("#music-player").html(data).fadeIn();
                        toastr.success("Update Music Success");
                    }
                },
                error: function (e) {
                    $("#music-error").html(e).fadeIn();
                }
            });
        });

        //update button click
        $('body').on('click', '#submitForm', function () {
            var localTime = new Date().getTimezoneOffset();
            $('#local_time').val(localTime);
            var updateForm = $("#updateForm");
            var formData = updateForm.serialize();
            $('#name-error').html("");
            $('#email-error').html("");
            $('#phone-error').html("");
            $('#password-error').html("");
            $('#stripe_id-error').html("");
            $('#open_time-error').html("");
            $('#close_time-error').html("");
            $('#shipper_distance_customer-error').html("");
            $('#address-error').html("");
            $('#service_radius-error').html("");
            $('#min_shipper_tip_percent-error').html("");

            $.ajax({
                url: '/admin/shop-update',
                type: 'POST',
                data: formData,
                beforeSend: function () {
                    $('#loader').show();
                },
                complete: function () {
                    $('#loader').hide();
                },
                success: function (data) {
                    if (data.errors) {
                        if (data.errors.name) {
                            $('#name-error').html(data.errors.name[0]);
                        }
                        if (data.errors.email) {
                            $('#email-error').html(data.errors.email[0]);
                        }
                        if (data.errors.password) {
                            $('#password-error').html(data.errors.password[0]);
                        }
                        if (data.errors.stripe_id) {
                            $('#stripe_id-error').html(data.errors.stripe_id[0]);
                        }
                        if (data.errors.open_time) {
                            $('#open_time-error').html(data.errors.open_time[0]);
                        }
                        if (data.errors.close_time) {
                            $('#close_time-error').html(data.errors.close_time[0]);
                        }
                        if (data.errors.min_shipper_tip_percent) {
                            $('#min_shipper_tip_percent-error').html(data.errors.min_shipper_tip_percent[0]);
                        }
                        if (data.errors.shipper_distance_customer) {
                            $('#shipper_distance_customer-error').html(data.errors.shipper_distance_customer[0]);
                        }
                        if (data.errors.phone) {
                            $('#phone-error').html(data.errors.phone[0]);
                        }
                        if (data.errors.address) {
                            $('#address-error').html(data.errors.address[0]);
                        }
                        if (data.errors.latitude) {
                            $('#address-error').html('Address invalid! Please try again');
                        }
                        if (data.errors.service_radius) {
                            $('#service_radius-error').html(data.errors.service_radius[0]);
                        }

                    }
                    if (data.success) {
                        toastr.success('Shop setting changed success.');
                        return false;
                    }
                },
            });
        });

        //Google map
        function initMap() {
            var lat = <?php
                echo $data->latitude ? $data->latitude : 0;
                ?>;
            var lng = <?php
                echo $data->longitude ? $data->longitude : 0;
                ?>;
            var Latlng = new google.maps.LatLng(lat, lng);

            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                center: {lat: lat, lng: lng},
                zoom: 15
            });

            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });

            if ($('#service_radius').val() != '') {
                var radius = $('#service_radius').val();
                loadRadius(radius);
            }

            //check when change Map address
            $('#pac-input').keyup(function () {
                changeMap();
            });

            //load new map when change address
            function changeMap() {
                var input = document.getElementById('pac-input');
                var autocomplete = new google.maps.places.Autocomplete(input);
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var map = new google.maps.Map(document.getElementById('map-canvas'), {
                        center: {lat: 0, lng: 0},
                        zoom: 15
                    });

                    var marker = new google.maps.Marker({
                        position: {lat: 0, lng: 0},
                        map: map
                    });

                    var place = autocomplete.getPlace();
                    // If the place has a geometry, then present it on a map.
                    if (place) {
                        if (place.geometry.viewport) {
                            map.fitBounds(place.geometry.viewport);
                        } else {
                            map.setCenter(place.geometry.location);
                            map.setZoom(13);
                        }
                    }

                    marker.setPosition(place.geometry.location);
                    marker.setVisible(true);

                    document.getElementById('latitude').value = place.geometry.location.lat();
                    document.getElementById('longitude').value = place.geometry.location.lng();

                    if ($('#service_radius').val() != "") {
                        var shopmap = {
                            center: {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()},
                        };
                        var radius = $('#service_radius').val();
                        var shopCircle = new google.maps.Circle({
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35,
                            map: map,
                            center: shopmap.center,
                            radius: radius * 1.609
                        });

                    }
                });
            }

            //check if service_radius change vallue
            $('#service_radius').on('change', function () {
                if ($('#service_radius').val() != "" && $('pac-input').val() != "") {
                    var radius = $('#service_radius').val();
                    loadRadius(radius);
                }
            });

            function loadRadius(radius) {
                var latitude = 0;
                latitude = parseFloat(document.getElementById('latitude').value);
                var longitude = 0;
                longitude = parseFloat(document.getElementById('longitude').value);

                var shopmap = {
                    center: {lat: latitude, lng: longitude},
                };

                function initMap() {
                    // Create the map.
                    var map = new google.maps.Map(document.getElementById('map-canvas'), {
                        zoom: 16,
                        center: {lat: latitude, lng: longitude},
                        mapTypeId: 'roadmap'
                    });

                    //set mark to shop base on address
                    var marker = new google.maps.Marker({
                        position: {lat: latitude, lng: longitude},
                        map: map
                    });
                    marker.setVisible(true);


                    // Construct the circle for each value in citymap.
                    var shopCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center: shopmap.center,
                        radius: radius * 1.609
                    });
                }

                initMap();


            }

        }
    </script>
    <script>
        $('#service_radius').change(function () {
            if ($('#service_radius').val() != "") {
                var radius = $('#service_radius').val();
                loadRadius(radius);
            }
        });

        function loadRadius(radius) {
            var latitude = 0;
            latitude = parseFloat(document.getElementById('latitude').value);
            var longitude = 0;
            longitude = parseFloat(document.getElementById('longitude').value);

            var shopmap = {
                center: {lat: latitude, lng: longitude},
            };

            function initMapRadius() {
                // Create the map.
                var map = new google.maps.Map(document.getElementById('map-canvas'), {
                    zoom: 16,
                    center: {lat: latitude, lng: longitude},
                    mapTypeId: 'roadmap'
                });

                //set mark to shop base on address
                var marker = new google.maps.Marker({
                    position: {lat: latitude, lng: longitude},
                    map: map
                });
                marker.setVisible(true);


                // Construct the circle for each value in citymap.
                var cityCircle = new google.maps.Circle({
                    strokeColor: '#FF0000',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#FF0000',
                    fillOpacity: 0.35,
                    map: map,
                    center: shopmap.center,
                    radius: radius * 1.609
                });
            }

            initMapRadius();

        }
    </script>
    <script src="@php echo 'https://maps.googleapis.com/maps/api/js?key='.env('GOOGLE_MAPS_API_KEY').'&libraries=places&callback=initMap'; @endphp "
            async defer></script>
@stop
