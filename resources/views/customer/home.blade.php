@extends('customer.layout.auth')

@section('content')
    <br><br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Success</div>

                <div class="panel-body">
                    Reset password success. Please close this site and try to login again.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
