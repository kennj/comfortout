-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 19, 2018 at 07:45 AM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beachad`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`, `category_image`, `deleted_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-04-17 21:55:43', '2018-04-18 19:43:38', 'categories/April2018/4WAZjjJFQAAEa0zyqYKv.png', NULL),
(2, NULL, 1, 'Category 2', 'category-2', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL, NULL),
(3, NULL, 1, 'test', 'test', '2018-04-18 04:28:09', '2018-04-18 04:28:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_login` tinyint(4) DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `password`, `phone`, `avatar`, `device_token`, `api_token`, `is_login`, `stripe_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'John', 'john@gmail.com', 'f5bb0c8de146c67b44babbf4e6584cc0', '123123123', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE IF NOT EXISTS `data_rows` (
  `id` int(10) unsigned NOT NULL,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(5, 1, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{"resize":{"width":"1000","height":"null"},"quality":"70%","upsize":true,"thumbnails":[{"name":"medium","scale":"50%"},{"name":"small","scale":"25%"},{"name":"cropped","crop":{"width":"300","height":"250"}}]}', 7),
(8, 1, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title","forceUpdate":true}}', 8),
(9, 1, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(10, 1, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{"default":"DRAFT","options":{"PUBLISHED":"published","DRAFT":"draft","PENDING":"pending"}}', 11),
(12, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(15, 2, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(16, 2, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(17, 2, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(18, 2, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(19, 2, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{"slugify":{"origin":"title"}}', 6),
(20, 2, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(21, 2, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(22, 2, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{"default":"INACTIVE","options":{"INACTIVE":"INACTIVE","ACTIVE":"ACTIVE"}}', 9),
(23, 2, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(24, 2, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(25, 2, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12),
(26, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(27, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(28, 3, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(29, 3, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{"model":"TCG\\\\Voyager\\\\Models\\\\Role","table":"roles","type":"belongsTo","column":"role_id","key":"id","label":"name","pivot_table":"roles","pivot":"0"}', 10),
(31, 3, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(32, 3, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(33, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(34, 3, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(35, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(39, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(40, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{"default":"","null":"","options":{"":"-- None --"},"relationship":{"key":"id","label":"name"}}', 2),
(41, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{"default":1}', 3),
(42, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(43, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{"slugify":{"origin":"name"}}', 5),
(44, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(45, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(46, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(51, 1, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(52, 1, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(53, 3, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, NULL, 9),
(60, 15, 'id', 'checkbox', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(61, 15, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(62, 15, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(63, 15, 'password', 'password', 'Password', 1, 1, 1, 1, 1, 1, NULL, 4),
(64, 15, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, NULL, 5),
(65, 15, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 6),
(66, 15, 'device_token', 'text', 'Device Token', 0, 0, 0, 0, 0, 0, NULL, 7),
(67, 15, 'api_token', 'text', 'Api Token', 0, 0, 0, 0, 0, 0, NULL, 8),
(68, 15, 'is_login', 'checkbox', 'Is Login', 0, 1, 1, 1, 1, 1, NULL, 9),
(69, 15, 'stripe_id', 'text', 'Stripe Id', 0, 1, 1, 1, 1, 1, NULL, 10),
(70, 15, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 11),
(71, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 12),
(72, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(73, 15, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 14),
(74, 16, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(75, 16, 'category_id', 'number', 'Category Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(76, 16, 'shop_id', 'number', 'Shop Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(77, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 4),
(78, 16, 'product_image', 'image', 'Product Image', 0, 1, 1, 1, 1, 1, NULL, 5),
(79, 16, 'price', 'number', 'Price', 0, 1, 1, 1, 1, 1, NULL, 6),
(80, 16, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, NULL, 7),
(81, 16, 'status', 'checkbox', 'Status', 0, 1, 1, 1, 1, 1, NULL, 8),
(82, 16, 'order_count', 'number', 'Order Count', 0, 1, 1, 1, 1, 1, NULL, 9),
(83, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 10),
(84, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 11),
(85, 16, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 12),
(86, 4, 'category_image', 'image', 'Category Image', 0, 1, 1, 1, 1, 1, NULL, 8),
(87, 4, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 9),
(88, 17, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(89, 17, 'product_id', 'number', 'Product Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(90, 17, 'customer_id', 'number', 'Customer Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(91, 17, 'amount', 'number', 'Amount', 1, 1, 1, 1, 1, 1, NULL, 4),
(92, 17, 'latitude', 'number', 'Latitude', 1, 1, 1, 1, 1, 1, NULL, 5),
(93, 17, 'longitude', 'number', 'Longitude', 1, 1, 1, 1, 1, 1, NULL, 6),
(94, 17, 'status', 'number', 'Status', 1, 1, 1, 1, 1, 1, NULL, 7),
(95, 17, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 8),
(96, 17, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(97, 17, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 10),
(98, 18, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(99, 18, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(100, 18, 'category_image', 'image', 'Category Image', 0, 1, 1, 1, 1, 1, NULL, 3),
(101, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(102, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(103, 18, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 6),
(104, 3, 'phone', 'text', 'Phone', 0, 1, 1, 1, 1, 1, NULL, 10),
(105, 3, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, NULL, 11),
(106, 3, 'latitude', 'number', 'Latitude', 0, 1, 1, 1, 1, 1, NULL, 12),
(107, 3, 'longitude', 'number', 'Longitude', 0, 1, 1, 1, 1, 1, NULL, 13),
(108, 3, 'time_open', 'text', 'Time Open', 0, 1, 1, 1, 1, 1, NULL, 14),
(109, 3, 'service_radius', 'number', 'Service Radius', 0, 1, 1, 1, 1, 1, NULL, 15),
(110, 3, 'is_login', 'checkbox', 'Is Login', 0, 1, 1, 1, 1, 1, NULL, 16),
(111, 3, 'stripe_id', 'text', 'Stripe Id', 0, 1, 1, 1, 1, 1, NULL, 17),
(112, 3, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 18),
(113, 19, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(114, 19, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(115, 19, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(116, 19, 'password', 'password', 'Password', 1, 1, 1, 1, 1, 1, NULL, 4),
(117, 19, 'phone', 'text', 'Phone', 1, 1, 1, 1, 1, 1, NULL, 5),
(118, 19, 'device_token', 'text', 'Device Token', 0, 1, 1, 1, 1, 1, NULL, 6),
(119, 19, 'api_token', 'text', 'Api Token', 0, 1, 1, 1, 1, 1, NULL, 7),
(120, 19, 'is_login', 'checkbox', 'Is Login', 0, 1, 1, 1, 1, 1, NULL, 8),
(121, 19, 'latitude', 'number', 'Latitude', 0, 1, 1, 1, 1, 1, NULL, 9),
(122, 19, 'longitude', 'number', 'Longitude', 0, 1, 1, 1, 1, 1, NULL, 10),
(123, 19, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 11),
(124, 19, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 12),
(125, 19, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 13),
(126, 18, 'parent_category_hasmany_category_relationship', 'relationship', 'categories', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Category","table":"categories","type":"hasMany","column":"parent_id","key":"id","label":"name","pivot_table":"categories","pivot":"0"}', 7),
(127, 4, 'category_hasmany_product_relationship', 'relationship', 'products', 0, 1, 1, 1, 1, 1, '{"model":"App\\\\Product","table":"products","type":"hasMany","column":"category_id","key":"id","label":"name","pivot_table":"categories","pivot":"0"}', 10);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE IF NOT EXISTS `data_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, '2018-04-17 21:55:42', '2018-04-17 21:55:42'),
(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, '2018-04-17 21:55:42', '2018-04-17 21:55:42'),
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '2018-04-17 21:55:42', '2018-04-18 21:47:38'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'App\\Category', NULL, NULL, NULL, 1, 0, '2018-04-17 21:55:42', '2018-04-18 03:21:31'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, '2018-04-17 21:55:42', '2018-04-17 21:55:42'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, '2018-04-17 21:55:42', '2018-04-17 21:55:42'),
(15, 'customers', 'customers', 'Customer', 'Customers', 'voyager-people', 'App\\Customer', NULL, NULL, NULL, 1, 0, '2018-04-18 02:19:18', '2018-04-18 02:31:34'),
(16, 'products', 'products', 'Product', 'Products', NULL, 'App\\Product', NULL, NULL, NULL, 1, 0, '2018-04-18 03:18:41', '2018-04-18 03:18:41'),
(17, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, NULL, NULL, 1, 0, '2018-04-18 21:36:06', '2018-04-18 21:36:06'),
(18, 'parent_categories', 'parent-categories', 'Parent Category', 'Parent Categories', NULL, 'App\\ParentCategory', NULL, NULL, NULL, 1, 0, '2018-04-18 21:46:15', '2018-04-18 21:46:15'),
(19, 'shippers', 'shippers', 'Shipper', 'Shippers', NULL, 'App\\Shipper', NULL, NULL, NULL, 1, 0, '2018-04-18 21:50:55', '2018-04-18 21:50:55');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-04-17 21:55:43', '2018-04-17 21:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-04-17 21:55:43', '2018-04-17 21:55:43', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.media.index', NULL),
(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.posts.index', NULL),
(4, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-04-17 21:55:43', '2018-04-17 21:55:43', 'voyager.users.index', NULL),
(5, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.categories.index', NULL),
(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.pages.index', NULL),
(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-04-17 21:55:43', '2018-04-17 21:55:43', 'voyager.roles.index', NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2018-04-17 21:55:43', '2018-04-18 02:23:05', NULL, NULL),
(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.menus.index', NULL),
(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.database.index', NULL),
(11, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 8, 3, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.compass.index', NULL),
(12, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.settings.index', NULL),
(13, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 8, 4, '2018-04-17 21:55:43', '2018-04-18 02:23:05', 'voyager.hooks', NULL),
(16, 1, 'Customers', '/cpanel/customers', '_self', 'voyager-people', '#000000', NULL, 10, '2018-04-18 02:19:18', '2018-04-18 03:10:31', NULL, ''),
(17, 1, 'Products', '/cpanel/products', '_self', 'voyager-rum', '#000000', NULL, 11, '2018-04-18 03:18:41', '2018-04-18 21:58:30', NULL, ''),
(18, 1, 'Orders', '/cpanel/orders', '_self', 'voyager-basket', '#000000', NULL, 12, '2018-04-18 21:36:06', '2018-04-18 21:59:20', NULL, ''),
(19, 1, 'Parent Categories', '/cpanel/parent-categories', '_self', 'voyager-star-two', '#000000', NULL, 13, '2018-04-18 21:46:16', '2018-04-18 22:00:22', NULL, ''),
(20, 1, 'Shippers', '/cpanel/shippers', '_self', 'voyager-truck', '#000000', NULL, 14, '2018-04-18 21:50:55', '2018-04-18 22:01:46', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_add_permission_group_id_to_permissions_table', 1),
(17, '2017_01_15_000000_create_permission_groups_table', 1),
(18, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(19, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(20, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(21, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(22, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(23, '2017_08_05_000000_add_group_to_settings_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '1',
  `latitude` int(11) NOT NULL DEFAULT '0',
  `longitude` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o''nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-04-17 21:55:43', '2018-04-17 21:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `parent_categories`
--

CREATE TABLE IF NOT EXISTS `parent_categories` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(2, 'browse_database', NULL, '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(3, 'browse_media', NULL, '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(4, 'browse_compass', NULL, '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(5, 'browse_menus', 'menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(6, 'read_menus', 'menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(7, 'edit_menus', 'menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(8, 'add_menus', 'menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(9, 'delete_menus', 'menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(10, 'browse_pages', 'pages', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(11, 'read_pages', 'pages', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(12, 'edit_pages', 'pages', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(13, 'add_pages', 'pages', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(14, 'delete_pages', 'pages', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(15, 'browse_roles', 'roles', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(16, 'read_roles', 'roles', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(17, 'edit_roles', 'roles', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(18, 'add_roles', 'roles', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(19, 'delete_roles', 'roles', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(20, 'browse_users', 'users', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(21, 'read_users', 'users', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(22, 'edit_users', 'users', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(23, 'add_users', 'users', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(24, 'delete_users', 'users', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(25, 'browse_posts', 'posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(26, 'read_posts', 'posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(27, 'edit_posts', 'posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(28, 'add_posts', 'posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(29, 'delete_posts', 'posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(30, 'browse_categories', 'categories', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(31, 'read_categories', 'categories', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(32, 'edit_categories', 'categories', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(33, 'add_categories', 'categories', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(34, 'delete_categories', 'categories', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(35, 'browse_settings', 'settings', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(36, 'read_settings', 'settings', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(37, 'edit_settings', 'settings', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(38, 'add_settings', 'settings', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(39, 'delete_settings', 'settings', '2018-04-17 21:55:43', '2018-04-17 21:55:43', NULL),
(40, 'browse_hooks', NULL, '2018-04-17 21:55:44', '2018-04-17 21:55:44', NULL),
(51, 'browse_customers', 'customers', '2018-04-18 02:19:18', '2018-04-18 02:19:18', NULL),
(52, 'read_customers', 'customers', '2018-04-18 02:19:18', '2018-04-18 02:19:18', NULL),
(53, 'edit_customers', 'customers', '2018-04-18 02:19:18', '2018-04-18 02:19:18', NULL),
(54, 'add_customers', 'customers', '2018-04-18 02:19:18', '2018-04-18 02:19:18', NULL),
(55, 'delete_customers', 'customers', '2018-04-18 02:19:18', '2018-04-18 02:19:18', NULL),
(56, 'browse_products', 'products', '2018-04-18 03:18:41', '2018-04-18 03:18:41', NULL),
(57, 'read_products', 'products', '2018-04-18 03:18:41', '2018-04-18 03:18:41', NULL),
(58, 'edit_products', 'products', '2018-04-18 03:18:41', '2018-04-18 03:18:41', NULL),
(59, 'add_products', 'products', '2018-04-18 03:18:41', '2018-04-18 03:18:41', NULL),
(60, 'delete_products', 'products', '2018-04-18 03:18:41', '2018-04-18 03:18:41', NULL),
(61, 'browse_orders', 'orders', '2018-04-18 21:36:06', '2018-04-18 21:36:06', NULL),
(62, 'read_orders', 'orders', '2018-04-18 21:36:06', '2018-04-18 21:36:06', NULL),
(63, 'edit_orders', 'orders', '2018-04-18 21:36:06', '2018-04-18 21:36:06', NULL),
(64, 'add_orders', 'orders', '2018-04-18 21:36:06', '2018-04-18 21:36:06', NULL),
(65, 'delete_orders', 'orders', '2018-04-18 21:36:06', '2018-04-18 21:36:06', NULL),
(66, 'browse_parent_categories', 'parent_categories', '2018-04-18 21:46:15', '2018-04-18 21:46:15', NULL),
(67, 'read_parent_categories', 'parent_categories', '2018-04-18 21:46:15', '2018-04-18 21:46:15', NULL),
(68, 'edit_parent_categories', 'parent_categories', '2018-04-18 21:46:15', '2018-04-18 21:46:15', NULL),
(69, 'add_parent_categories', 'parent_categories', '2018-04-18 21:46:15', '2018-04-18 21:46:15', NULL),
(70, 'delete_parent_categories', 'parent_categories', '2018-04-18 21:46:15', '2018-04-18 21:46:15', NULL),
(71, 'browse_shippers', 'shippers', '2018-04-18 21:50:55', '2018-04-18 21:50:55', NULL),
(72, 'read_shippers', 'shippers', '2018-04-18 21:50:55', '2018-04-18 21:50:55', NULL),
(73, 'edit_shippers', 'shippers', '2018-04-18 21:50:55', '2018-04-18 21:50:55', NULL),
(74, 'add_shippers', 'shippers', '2018-04-18 21:50:55', '2018-04-18 21:50:55', NULL),
(75, 'delete_shippers', 'shippers', '2018-04-18 21:50:55', '2018-04-18 21:50:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE IF NOT EXISTS `permission_groups` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-17 21:55:43', '2018-04-17 21:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` float DEFAULT '0',
  `description` text COLLATE utf8_unicode_ci,
  `status` tinyint(4) DEFAULT '1',
  `order_count` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(2, 'user', 'Normal User', '2018-04-17 21:55:43', '2018-04-17 21:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/April2018/VDk5MfYcfJ5YApxDAwub.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', '', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Beach Cpanel', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Beach Cpanel.', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin'),
(11, 'site.site_url', 'Site Url', 'http://beachad.test', NULL, 'text', 6, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `shippers`
--

CREATE TABLE IF NOT EXISTS `shippers` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `device_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_login` tinyint(4) DEFAULT '0',
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(10) unsigned NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o''nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(22, 'menu_items', 'title', 3, 'pt', 'Publicações', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(23, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(24, 'menu_items', 'title', 5, 'pt', 'Categorias', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(25, 'menu_items', 'title', 6, 'pt', 'Páginas', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(26, 'menu_items', 'title', 7, 'pt', 'Funções', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(27, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(28, 'menu_items', 'title', 9, 'pt', 'Menus', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(29, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2018-04-17 21:55:43', '2018-04-17 21:55:43'),
(30, 'menu_items', 'title', 12, 'pt', 'Configurações', '2018-04-17 21:55:43', '2018-04-17 21:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `time_open` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_radius` int(11) DEFAULT '5',
  `is_login` tinyint(4) DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `address`, `latitude`, `longitude`, `time_open`, `service_radius`, `is_login`, `stripe_id`, `deleted_at`) VALUES
(1, 1, 'Admin', 'admin@admin.com', 'users/April2018/1vobPv3OeSVQuURyJ7dD.png', '$2y$10$aY8vXAQ.Eayk36Wr0neZMOZGaYy5Ssx7xM42qwWOCdepFDO.hPe8q', 'DgvHddIITBpJaXYwfV8vjKA2M2TpCCPn4OWvFpiekXBc7mcEx8MIEj0jY0Ly', '2018-04-17 21:55:43', '2018-04-18 01:53:22', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, NULL),
(2, 2, 'Shop', 'shop@shop.com', 'users/default.png', '$2y$10$YEQHRkj7iAoDiJ2zTOpn3u5pDHvPvpzIglOWzdSXR9EU7A5lpSd9.', 'R3uj6TraARGRbLbbuzpcElYBWXTkzWGtj76x7xffS1KaMv6ky1215jZ6wB9O', '2018-04-17 21:56:54', '2018-04-17 21:56:54', NULL, NULL, NULL, NULL, NULL, 5, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`),
  ADD UNIQUE KEY `customers_api_token_unique` (`api_token`),
  ADD KEY `customers_stripe_id_index` (`stripe_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `parent_categories`
--
ALTER TABLE `parent_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `shippers`
--
ALTER TABLE `shippers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `parent_categories`
--
ALTER TABLE `parent_categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `shippers`
--
ALTER TABLE `shippers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
