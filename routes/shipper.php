<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('shipper')->user();

    //dd($users);

    return view('shipper.home');
})->name('home');

