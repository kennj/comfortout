<?php

use Illuminate\Http\Request;
use App\Http\Middleware\CheckCustomerApiToken;
use App\Http\Middleware\CheckShipperApiToken;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'v1'], function () {
    // Customer
    Route::get('/customer/getAll', 'CustomerController@getAll');
    Route::get('/customer/getByID/{customerID}', 'CustomerController@getbyID');
    Route::post('/customer/register', 'CustomerController@register');
    Route::post('/customer/login', 'CustomerController@login');
    Route::post('/customer/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLink')->name('password.request');

    //Shipper
    Route::get('/shipper/getAll', 'ShipperController@getAll');
    Route::get('/shipper/getByID/{customerID}', 'ShipperController@getbyID');
    Route::post('/shipper/register', 'ShipperController@register');
    Route::post('/shipper/login', 'ShipperController@login');
    Route::post('/shipper/password/email', 'ShipperAuth\ForgotPasswordController@sendResetLink')->name('password.request');

    //Parent Categories
    Route::get('/parent-category','ParentCategoryController@showAllParentCategory');
    Route:: post('/category-by-parent-id','ParentCategoryController@showCategoriesByParentCategory');

    //Services
    Route::post('/services','ServiceController@showAllServices');
    Route:: post('/services/parent-category-by-service-id','ServiceController@showParentCategory');

    //Categories
    Route::get('/categories', 'CategoryController@showAllCategories');
    Route::post('categories-products', 'CategoryController@showProductByCategory');

    //Products
    Route::post('/product','ProductController@show');
    Route::get('/all-product','ProductController@showAll');
    Route::post('/product-hot','ProductController@showHot');
    Route::post('/related-product/{product_id}','ProductController@showRelatedProducts');

    //Shop
    Route::get('/shop','ShopController@showAll');
    Route::get('shop/{shop_id}','ShopController@getShop');
    Route::get('product-by-shop/{shop_id}','ShopController@getProductByShop');

    //Order
    Route::post('/check-order-status', 'OrderController@checkOrderStatus');


    /* Routes Check Middleware for Customer */
    Route::group(['middleware' => CheckCustomerApiToken::class], function () {

        //Customer
        Route::post('/customer/update', 'CustomerController@update');
        Route::post('/customer/update-location', 'CustomerController@updateLocation');
        Route::post('customer/logout', 'CustomerController@logout');
        Route::post('/customer/search-by-address','SearchController@SearchShopByAddress');


        Route::post('customer/delete-notification', 'CustomerController@deleteNotification');
        Route::post('customer/delete-all-notification', 'CustomerController@deleteAllNotifications');
        Route::post('/customer/email', 'CustomerController@getByEmail');
        Route::post('/customer/delete', 'CustomerController@deleteByEmail');

        // Credit Card
        Route::post('/credit-card/balance', 'CreditCardController@getBalance');
        Route::post('/credit-card/add', 'CreditCardController@add');
        Route::post('/credit-card/set-default', 'CreditCardController@setDefault');
        Route::post('/credit-card/remove', 'CreditCardController@remove');
        Route::post('/credit-card/list', 'CreditCardController@getList');

        // Payment
        Route::post('/payment/checkout', 'PaymentController@checkout');

        //Order
        Route::post('/create-order', 'CustomerController@createOrders');
        Route::post('/order-again', 'CustomerController@orderAgain');
        Route::post('customer/cancel-order', 'CustomerController@cancelOrderByCustomer');
        Route::post('/customer/list-all-orders-by-customer', 'CustomerController@listAllOrderByCustomer');

        //Notifications
        Route::post('customer/notification', 'CustomerController@getListNotification');
        Route::post('customer/delete-notification', 'CustomerController@deleteNotification');
        Route::post('customer/delete-all-notification', 'CustomerController@deleteAllNotifications');

        //Order
        Route::post('customer/check-order-status', 'OrderController@checkOrderStatus');

        //Check if Shop available using distance
        Route::post('customer/check-if-checkout-available', 'CustomerController@checkIfCheckOutAvailable');
    });
    /* Routes Check Middleware for Shipper */
    Route::group(['middleware' => CheckShipperApiToken::class], function () {

        //Shipper
        Route::post('/shipper/update', 'ShipperController@update');
        Route::post('/shipper/update-location', 'ShipperController@updateLocation');
        Route::post('shipper/logout', 'ShipperController@logout');

        Route::post('shipper/notification', 'ShipperController@getListNotification');
        Route::post('shipper/delete-notification', 'ShipperController@deleteNotification');
        Route::post('shipper/delete-all-notification', 'ShipperController@deleteAllNotifications');
        Route::post('/shipper/email', 'ShipperController@getByEmail');
        Route::post('/shipper/delete', 'ShipperController@deleteByEmail');

        // Credit Card
//        Route::post('/credit-card/balance', 'CreditCardController@getBalance');
//        Route::post('/credit-card/add', 'CreditCardController@add');
//        Route::post('/credit-card/set-default', 'CreditCardController@setDefault');
//        Route::post('/credit-card/remove', 'CreditCardController@remove');
//        Route::post('/credit-card/list', 'CreditCardController@getList');

        // Payment
//        Route::post('/payment/checkout', 'PaymentController@checkout');

        //Order
        Route::post('/list-all-orders', 'ShipperController@listAllOrder');
        Route::post('/list-all-orders-not-pick-up', 'ShipperController@listAllOrderOfShopNotPickUp');
        Route::post('/list-all-orders-pick-up', 'ShipperController@listAllOrderOfShopPickUp');
        Route::post('/list-all-orders-pick-up-in-day', 'ShipperController@listAllOrderOfShopPickUpInDay');
        Route::post('/check-distance', 'ShipperController@checkDistanceOrderCustomer');
        Route::post('/approve-order', 'ShipperController@approveOrder');
        Route::post('/cancel-order', 'ShipperController@cancelOrder');
        Route::post('/delevering-order', 'ShipperController@deliveringOrder');
        Route::post('/complete-order', 'ShipperController@completeOrder');
        Route::post('/complete-payment-tip', 'ShipperController@completePaymentAndTip');
        Route::post('/delete-order-history', 'ShipperController@deleteHistoryOrder');
        Route::post('/tips-count', 'ShipperController@tipCount');

        //Notifications
        Route::post('shipper/notification', 'ShipperController@getListNotification');
        Route::post('shipper/delete-notification', 'ShipperController@deleteNotification');
        Route::post('shipper/delete-all-notification', 'ShipperController@deleteAllNotifications');

    });
});