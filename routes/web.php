<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');

Route::post('admin/serviceTypeList', function () {
    if (Request::ajax()) {
        $service_id = \Illuminate\Support\Facades\Input::get('service');
        $serviceTypes = \App\ServiceLocation::where('service_id', $service_id)->get();


        if ($service_id !== 'Select') {
            echo "<label>Service Location:</label>";
            echo '<select id="service_location" name="service_location" class="form-control" required> ';
            echo '<option value="">Select location</option>';
            foreach ($serviceTypes as $serviceType) {
                echo '<option value="' . $serviceType->id . '">' . $serviceType->name . "</option>";
            }
            echo "</select>";
        }
        echo "<br><label>Not in list? Please contact us to request new</label>";
    }
});


Route::post('getCategoryList', function () {
    if (Request::ajax()) {
        $parent_category_id = \Illuminate\Support\Facades\Input::get('parentCategory');
        $allCategory = \App\Category::where('parent_id', $parent_category_id)->get();
        if (count($allCategory) != 0) {
//            echo '<select id="service-type" name="service-type" class="form-control">';
            echo '<div class="row">';
            foreach ($allCategory as $category) {
                echo '<div class="col-md-1 catgegory" >
<div class="row"><p class="text-center" style="margin:0 auto;color:red; font-weight:700">' . $category->name . '</p></div><div class="row">
                                                <label class="btn">
                                                    <img src="' . $category->category_image . '" class="img-thumbnail img-check" style="background-color: black;">
                                                    <input type="radio"  name="category_id" id="' . $category->id . '" value="' . $category->id . '" autocomplete="off">
                                                </label>
                                                </div>
                                              </div>';
            }
            echo '</div>';
            echo '<script>                    $( "input[name=category_id]:radio" ).change(function () { 
                        $(".catgegory").addClass("radioActive");
                        $("div:has(input:radio:not(:checked))").removeClass("radioActive");
                    });</script>';
        } else {
            echo "There is no Category in this Parent Category. Please contact Admin to add category for this.";
        }
    }
});

Auth::routes();


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('/user-not-active', 'ShopNotActiveController@index');

    //call ajax to active shop (change shop active to true)
    Route::post('active-shop', 'ShopNotActiveController@activeShop');

    //call ajax to update shop (from shop-settings page)
    Route::post('shop-update', 'ShopSettingsController@update');

    //call ajax to update shop avatar (from shop-settings page)
    Route::post('shop-avatar-update', 'ShopSettingsController@updateAvatar');

    //call ajax to update shop background (from shop-settings page)
    Route::post('shop-background-update', 'ShopSettingsController@updateShopBackground');

    //call ajax to update shop avatar (from shop-settings page)
    Route::post('shop-music-update', 'ShopSettingsController@updateMusic');

    //call ajax to update shop avatar (from shipper-settings page)
    Route::post('shipper/shipper-music-update', 'ShopSettingsController@shipperUpdateMusic');

    //get Parent Category List on category  create/edit page
    Route::post('parentCategoryList', function () {
        if (Request::ajax()) {
            $service_id = \Illuminate\Support\Facades\Input::get('service');
            $parentCategoryList = \App\ParentCategory::where('service_id', $service_id)->get();

            if ($parentCategoryList->count()) {
                echo '<select id="parent_id" name="parent_id" class="form-control">';
                echo '<option value="">Select Parent Category:</option>';
                foreach ($parentCategoryList as $parentCategory) {
                    echo '<option value="' . $parentCategory->id . '">' . $parentCategory->name . "</option>";
                }
                echo "</select>";
            } else
                echo 'There is no Parent Category in this Service. <a href="http://comfortout.kennjdemo.com/public/admin/parent-categories/create">Click here to create Parent Category</a>';

        }
    });

});

Route::group(['prefix' => 'customer'], function () {
//  Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('login');
//  Route::post('/login', 'CustomerAuth\LoginController@login');
//  Route::post('/logout', 'CustomerAuth\LoginController@logout')->name('logout');
//
//  Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm')->name('register');
//  Route::post('/register', 'CustomerAuth\RegisterController@register');
//
  Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'shipper'], function () {
//  Route::get('/login', 'ShipperAuth\LoginController@showLoginForm')->name('login');
//  Route::post('/login', 'ShipperAuth\LoginController@login');
//  Route::post('/logout', 'ShipperAuth\LoginController@logout')->name('logout');
//
//  Route::get('/register', 'ShipperAuth\RegisterController@showRegistrationForm')->name('register');
//  Route::post('/register', 'ShipperAuth\RegisterController@register');
//
  Route::post('/password/reset', 'ShipperAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'ShipperAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'ShipperAuth\ResetPasswordController@showResetForm');
});

